-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 02 Nis 2018, 10:30:30
-- Sunucu sürümü: 10.1.30-MariaDB
-- PHP Sürümü: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `ormix`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `alt_menu`
--

CREATE TABLE `alt_menu` (
  `id` int(11) NOT NULL,
  `adi` varchar(255) NOT NULL,
  `ust_menu_id` int(11) NOT NULL,
  `durum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `alt_menu`
--

INSERT INTO `alt_menu` (`id`, `adi`, `ust_menu_id`, `durum`) VALUES
(1, 'gelen kutusu', 2, 0),
(2, 'mesaj oluştur', 2, 0),
(3, 'uye ekle', 3, 0),
(4, 'uyeler', 3, 0),
(5, 'müşteri ekle', 4, 0),
(6, 'Müşteri listele', 4, 0),
(7, 'firma ekle', 5, 0),
(8, 'firma listesi', 5, 0),
(9, 'ürün ekle', 6, 0),
(10, 'ürün seri ekle', 6, 0),
(11, 'üretim modülü ekle', 6, 0),
(12, 'üretim modülü listele', 6, 0),
(13, 'üretim ekle', 6, 0),
(14, 'iade üretim listesi', 6, 0),
(15, 'kasa ekle', 7, 0),
(16, 'kasa yonetimi', 7, 0),
(17, 'kasa listesi', 7, 0),
(18, 'senet ve çek listesi', 7, 0),
(19, 'onaylanmamış siparişler', 8, 0),
(20, 'beklemedeki siparişler', 8, 0),
(21, 'onaylanmış siparişler', 8, 0),
(22, 'tamamlanan siparisler', 8, 0),
(23, 'onaylanmamış iş emirleri', 9, 0),
(24, 'onaylanmış iş emirleri', 9, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `cevap`
--

CREATE TABLE `cevap` (
  `id` int(11) NOT NULL,
  `gonderen_id` int(11) NOT NULL,
  `alan_id` int(11) NOT NULL,
  `konu` varchar(255) NOT NULL,
  `icerik` text NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `gonderen_sil` int(11) NOT NULL,
  `alan_sil` int(11) NOT NULL,
  `ek_id` int(11) NOT NULL,
  `alan_gordumu` int(11) NOT NULL,
  `gonderen_gordumu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ekler`
--

CREATE TABLE `ekler` (
  `id` int(11) NOT NULL,
  `dosya_adi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `firmalar`
--

CREATE TABLE `firmalar` (
  `firma_id` int(11) NOT NULL,
  `firma_kodu` int(11) NOT NULL,
  `firma_adi` varchar(255) NOT NULL,
  `firma_turu` int(11) NOT NULL,
  `firma_adresi` text NOT NULL,
  `firma_mail` varchar(255) NOT NULL,
  `musteri_adi` varchar(255) NOT NULL,
  `musteri_soyadi` varchar(255) NOT NULL,
  `musteri_telefon` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `ulke` varchar(255) NOT NULL,
  `il` varchar(255) NOT NULL,
  `ilce` varchar(255) NOT NULL,
  `cari` int(11) NOT NULL,
  `hesap_turu` varchar(255) NOT NULL,
  `adres` text NOT NULL,
  `musteri_unvani` varchar(255) NOT NULL,
  `vergi_no` int(11) NOT NULL,
  `il1` varchar(255) NOT NULL,
  `ilce1` varchar(255) NOT NULL,
  `vergi_dairesi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `firmalar`
--

INSERT INTO `firmalar` (`firma_id`, `firma_kodu`, `firma_adi`, `firma_turu`, `firma_adresi`, `firma_mail`, `musteri_adi`, `musteri_soyadi`, `musteri_telefon`, `fax`, `ulke`, `il`, `ilce`, `cari`, `hesap_turu`, `adres`, `musteri_unvani`, `vergi_no`, `il1`, `ilce1`, `vergi_dairesi`) VALUES
(1, 93055, 'ASDFASDF', 2, 'KKDSDAS', 'FSDFSD@GAİL.COM', 'MÜMİN', 'BAKIRCI', '5073746141', '2122222222', 'Türkiye', 'İstanbul', 'Bağcılar', 2500, 'Türk Lirası', 'KKDSDAS', 'ASDFASD', 23412412, 'FDSFS', 'SDFSDF', 'FASDFASFAS');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `firmaodeme`
--

CREATE TABLE `firmaodeme` (
  `id` int(11) NOT NULL,
  `musteri_id` int(11) NOT NULL,
  `odenen_miktar` varchar(255) NOT NULL,
  `ekleme_tarihi` varchar(255) NOT NULL,
  `odeme_tarih` varchar(255) NOT NULL,
  `kesidici` varchar(255) NOT NULL,
  `muhattap` varchar(255) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `banka_hesabi` int(11) NOT NULL,
  `banka_adi` varchar(255) NOT NULL,
  `odemeturu` varchar(255) NOT NULL,
  `tursec` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `gelenkutusu`
--

CREATE TABLE `gelenkutusu` (
  `id` int(11) NOT NULL,
  `gonderen_id` int(11) NOT NULL,
  `alan_id` int(11) NOT NULL,
  `konu` varchar(255) NOT NULL,
  `icerik` text NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `gonderen_sil` int(11) NOT NULL,
  `alan_sil` int(11) NOT NULL,
  `ek_id` varchar(255) NOT NULL,
  `alan_gordumu` int(11) NOT NULL,
  `gonderen_gordumu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `gonderilenler`
--

CREATE TABLE `gonderilenler` (
  `id` int(11) NOT NULL,
  `gonderen_id` int(11) NOT NULL,
  `alan_id` int(11) NOT NULL,
  `konu` varchar(255) NOT NULL,
  `icerik` text NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `gonderen_sil` int(11) NOT NULL,
  `alan_sil` int(11) NOT NULL,
  `ek_id` varchar(255) NOT NULL,
  `mesaj_goster_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `grup_rutbe`
--

CREATE TABLE `grup_rutbe` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `rutbe_id` int(11) NOT NULL,
  `durum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `grup_rutbe`
--

INSERT INTO `grup_rutbe` (`id`, `menu_id`, `rutbe_id`, `durum`) VALUES
(1, 2, 1, 1),
(2, 3, 1, 1),
(3, 4, 1, 1),
(4, 5, 1, 1),
(5, 6, 1, 1),
(6, 7, 1, 1),
(7, 8, 1, 1),
(8, 9, 1, 1),
(9, 10, 1, 1),
(10, 2, 2, 0),
(11, 3, 2, 0),
(12, 4, 2, 0),
(13, 5, 2, 0),
(14, 6, 2, 0),
(15, 7, 2, 0),
(16, 8, 2, 0),
(17, 9, 2, 0),
(18, 10, 2, 0),
(19, 2, 3, 0),
(20, 3, 3, 0),
(21, 4, 3, 0),
(22, 5, 3, 0),
(23, 6, 3, 0),
(24, 7, 3, 0),
(25, 8, 3, 0),
(26, 9, 3, 0),
(27, 10, 3, 0),
(28, 1, 4, 0),
(29, 2, 4, 0),
(30, 3, 4, 0),
(31, 4, 4, 0),
(32, 5, 4, 0),
(33, 6, 4, 0),
(34, 7, 4, 0),
(35, 8, 4, 0),
(36, 9, 4, 0),
(37, 10, 4, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `iadeurunler`
--

CREATE TABLE `iadeurunler` (
  `id` int(11) NOT NULL,
  `siparis_id` int(11) NOT NULL,
  `urun_id` int(11) NOT NULL,
  `iade_adet` int(11) NOT NULL,
  `mevcut_adet` int(11) NOT NULL,
  `notekle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ilceler`
--

CREATE TABLE `ilceler` (
  `ilce_no` int(11) NOT NULL,
  `isim` varchar(50) DEFAULT NULL,
  `il_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `ilceler`
--

INSERT INTO `ilceler` (`ilce_no`, `isim`, `il_no`) VALUES
(1, 'Abana', 37),
(2, 'Acıgöl', 50),
(3, 'Acıpayam', 20),
(4, 'Adaklı', 12),
(5, 'Adalar', 34),
(6, 'Adapazarı', 54),
(7, 'Adıyaman', 2),
(8, 'Adilcevaz', 13),
(9, 'Afşin', 46),
(10, 'Afyonkarahisar', 3),
(11, 'Ağaçören', 68),
(12, 'Ağın', 23),
(13, 'Ağlasun', 15),
(14, 'Ağlı', 37),
(15, 'Ağrı', 4),
(16, 'Ahırlı', 42),
(17, 'Ahlat', 13),
(18, 'Ahmetli', 45),
(19, 'Akçaabat', 61),
(20, 'Akçadağ', 44),
(21, 'Akçakale', 63),
(22, 'Akçakent', 40),
(23, 'Akçakoca', 81),
(24, 'Akdağmadeni', 66),
(25, 'Akdeniz', 33),
(26, 'Akhisar', 45),
(27, 'Akıncılar', 58),
(28, 'Akkışla', 38),
(29, 'Akkuş', 52),
(30, 'Akören', 42),
(31, 'Akpınar', 40),
(32, 'Aksaray', 68),
(33, 'Akseki', 7),
(34, 'Aksu', 7),
(35, 'Aksu', 32),
(36, 'Akşehir', 42),
(37, 'Akyaka', 36),
(38, 'Akyazı', 54),
(39, 'Akyurt', 6),
(40, 'Alaca', 19),
(41, 'Alacakaya', 23),
(42, 'Alaçam', 55),
(43, 'Aladağ', 1),
(44, 'Alanya', 7),
(45, 'Alaplı', 67),
(46, 'Alaşehir', 45),
(47, 'Aliağa', 35),
(48, 'Almus', 60),
(49, 'Alpu', 26),
(50, 'Altıeylül', 10),
(51, 'Altındağ', 6),
(52, 'Altınekin', 42),
(53, 'Altınordu', 52),
(54, 'Altınova', 77),
(55, 'Altınözü', 31),
(56, 'Altıntaş', 43),
(57, 'Altınyayla', 15),
(58, 'Altınyayla', 58),
(59, 'Altunhisar', 51),
(60, 'Alucra', 28),
(61, 'Amasra', 74),
(62, 'Amasya', 5),
(63, 'Anamur', 33),
(64, 'Andırın', 46),
(65, 'Antakya', 31),
(66, 'Araban', 27),
(67, 'Araç', 37),
(68, 'Araklı', 61),
(69, 'Aralık', 76),
(70, 'Arapgir', 44),
(71, 'Ardahan', 75),
(72, 'Ardanuç', 8),
(73, 'Ardeşen', 53),
(74, 'Arguvan', 44),
(75, 'Arhavi', 8),
(76, 'Arıcak', 23),
(77, 'Arifiye', 54),
(78, 'Armutlu', 77),
(79, 'Arnavutköy', 34),
(80, 'Arpaçay', 36),
(81, 'Arsin', 61),
(82, 'Arsuz', 31),
(83, 'Artova', 60),
(84, 'Artuklu', 47),
(85, 'Artvin', 8),
(86, 'Asarcık', 55),
(87, 'Aslanapa', 43),
(88, 'Aşkale', 25),
(89, 'Atabey', 32),
(90, 'Atakum', 55),
(91, 'Ataşehir', 34),
(92, 'Atkaracalar', 18),
(93, 'Avanos', 50),
(94, 'Avcılar', 34),
(95, 'Ayancık', 57),
(96, 'Ayaş', 6),
(97, 'Aybastı', 52),
(98, 'Aydıncık', 33),
(99, 'Aydıncık', 66),
(100, 'Aydıntepe', 69),
(101, 'Ayrancı', 70),
(102, 'Ayvacık', 17),
(103, 'Ayvacık', 55),
(104, 'Ayvalık', 10),
(105, 'Azdavay', 37),
(106, 'Aziziye', 25),
(107, 'Babadağ', 20),
(108, 'Babaeski', 39),
(109, 'Bafra', 55),
(110, 'Bağcılar', 34),
(111, 'Bağlar', 21),
(112, 'Bahçe', 80),
(113, 'Bahçelievler', 34),
(114, 'Bahçesaray', 65),
(115, 'Bahşili', 71),
(116, 'Bakırköy', 34),
(117, 'Baklan', 20),
(118, 'Bala', 6),
(119, 'Balçova', 35),
(120, 'Balışeyh', 71),
(121, 'Balya', 10),
(122, 'Banaz', 64),
(123, 'Bandırma', 10),
(124, 'Bartın', 74),
(125, 'Baskil', 23),
(126, 'Başakşehir', 34),
(127, 'Başçiftlik', 60),
(128, 'Başiskele', 41),
(129, 'Başkale', 65),
(130, 'Başmakçı', 3),
(131, 'Başyayla', 70),
(132, 'Batman', 72),
(133, 'Battalgazi', 44),
(134, 'Bayat', 3),
(135, 'Bayat', 19),
(136, 'Bayburt', 69),
(137, 'Bayındır', 35),
(138, 'Baykan', 56),
(139, 'Bayraklı', 35),
(140, 'Bayramiç', 17),
(141, 'Bayramören', 18),
(142, 'Bayrampaşa', 34),
(143, 'Bekilli', 20),
(144, 'Belen', 31),
(145, 'Bergama', 35),
(146, 'Besni', 2),
(147, 'Beşikdüzü', 61),
(148, 'Beşiktaş', 34),
(149, 'Beşiri', 72),
(150, 'Beyağaç', 20),
(151, 'Beydağ', 35),
(152, 'Beykoz', 34),
(153, 'Beylikdüzü', 34),
(154, 'Beylikova', 26),
(155, 'Beyoğlu', 34),
(156, 'Beypazarı', 6),
(157, 'Beyşehir', 42),
(158, 'Beytüşşebap', 73),
(159, 'Biga', 17),
(160, 'Bigadiç', 10),
(161, 'Bilecik', 11),
(162, 'Bingöl', 12),
(163, 'Birecik', 63),
(164, 'Bismil', 21),
(165, 'Bitlis', 13),
(166, 'Bodrum', 48),
(167, 'Boğazkale', 19),
(168, 'Boğazlıyan', 66),
(169, 'Bolu', 14),
(170, 'Bolvadin', 3),
(171, 'Bor', 51),
(172, 'Borçka', 8),
(173, 'Bornova', 35),
(174, 'Boyabat', 57),
(175, 'Bozcaada', 17),
(176, 'Bozdoğan', 9),
(177, 'Bozkır', 42),
(178, 'Bozkurt', 20),
(179, 'Bozkurt', 37),
(180, 'Bozova', 63),
(181, 'Boztepe', 40),
(182, 'Bozüyük', 11),
(183, 'Bozyazı', 33),
(184, 'Buca', 35),
(185, 'Bucak', 15),
(186, 'Buharkent', 9),
(187, 'Bulancak', 28),
(188, 'Bulanık', 49),
(189, 'Buldan', 20),
(190, 'Burdur', 15),
(191, 'Burhaniye', 10),
(192, 'Bünyan', 38),
(193, 'Büyükçekmece', 34),
(194, 'Büyükorhan', 16),
(195, 'Canik', 55),
(196, 'Ceyhan', 1),
(197, 'Ceylanpınar', 63),
(198, 'Cide', 37),
(199, 'Cihanbeyli', 42),
(200, 'Cizre', 73),
(201, 'Cumayeri', 81),
(202, 'Çağlayancerit', 46),
(203, 'Çal', 20),
(204, 'Çaldıran', 65),
(205, 'Çamardı', 51),
(206, 'Çamaş', 52),
(207, 'Çameli', 20),
(208, 'Çamlıdere', 6),
(209, 'Çamlıhemşin', 53),
(210, 'Çamlıyayla', 33),
(211, 'Çamoluk', 28),
(212, 'Çan', 17),
(213, 'Çanakçı', 28),
(214, 'Çanakkale', 17),
(215, 'Çandır', 66),
(216, 'Çankaya', 6),
(217, 'Çankırı', 18),
(218, 'Çardak', 20),
(219, 'Çarşamba', 55),
(220, 'Çarşıbaşı', 61),
(221, 'Çat', 25),
(222, 'Çatak', 65),
(223, 'Çatalca', 34),
(224, 'Çatalpınar', 52),
(225, 'Çatalzeytin', 37),
(226, 'Çavdarhisar', 43),
(227, 'Çavdır', 15),
(228, 'Çay', 3),
(229, 'Çaybaşı', 52),
(230, 'Çaycuma', 67),
(231, 'Çayeli', 53),
(232, 'Çayıralan', 66),
(233, 'Çayırlı', 24),
(234, 'Çayırova', 41),
(235, 'Çaykara', 61),
(236, 'Çekerek', 66),
(237, 'Çekmeköy', 34),
(238, 'Çelebi', 71),
(239, 'Çelikhan', 2),
(240, 'Çeltik', 42),
(241, 'Çeltikçi', 15),
(242, 'Çemişgezek', 62),
(243, 'Çerkeş', 18),
(244, 'Çerkezköy', 59),
(245, 'Çermik', 21),
(246, 'Çeşme', 35),
(247, 'Çıldır', 75),
(248, 'Çınar', 21),
(249, 'Çınarcık', 77),
(250, 'Çiçekdağı', 40),
(251, 'Çifteler', 26),
(252, 'Çiftlik', 51),
(253, 'Çiftlikköy', 77),
(254, 'Çiğli', 35),
(255, 'Çilimli', 81),
(256, 'Çine', 9),
(257, 'Çivril', 20),
(258, 'Çobanlar', 3),
(259, 'Çorlu', 59),
(260, 'Çorum', 19),
(261, 'Çubuk', 6),
(262, 'Çukurca', 30),
(263, 'Çukurova', 1),
(264, 'Çumra', 42),
(265, 'Çüngüş', 21),
(266, 'Daday', 37),
(267, 'Dalaman', 48),
(268, 'Damal', 75),
(269, 'Darende', 44),
(270, 'Dargeçit', 47),
(271, 'Darıca', 41),
(272, 'Datça', 48),
(273, 'Dazkırı', 3),
(274, 'Defne', 31),
(275, 'Delice', 71),
(276, 'Demirci', 45),
(277, 'Demirköy', 39),
(278, 'Demirözü', 69),
(279, 'Demre', 7),
(280, 'Derbent', 42),
(281, 'Derebucak', 42),
(282, 'Dereli', 28),
(283, 'Derepazarı', 53),
(284, 'Derik', 47),
(285, 'Derince', 41),
(286, 'Derinkuyu', 50),
(287, 'Dernekpazarı', 61),
(288, 'Develi', 38),
(289, 'Devrek', 67),
(290, 'Devrekani', 37),
(291, 'Dicle', 21),
(292, 'Didim', 9),
(293, 'Digor', 36),
(294, 'Dikili', 35),
(295, 'Dikmen', 57),
(296, 'Dilovası', 41),
(297, 'Dinar', 3),
(298, 'Divriği', 58),
(299, 'Diyadin', 4),
(300, 'Dodurga', 19),
(301, 'Doğanhisar', 42),
(302, 'Doğankent', 28),
(303, 'Doğanşar', 58),
(304, 'Doğanşehir', 44),
(305, 'Doğanyol', 44),
(306, 'Doğanyurt', 37),
(307, 'Doğubayazıt', 4),
(308, 'Domaniç', 43),
(309, 'Dörtdivan', 14),
(310, 'Dörtyol', 31),
(311, 'Döşemealtı', 7),
(312, 'Dulkadiroğlu', 46),
(313, 'Dumlupınar', 43),
(314, 'Durağan', 57),
(315, 'Dursunbey', 10),
(316, 'Düzce', 81),
(317, 'Düziçi', 80),
(318, 'Düzköy', 61),
(319, 'Eceabat', 17),
(320, 'Edirne', 22),
(321, 'Edremit', 10),
(322, 'Edremit', 65),
(323, 'Efeler', 9),
(324, 'Eflani', 78),
(325, 'Eğil', 21),
(326, 'Eğirdir', 32),
(327, 'Ekinözü', 46),
(328, 'Elazığ', 23),
(329, 'Elbeyli', 79),
(330, 'Elbistan', 46),
(331, 'Eldivan', 18),
(332, 'Eleşkirt', 4),
(333, 'Elmadağ', 6),
(334, 'Elmalı', 7),
(335, 'Emet', 43),
(336, 'Emirdağ', 3),
(337, 'Emirgazi', 42),
(338, 'Enez', 22),
(339, 'Erbaa', 60),
(340, 'Erciş', 65),
(341, 'Erdek', 10),
(342, 'Erdemli', 33),
(343, 'Ereğli', 42),
(344, 'Ereğli', 67),
(345, 'Erenler', 54),
(346, 'Erfelek', 57),
(347, 'Ergani', 21),
(348, 'Ergene', 59),
(349, 'Ermenek', 70),
(350, 'Eruh', 56),
(351, 'Erzin', 31),
(352, 'Erzincan', 24),
(353, 'Esenler', 34),
(354, 'Esenyurt', 34),
(355, 'Eskil', 68),
(356, 'Eskipazar', 78),
(357, 'Espiye', 28),
(358, 'Eşme', 64),
(359, 'Etimesgut', 6),
(360, 'Evciler', 3),
(361, 'Evren', 6),
(362, 'Eynesil', 28),
(363, 'Eyüp', 34),
(364, 'Eyyübiye', 63),
(365, 'Ezine', 17),
(366, 'Fatih', 34),
(367, 'Fatsa', 52),
(368, 'Feke', 1),
(369, 'Felahiye', 38),
(370, 'Ferizli', 54),
(371, 'Fethiye', 48),
(372, 'Fındıklı', 53),
(373, 'Finike', 7),
(374, 'Foça', 35),
(375, 'Gaziemir', 35),
(376, 'Gaziosmanpaşa', 34),
(377, 'Gazipaşa', 7),
(378, 'Gebze', 41),
(379, 'Gediz', 43),
(380, 'Gelendost', 32),
(381, 'Gelibolu', 17),
(382, 'Gemerek', 58),
(383, 'Gemlik', 16),
(384, 'Genç', 12),
(385, 'Gercüş', 72),
(386, 'Gerede', 14),
(387, 'Gerger', 2),
(388, 'Germencik', 9),
(389, 'Gerze', 57),
(390, 'Gevaş', 65),
(391, 'Geyve', 54),
(392, 'Giresun', 28),
(393, 'Gökçeada', 17),
(394, 'Gökçebey', 67),
(395, 'Göksun', 46),
(396, 'Gölbaşı', 2),
(397, 'Gölbaşı', 6),
(398, 'Gölcük', 41),
(399, 'Göle', 75),
(400, 'Gölhisar', 15),
(401, 'Gölköy', 52),
(402, 'Gölmarmara', 45),
(403, 'Gölova', 58),
(404, 'Gölpazarı', 11),
(405, 'Gölyaka', 81),
(406, 'Gömeç', 10),
(407, 'Gönen', 10),
(408, 'Gönen', 32),
(409, 'Gördes', 45),
(410, 'Görele', 28),
(411, 'Göynücek', 5),
(412, 'Göynük', 14),
(413, 'Güce', 28),
(414, 'Güçlükonak', 73),
(415, 'Güdül', 6),
(416, 'Gülağaç', 68),
(417, 'Gülnar', 33),
(418, 'Gülşehir', 50),
(419, 'Gülyalı', 52),
(420, 'Gümüşhacıköy', 5),
(421, 'Gümüşhane', 29),
(422, 'Gümüşova', 81),
(423, 'Gündoğmuş', 7),
(424, 'Güney', 20),
(425, 'Güneysınır', 42),
(426, 'Güneysu', 53),
(427, 'Güngören', 34),
(428, 'Günyüzü', 26),
(429, 'Gürgentepe', 52),
(430, 'Güroymak', 13),
(431, 'Gürpınar', 65),
(432, 'Gürsu', 16),
(433, 'Gürün', 58),
(434, 'Güzelbahçe', 35),
(435, 'Güzelyurt', 68),
(436, 'Hacıbektaş', 50),
(437, 'Hacılar', 38),
(438, 'Hadim', 42),
(439, 'Hafik', 58),
(440, 'Hakkâri', 30),
(441, 'Halfeti', 63),
(442, 'Haliliye', 63),
(443, 'Halkapınar', 42),
(444, 'Hamamözü', 5),
(445, 'Hamur', 4),
(446, 'Han', 26),
(447, 'Hanak', 75),
(448, 'Hani', 21),
(449, 'Hanönü', 37),
(450, 'Harmancık', 16),
(451, 'Harran', 63),
(452, 'Hasanbeyli', 80),
(453, 'Hasankeyf', 72),
(454, 'Hasköy', 49),
(455, 'Hassa', 31),
(456, 'Havran', 10),
(457, 'Havsa', 22),
(458, 'Havza', 55),
(459, 'Haymana', 6),
(460, 'Hayrabolu', 59),
(461, 'Hayrat', 61),
(462, 'Hazro', 21),
(463, 'Hekimhan', 44),
(464, 'Hemşin', 53),
(465, 'Hendek', 54),
(466, 'Hınıs', 25),
(467, 'Hilvan', 63),
(468, 'Hisarcık', 43),
(469, 'Hizan', 13),
(470, 'Hocalar', 3),
(471, 'Honaz', 20),
(472, 'Hopa', 8),
(473, 'Horasan', 25),
(474, 'Hozat', 62),
(475, 'Hüyük', 42),
(476, 'Iğdır', 76),
(477, 'Ilgaz', 18),
(478, 'Ilgın', 42),
(479, 'Isparta', 32),
(480, 'İbradı', 7),
(481, 'İdil', 73),
(482, 'İhsangazi', 37),
(483, 'İhsaniye', 3),
(484, 'İkizce', 52),
(485, 'İkizdere', 53),
(486, 'İliç', 24),
(487, 'İlkadım', 55),
(488, 'İmamoğlu', 1),
(489, 'İmranlı', 58),
(490, 'İncesu', 38),
(491, 'İncirliova', 9),
(492, 'İnebolu', 37),
(493, 'İnegöl', 16),
(494, 'İnhisar', 11),
(495, 'İnönü', 26),
(496, 'İpekyolu', 65),
(497, 'İpsala', 22),
(498, 'İscehisar', 3),
(499, 'İskenderun', 31),
(500, 'İskilip', 19),
(501, 'İslahiye', 27),
(502, 'İspir', 25),
(503, 'İvrindi', 10),
(504, 'İyidere', 53),
(505, 'İzmit', 41),
(506, 'İznik', 16),
(507, 'Kabadüz', 52),
(508, 'Kabataş', 52),
(509, 'Kadıköy', 34),
(510, 'Kadınhanı', 42),
(511, 'Kadışehri', 66),
(512, 'Kadirli', 80),
(513, 'Kağıthane', 34),
(514, 'Kağızman', 36),
(515, 'Kahta', 2),
(516, 'Kale', 20),
(517, 'Kale', 44),
(518, 'Kalecik', 6),
(519, 'Kalkandere', 53),
(520, 'Kaman', 40),
(521, 'Kandıra', 41),
(522, 'Kangal', 58),
(523, 'Kapaklı', 59),
(524, 'Karabağlar', 35),
(525, 'Karaburun', 35),
(526, 'Karabük', 78),
(527, 'Karacabey', 16),
(528, 'Karacasu', 9),
(529, 'Karaçoban', 25),
(530, 'Karahallı', 64),
(531, 'Karaisalı', 1),
(532, 'Karakeçili', 71),
(533, 'Karakoçan', 23),
(534, 'Karakoyunlu', 76),
(535, 'Karaköprü', 63),
(536, 'Karaman', 70),
(537, 'Karamanlı', 15),
(538, 'Karamürsel', 41),
(539, 'Karapınar', 42),
(540, 'Karapürçek', 54),
(541, 'Karasu', 54),
(542, 'Karataş', 1),
(543, 'Karatay', 42),
(544, 'Karayazı', 25),
(545, 'Karesi', 10),
(546, 'Kargı', 19),
(547, 'Karkamış', 27),
(548, 'Karlıova', 12),
(549, 'Karpuzlu', 9),
(550, 'Kars', 36),
(551, 'Karşıyaka', 35),
(552, 'Kartal', 34),
(553, 'Kartepe', 41),
(554, 'Kastamonu', 37),
(555, 'Kaş', 7),
(556, 'Kavak', 55),
(557, 'Kavaklıdere', 48),
(558, 'Kayapınar', 21),
(559, 'Kaynarca', 54),
(560, 'Kaynaşlı', 81),
(561, 'Kazan', 6),
(562, 'Kazımkarabekir', 70),
(563, 'Keban', 23),
(564, 'Keçiborlu', 32),
(565, 'Keçiören', 6),
(566, 'Keles', 16),
(567, 'Kelkit', 29),
(568, 'Kemah', 24),
(569, 'Kemaliye', 24),
(570, 'Kemalpaşa', 35),
(571, 'Kemer', 7),
(572, 'Kemer', 15),
(573, 'Kepez', 7),
(574, 'Kepsut', 10),
(575, 'Keskin', 71),
(576, 'Kestel', 16),
(577, 'Keşan', 22),
(578, 'Keşap', 28),
(579, 'Kıbrıscık', 14),
(580, 'Kınık', 35),
(581, 'Kırıkhan', 31),
(582, 'Kırıkkale', 71),
(583, 'Kırkağaç', 45),
(584, 'Kırklareli', 39),
(585, 'Kırşehir', 40),
(586, 'Kızılcahamam', 6),
(587, 'Kızılırmak', 18),
(588, 'Kızılören', 3),
(589, 'Kızıltepe', 47),
(590, 'Kiğı', 12),
(591, 'Kilimli', 67),
(592, 'Kilis', 79),
(593, 'Kiraz', 35),
(594, 'Kocaali', 54),
(595, 'Kocaköy', 21),
(596, 'Kocasinan', 38),
(597, 'Koçarlı', 9),
(598, 'Kofçaz', 39),
(599, 'Konak', 35),
(600, 'Konyaaltı', 7),
(601, 'Korgan', 52),
(602, 'Korgun', 18),
(603, 'Korkut', 49),
(604, 'Korkuteli', 7),
(605, 'Kovancılar', 23),
(606, 'Koyulhisar', 58),
(607, 'Kozaklı', 50),
(608, 'Kozan', 1),
(609, 'Kozlu', 67),
(610, 'Kozluk', 72),
(611, 'Köprübaşı', 45),
(612, 'Köprübaşı', 61),
(613, 'Köprüköy', 25),
(614, 'Körfez', 41),
(615, 'Köse', 29),
(616, 'Köşk', 9),
(617, 'Köyceğiz', 48),
(618, 'Kula', 45),
(619, 'Kulp', 21),
(620, 'Kulu', 42),
(621, 'Kuluncak', 44),
(622, 'Kumlu', 31),
(623, 'Kumluca', 7),
(624, 'Kumru', 52),
(625, 'Kurşunlu', 18),
(626, 'Kurtalan', 56),
(627, 'Kurucaşile', 74),
(628, 'Kuşadası', 9),
(629, 'Kuyucak', 9),
(630, 'Küçükçekmece', 34),
(631, 'Küre', 37),
(632, 'Kürtün', 29),
(633, 'Kütahya', 43),
(634, 'Laçin', 19),
(635, 'Ladik', 55),
(636, 'Lalapaşa', 22),
(637, 'Lapseki', 17),
(638, 'Lice', 21),
(639, 'Lüleburgaz', 39),
(640, 'Maçka', 61),
(641, 'Maden', 23),
(642, 'Mahmudiye', 26),
(643, 'Malazgirt', 49),
(644, 'Malkara', 59),
(645, 'Maltepe', 34),
(646, 'Mamak', 6),
(647, 'Manavgat', 7),
(648, 'Manyas', 10),
(649, 'Marmara', 10),
(650, 'Marmaraereğlisi', 59),
(651, 'Marmaris', 48),
(652, 'Mazgirt', 62),
(653, 'Mazıdağı', 47),
(654, 'Mecitözü', 19),
(655, 'Melikgazi', 38),
(656, 'Menderes', 35),
(657, 'Menemen', 35),
(658, 'Mengen', 14),
(659, 'Menteşe', 48),
(660, 'Meram', 42),
(661, 'Meriç', 22),
(662, 'Merkezefendi', 20),
(663, 'Merzifon', 5),
(664, 'Mesudiye', 52),
(665, 'Mezitli', 33),
(666, 'Midyat', 47),
(667, 'Mihalgazi', 26),
(668, 'Mihalıççık', 26),
(669, 'Milas', 48),
(670, 'Mucur', 40),
(671, 'Mudanya', 16),
(672, 'Mudurnu', 14),
(673, 'Muradiye', 65),
(674, 'Muratlı', 59),
(675, 'Muratpaşa', 7),
(676, 'Murgul', 8),
(677, 'Musabeyli', 79),
(678, 'Mustafakemalpaşa', 16),
(679, 'Muş', 49),
(680, 'Mut', 33),
(681, 'Mutki', 13),
(682, 'Nallıhan', 6),
(683, 'Narlıdere', 35),
(684, 'Narman', 25),
(685, 'Nazımiye', 62),
(686, 'Nazilli', 9),
(687, 'Nevşehir', 50),
(688, 'Niğde', 51),
(689, 'Niksar', 60),
(690, 'Nilüfer', 16),
(691, 'Nizip', 27),
(692, 'Nurdağı', 27),
(693, 'Nurhak', 46),
(694, 'Nusaybin', 47),
(695, 'Odunpazarı', 26),
(696, 'Of', 61),
(697, 'Oğuzeli', 27),
(698, 'Oğuzlar', 19),
(699, 'Oltu', 25),
(700, 'Olur', 25),
(701, 'Ondokuzmayıs', 55),
(702, 'Onikişubat', 46),
(703, 'Orhaneli', 16),
(704, 'Orhangazi', 16),
(705, 'Orta', 18),
(706, 'Ortaca', 48),
(707, 'Ortahisar', 61),
(708, 'Ortaköy', 68),
(709, 'Ortaköy', 19),
(710, 'Osmancık', 19),
(711, 'Osmaneli', 11),
(712, 'Osmangazi', 16),
(713, 'Osmaniye', 80),
(714, 'Otlukbeli', 24),
(715, 'Ovacık', 78),
(716, 'Ovacık', 62),
(717, 'Ödemiş', 35),
(718, 'Ömerli', 47),
(719, 'Özalp', 65),
(720, 'Özvatan', 38),
(721, 'Palandöken', 25),
(722, 'Palu', 23),
(723, 'Pamukkale', 20),
(724, 'Pamukova', 54),
(725, 'Pasinler', 25),
(726, 'Patnos', 4),
(727, 'Payas', 31),
(728, 'Pazar', 53),
(729, 'Pazar', 60),
(730, 'Pazarcık', 46),
(731, 'Pazarlar', 43),
(732, 'Pazaryeri', 11),
(733, 'Pazaryolu', 25),
(734, 'Pehlivanköy', 39),
(735, 'Pendik', 34),
(736, 'Perşembe', 52),
(737, 'Pertek', 62),
(738, 'Pervari', 56),
(739, 'Pınarbaşı', 37),
(740, 'Pınarbaşı', 38),
(741, 'Pınarhisar', 39),
(742, 'Piraziz', 28),
(743, 'Polateli', 79),
(744, 'Polatlı', 6),
(745, 'Posof', 75),
(746, 'Pozantı', 1),
(747, 'Pursaklar', 6),
(748, 'Pülümür', 62),
(749, 'Pütürge', 44),
(750, 'Refahiye', 24),
(751, 'Reşadiye', 60),
(752, 'Reyhanlı', 31),
(753, 'Rize', 53),
(754, 'Safranbolu', 78),
(755, 'Saimbeyli', 1),
(756, 'Salıpazarı', 55),
(757, 'Salihli', 45),
(758, 'Samandağ', 31),
(759, 'Samsat', 2),
(760, 'Sancaktepe', 34),
(761, 'Sandıklı', 3),
(762, 'Sapanca', 54),
(763, 'Saray', 59),
(764, 'Saray', 65),
(765, 'Saraydüzü', 57),
(766, 'Saraykent', 66),
(767, 'Sarayköy', 20),
(768, 'Sarayönü', 42),
(769, 'Sarıcakaya', 26),
(770, 'Sarıçam', 1),
(771, 'Sarıgöl', 45),
(772, 'Sarıkamış', 36),
(773, 'Sarıkaya', 66),
(774, 'Sarıoğlan', 38),
(775, 'Sarıveliler', 70),
(776, 'Sarıyahşi', 68),
(777, 'Sarıyer', 34),
(778, 'Sarız', 38),
(779, 'Saruhanlı', 45),
(780, 'Sason', 72),
(781, 'Savaştepe', 10),
(782, 'Savur', 47),
(783, 'Seben', 14),
(784, 'Seferihisar', 35),
(785, 'Selçuk', 35),
(786, 'Selçuklu', 42),
(787, 'Selendi', 45),
(788, 'Selim', 36),
(789, 'Senirkent', 32),
(790, 'Serdivan', 54),
(791, 'Serik', 7),
(792, 'Serinhisar', 20),
(793, 'Seydikemer', 48),
(794, 'Seydiler', 37),
(795, 'Seydişehir', 42),
(796, 'Seyhan', 1),
(797, 'Seyitgazi', 26),
(798, 'Sındırgı', 10),
(799, 'Siirt', 56),
(800, 'Silifke', 33),
(801, 'Silivri', 34),
(802, 'Silopi', 73),
(803, 'Silvan', 21),
(804, 'Simav', 43),
(805, 'Sinanpaşa', 3),
(806, 'Sincan', 6),
(807, 'Sincik', 2),
(808, 'Sinop', 57),
(809, 'Sivas', 58),
(810, 'Sivaslı', 64),
(811, 'Siverek', 63),
(812, 'Sivrice', 23),
(813, 'Sivrihisar', 26),
(814, 'Solhan', 12),
(815, 'Soma', 45),
(816, 'Sorgun', 66),
(817, 'Söğüt', 11),
(818, 'Söğütlü', 54),
(819, 'Söke', 9),
(820, 'Sulakyurt', 71),
(821, 'Sultanbeyli', 34),
(822, 'Sultandağı', 3),
(823, 'Sultangazi', 34),
(824, 'Sultanhisar', 9),
(825, 'Suluova', 5),
(826, 'Sulusaray', 60),
(827, 'Sumbas', 80),
(828, 'Sungurlu', 19),
(829, 'Sur', 21),
(830, 'Suruç', 63),
(831, 'Susurluk', 10),
(832, 'Susuz', 36),
(833, 'Suşehri', 58),
(834, 'Süleymanpaşa', 59),
(835, 'Süloğlu', 22),
(836, 'Sürmene', 61),
(837, 'Sütçüler', 32),
(838, 'Şabanözü', 18),
(839, 'Şahinbey', 27),
(840, 'Şalpazarı', 61),
(841, 'Şaphane', 43),
(842, 'Şarkışla', 58),
(843, 'Şarkikaraağaç', 32),
(844, 'Şarköy', 59),
(845, 'Şavşat', 8),
(846, 'Şebinkarahisar', 28),
(847, 'Şefaatli', 66),
(848, 'Şehitkamil', 27),
(849, 'Şehzadeler', 45),
(850, 'Şemdinli', 30),
(851, 'Şenkaya', 25),
(852, 'Şenpazar', 37),
(853, 'Şereflikoçhisar', 6),
(854, 'Şırnak', 73),
(855, 'Şile', 34),
(856, 'Şiran', 29),
(857, 'Şirvan', 56),
(858, 'Şişli', 34),
(859, 'Şuhut', 3),
(860, 'Talas', 38),
(861, 'Taraklı', 54),
(862, 'Tarsus', 33),
(863, 'Taşkent', 42),
(864, 'Taşköprü', 37),
(865, 'Taşlıçay', 4),
(866, 'Taşova', 5),
(867, 'Tatvan', 13),
(868, 'Tavas', 20),
(869, 'Tavşanlı', 43),
(870, 'Tefenni', 15),
(871, 'Tekkeköy', 55),
(872, 'Tekman', 25),
(873, 'Tepebaşı', 26),
(874, 'Tercan', 24),
(875, 'Termal', 77),
(876, 'Terme', 55),
(877, 'Tillo', 56),
(878, 'Tire', 35),
(879, 'Tirebolu', 28),
(880, 'Tokat', 60),
(881, 'Tomarza', 38),
(882, 'Tonya', 61),
(883, 'Toprakkale', 80),
(884, 'Torbalı', 35),
(885, 'Toroslar', 33),
(886, 'Tortum', 25),
(887, 'Torul', 29),
(888, 'Tosya', 37),
(889, 'Tufanbeyli', 1),
(890, 'Tunceli', 62),
(891, 'Turgutlu', 45),
(892, 'Turhal', 60),
(893, 'Tuşba', 65),
(894, 'Tut', 2),
(895, 'Tutak', 4),
(896, 'Tuzla', 34),
(897, 'Tuzluca', 76),
(898, 'Tuzlukçu', 42),
(899, 'Türkeli', 57),
(900, 'Türkoğlu', 46),
(901, 'Uğurludağ', 19),
(902, 'Ula', 48),
(903, 'Ulaş', 58),
(904, 'Ulubey', 52),
(905, 'Ulubey', 64),
(906, 'Uluborlu', 32),
(907, 'Uludere', 73),
(908, 'Ulukışla', 51),
(909, 'Ulus', 74),
(910, 'Urla', 35),
(911, 'Uşak', 64),
(912, 'Uzundere', 25),
(913, 'Uzunköprü', 22),
(914, 'Ümraniye', 34),
(915, 'Ünye', 52),
(916, 'Ürgüp', 50),
(917, 'Üsküdar', 34),
(918, 'Üzümlü', 24),
(919, 'Vakfıkebir', 61),
(920, 'Varto', 49),
(921, 'Vezirköprü', 55),
(922, 'Viranşehir', 63),
(923, 'Vize', 39),
(924, 'Yağlıdere', 28),
(925, 'Yahşihan', 71),
(926, 'Yahyalı', 38),
(927, 'Yakakent', 55),
(928, 'Yakutiye', 25),
(929, 'Yalıhüyük', 42),
(930, 'Yalova', 77),
(931, 'Yalvaç', 32),
(932, 'Yapraklı', 18),
(933, 'Yatağan', 48),
(934, 'Yavuzeli', 27),
(935, 'Yayladağı', 31),
(936, 'Yayladere', 12),
(937, 'Yazıhan', 44),
(938, 'Yedisu', 12),
(939, 'Yenice', 17),
(940, 'Yenice', 78),
(941, 'Yeniçağa', 14),
(942, 'Yenifakılı', 66),
(943, 'Yenimahalle', 6),
(944, 'Yenipazar', 9),
(945, 'Yenipazar', 11),
(946, 'Yenişarbademli', 32),
(947, 'Yenişehir', 16),
(948, 'Yenişehir', 21),
(949, 'Yenişehir', 33),
(950, 'Yerköy', 66),
(951, 'Yeşilhisar', 38),
(952, 'Yeşilli', 47),
(953, 'Yeşilova', 15),
(954, 'Yeşilyurt', 44),
(955, 'Yeşilyurt', 60),
(956, 'Yığılca', 81),
(957, 'Yıldırım', 16),
(958, 'Yıldızeli', 58),
(959, 'Yomra', 61),
(960, 'Yozgat', 66),
(961, 'Yumurtalık', 1),
(962, 'Yunak', 42),
(963, 'Yunusemre', 45),
(964, 'Yusufeli', 8),
(965, 'Yüksekova', 30),
(966, 'Yüreğir', 1),
(967, 'Zara', 58),
(968, 'Zeytinburnu', 34),
(969, 'Zile', 60),
(970, 'Zonguldak', 67),
(971, 'Kemalpaşa', 8),
(972, 'Sultanhanı', 68);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `iller`
--

CREATE TABLE `iller` (
  `il_no` int(11) NOT NULL,
  `isim` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `iller`
--

INSERT INTO `iller` (`il_no`, `isim`) VALUES
(1, 'Adana'),
(2, 'Adıyaman'),
(3, 'Afyonkarahisar'),
(4, 'Ağrı'),
(5, 'Amasya'),
(6, 'Ankara'),
(7, 'Antalya'),
(8, 'Artvin'),
(9, 'Aydın'),
(10, 'Balıkesir'),
(11, 'Bilecik'),
(12, 'Bingöl'),
(13, 'Bitlis'),
(14, 'Bolu'),
(15, 'Burdur'),
(16, 'Bursa'),
(17, 'Çanakkale'),
(18, 'Çankırı'),
(19, 'Çorum'),
(20, 'Denizli'),
(21, 'Diyarbakır'),
(22, 'Edirne'),
(23, 'Elâzığ'),
(24, 'Erzincan'),
(25, 'Erzurum'),
(26, 'Eskişehir'),
(27, 'Gaziantep'),
(28, 'Giresun'),
(29, 'Gümüşhane'),
(30, 'Hakkâri'),
(31, 'Hatay'),
(32, 'Isparta'),
(33, 'Mersin'),
(34, 'İstanbul'),
(35, 'İzmir'),
(36, 'Kars'),
(37, 'Kastamonu'),
(38, 'Kayseri'),
(39, 'Kırklareli'),
(40, 'Kırşehir'),
(41, 'Kocaeli'),
(42, 'Konya'),
(43, 'Kütahya'),
(44, 'Malatya'),
(45, 'Manisa'),
(46, 'Kahramanmaraş'),
(47, 'Mardin'),
(48, 'Muğla'),
(49, 'Muş'),
(50, 'Nevşehir'),
(51, 'Niğde'),
(52, 'Ordu'),
(53, 'Rize'),
(54, 'Sakarya'),
(55, 'Samsun'),
(56, 'Siirt'),
(57, 'Sinop'),
(58, 'Sivas'),
(59, 'Tekirdağ'),
(60, 'Tokat'),
(61, 'Trabzon'),
(62, 'Tunceli'),
(63, 'Şanlıurfa'),
(64, 'Uşak'),
(65, 'Van'),
(66, 'Yozgat'),
(67, 'Zonguldak'),
(68, 'Aksaray'),
(69, 'Bayburt'),
(70, 'Karaman'),
(71, 'Kırıkkale'),
(72, 'Batman'),
(73, 'Şırnak'),
(74, 'Bartın'),
(75, 'Ardahan'),
(76, 'Iğdır'),
(77, 'Yalova'),
(78, 'Karabük'),
(79, 'Kilis'),
(80, 'Osmaniye'),
(81, 'Düzce');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kasaekle`
--

CREATE TABLE `kasaekle` (
  `id` int(11) NOT NULL,
  `kasakodu` varchar(255) NOT NULL,
  `kasa_adi` varchar(255) NOT NULL,
  `aciklama` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kasagiderleri`
--

CREATE TABLE `kasagiderleri` (
  `id` int(11) NOT NULL,
  `odenen_miktar` varchar(255) NOT NULL,
  `ekleme_tarihi` varchar(255) NOT NULL,
  `odeme_tarih` varchar(255) NOT NULL,
  `kesidici` varchar(255) NOT NULL,
  `muhattap` varchar(255) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `banka_hesabi` int(11) NOT NULL,
  `banka_adi` varchar(255) NOT NULL,
  `odemeturu` varchar(255) NOT NULL,
  `hesapturu` varchar(255) NOT NULL,
  `kasaadi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `ad_soyad` varchar(255) NOT NULL,
  `yapilan_islem` varchar(300) NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `logs`
--

INSERT INTO `logs` (`id`, `ad_soyad`, `yapilan_islem`, `tarih`, `ip`) VALUES
(1, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '26.03.18 - 19:18:17', '85.102.176.240'),
(2, 'Emre Çelik', 'Sisteme giriş yapıldı.', '26.03.18 - 19:24:47', '85.102.176.240'),
(3, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '26.03.18 - 19:24:51', '85.102.176.240'),
(4, 'Emre Çelik', 'Sisteme giriş yapıldı.', '26.03.18 - 19:26:36', '85.102.176.240'),
(5, 'Emre Çelik', 'Sisteme giriş yapıldı.', '26.03.18 - 19:36:45', '85.102.176.240'),
(6, ' ', 'Sisteme giriş yapıldı.', '26.03.18 - 19:39:14', '85.102.176.240'),
(7, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 08:38:45', '176.233.94.20'),
(8, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 10:49:09', '85.102.176.240'),
(9, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 11:28:54', '176.218.111.133'),
(10, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:29:40', '176.218.111.133'),
(11, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 11:30:31', '176.218.111.133'),
(12, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:31:12', '176.218.111.133'),
(13, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 11:31:24', '176.218.111.133'),
(14, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:33:37', '176.218.111.133'),
(15, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 11:33:53', '176.218.111.133'),
(16, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:37:51', '176.218.111.133'),
(17, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 11:38:00', '176.218.111.133'),
(18, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:38:06', '176.218.111.133'),
(19, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 11:38:20', '176.218.111.133'),
(20, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:52:39', '176.218.111.133'),
(21, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 11:54:13', '176.218.111.133'),
(22, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 11:59:30', '176.218.111.133'),
(23, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 11:59:48', '176.218.111.133'),
(24, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 12:02:12', '176.218.111.133'),
(25, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 12:02:36', '176.218.111.133'),
(26, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 12:02:46', '176.218.111.133'),
(27, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 12:02:55', '176.218.111.133'),
(28, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 12:06:00', '176.218.111.133'),
(29, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 12:06:19', '176.218.111.133'),
(30, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 12:06:52', '176.218.111.133'),
(31, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 12:12:06', '176.218.111.133'),
(32, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 12:36:26', '178.243.84.226'),
(33, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 13:41:04', '88.244.226.183'),
(34, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 13:50:28', '85.96.161.208'),
(35, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 14:02:39', '88.244.226.183'),
(36, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 14:06:38', '85.96.161.208'),
(37, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 14:29:57', '88.244.226.183'),
(38, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 14:30:37', '88.244.226.183'),
(39, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 14:32:32', '88.244.226.183'),
(40, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 14:38:41', '88.244.226.183'),
(41, 'Bektaş Öztürk', 'Sisteme giriş yapıldı.', '27.03.18 - 14:38:45', '88.244.226.183'),
(42, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 15:58:03', '176.218.111.133'),
(43, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 17:09:56', '176.218.111.133'),
(44, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 17:10:02', '176.218.111.133'),
(45, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 17:10:08', '176.218.111.133'),
(46, ' ', 'Sistemden çıkış yapıldı.', '27.03.18 - 17:28:04', '176.218.111.133'),
(47, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 19:13:45', '85.96.161.208'),
(48, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '27.03.18 - 19:13:51', '85.96.161.208'),
(49, 'Emre Çelik', 'Sisteme giriş yapıldı.', '27.03.18 - 21:12:49', '85.102.176.240'),
(50, ' ', 'Sisteme giriş yapıldı.', '27.03.18 - 21:13:40', '85.102.176.240'),
(51, 'Emre Çelik', 'Sisteme giriş yapıldı.', '28.03.18 - 15:46:09', '::1'),
(52, 'Emre Çelik', 'Sistemden çıkış yapıldı.', '28.03.18 - 15:57:02', '::1'),
(53, 'Emre Çelik', 'Sisteme giriş yapıldı.', '28.03.18 - 16:00:50', '::1'),
(54, 'Emre Çelik', 'Sisteme giriş yapıldı.', '28.03.18 - 17:14:34', '::1'),
(55, 'Emre Çelik', 'Sisteme giriş yapıldı.', '29.03.18 - 10:54:09', '::1'),
(56, 'Emre Çelik', 'Sisteme giriş yapıldı.', '29.03.18 - 14:51:43', '::1'),
(57, 'Emre Çelik', 'Sisteme giriş yapıldı.', '30.03.18 - 11:50:42', '::1'),
(58, 'Emre Çelik', 'Sisteme giriş yapıldı.', '30.03.18 - 15:03:31', '::1'),
(59, 'Emre Çelik', 'Sisteme giriş yapıldı.', '31.03.18 - 12:51:48', '::1');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `malzemestok`
--

CREATE TABLE `malzemestok` (
  `id` int(11) NOT NULL,
  `urun_kodu` varchar(255) NOT NULL,
  `urun_id` int(11) NOT NULL,
  `urun_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `mesajlasmalar`
--

CREATE TABLE `mesajlasmalar` (
  `id` int(11) NOT NULL,
  `mesaj_id` int(11) NOT NULL,
  `mesaj_gonderen_id` int(11) NOT NULL,
  `mesaj_alan_id` int(11) NOT NULL,
  `mesaj_tarihi` varchar(255) NOT NULL,
  `mesaj_icerik` text NOT NULL,
  `goruldu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `musteriler`
--

CREATE TABLE `musteriler` (
  `musteri_id` int(11) NOT NULL,
  `musteri_adi` varchar(255) NOT NULL,
  `musteri_soyadi` varchar(255) NOT NULL,
  `musteri_kodu` varchar(255) NOT NULL,
  `musteri_telefon` varchar(255) NOT NULL,
  `musteri_fax` varchar(255) NOT NULL,
  `musteri_ulke` varchar(255) NOT NULL,
  `musteri_il` varchar(255) NOT NULL,
  `musteri_ilce` varchar(255) NOT NULL,
  `musteri_mail` varchar(255) NOT NULL,
  `musteri_iskot` varchar(255) NOT NULL,
  `musteri_cari` float NOT NULL,
  `musteri_ceklimit` float NOT NULL,
  `musteri_senetlimit` float NOT NULL,
  `musteri_nakitlimit` float NOT NULL,
  `musteri_turu` int(11) NOT NULL,
  `musteri_adres` text NOT NULL,
  `musteri_unvani` varchar(255) NOT NULL,
  `musteri_vergino` varchar(255) NOT NULL,
  `musteri_il1` varchar(255) NOT NULL,
  `musteri_ilce1` varchar(255) NOT NULL,
  `musteri_vergidairesi` varchar(255) NOT NULL,
  `musteri_adres1` text NOT NULL,
  `firma_adi` varchar(255) NOT NULL,
  `musteri_kuladi` varchar(255) NOT NULL,
  `musteri_sifre` varchar(255) NOT NULL,
  `uye_turu` int(11) NOT NULL,
  `gorevlipazarlama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `musteriler`
--

INSERT INTO `musteriler` (`musteri_id`, `musteri_adi`, `musteri_soyadi`, `musteri_kodu`, `musteri_telefon`, `musteri_fax`, `musteri_ulke`, `musteri_il`, `musteri_ilce`, `musteri_mail`, `musteri_iskot`, `musteri_cari`, `musteri_ceklimit`, `musteri_senetlimit`, `musteri_nakitlimit`, `musteri_turu`, `musteri_adres`, `musteri_unvani`, `musteri_vergino`, `musteri_il1`, `musteri_ilce1`, `musteri_vergidairesi`, `musteri_adres1`, `firma_adi`, `musteri_kuladi`, `musteri_sifre`, `uye_turu`, `gorevlipazarlama`) VALUES
(1, 'Bekir', 'ERTÜRK', '47418-1', '5073746141', '5076745141', 'Türkiye', 'İstanbul', 'Bağcılar', 'bekirsadikerturk@gmail.com', '', 5000, 20000, 20000, 20000, 0, 'sadfasdfasdasdf', 'Kodifix Bilişim Hizmetleri', '3750288556', 'İSTANBUL', 'FATİH', 'FATİH', 'BEASDFASDFASDFDAS', 'Kodifix', 'bekir', '123', 0, '43');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `odemeler`
--

CREATE TABLE `odemeler` (
  `id` int(11) NOT NULL,
  `musteri_id` int(11) NOT NULL,
  `odenen_miktar` float NOT NULL,
  `ekleme_tarihi` varchar(255) NOT NULL,
  `odeme_tarih` varchar(255) NOT NULL,
  `kesidici` varchar(255) NOT NULL,
  `muhattap` varchar(255) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `banka_hesabi` int(11) NOT NULL,
  `banka_adi` varchar(255) NOT NULL,
  `odemeturu` varchar(255) NOT NULL,
  `tursec` varchar(255) NOT NULL,
  `durum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `pazarlamacigider`
--

CREATE TABLE `pazarlamacigider` (
  `id` int(11) NOT NULL,
  `pazarlamaci_id` int(11) NOT NULL,
  `tarih` text NOT NULL,
  `giderfiyat` int(11) NOT NULL,
  `gideraciklama` text NOT NULL,
  `tur` varchar(255) NOT NULL,
  `odemesekli` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `rutbelerim`
--

CREATE TABLE `rutbelerim` (
  `id` int(11) NOT NULL,
  `adi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `rutbelerim`
--

INSERT INTO `rutbelerim` (`id`, `adi`) VALUES
(1, 'Yönetici'),
(2, 'Pazarlamacı'),
(3, 'Muhasebe'),
(4, 'Depo');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `siparisler`
--

CREATE TABLE `siparisler` (
  `siparis_id` int(11) NOT NULL,
  `urun_id` int(11) NOT NULL,
  `musteri_id` int(11) NOT NULL,
  `uye_id` int(11) NOT NULL,
  `tarih` varchar(255) NOT NULL,
  `onay` int(11) NOT NULL,
  `birim` varchar(255) NOT NULL,
  `adet` varchar(255) NOT NULL,
  `satis_onay` int(11) NOT NULL,
  `muhasebe_onay` int(11) NOT NULL,
  `fiyat` float NOT NULL,
  `iskonto` int(11) NOT NULL,
  `teslim` int(11) NOT NULL,
  `bekleme` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `siparisler`
--

INSERT INTO `siparisler` (`siparis_id`, `urun_id`, `musteri_id`, `uye_id`, `tarih`, `onay`, `birim`, `adet`, `satis_onay`, `muhasebe_onay`, `fiyat`, `iskonto`, `teslim`, `bekleme`) VALUES
(1, 17, 1, 31, '27.03.2018 14:18:33', 1, 'adet', '5', 1, 2, 21.89, 0, 1, 0),
(2, 15, 1, 31, '27.03.2018 14:18:42', 1, 'adet', '45', 1, 2, 4, 0, 1, 0),
(3, 15, 1, 0, '27.03.2018 17:23:47', 1, 'adet', '10', 1, 2, 4, 5, 0, 0);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `tedarikcifirmalar`
--

CREATE TABLE `tedarikcifirmalar` (
  `id` int(11) NOT NULL,
  `urun_adi` int(11) NOT NULL,
  `birim` varchar(255) NOT NULL,
  `adet` varchar(255) NOT NULL,
  `fiyat` int(11) NOT NULL,
  `kdv` int(11) NOT NULL,
  `firma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uretimler`
--

CREATE TABLE `uretimler` (
  `uretim_id` int(11) NOT NULL,
  `uretim_kodu` varchar(255) NOT NULL,
  `uretim_seriadi` varchar(11) NOT NULL,
  `urun_adet` int(11) NOT NULL,
  `urun_aciklamasi` text NOT NULL,
  `urun_id` int(11) NOT NULL,
  `onay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunler`
--

CREATE TABLE `urunler` (
  `urun_id` int(11) NOT NULL,
  `urun_kodu` varchar(255) NOT NULL,
  `urun_adi` varchar(255) NOT NULL,
  `alis_fiyati` float NOT NULL,
  `satis_fiyati` float NOT NULL,
  `urun_serisi` varchar(255) NOT NULL,
  `tedarikci_firma` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `kdv_orani` int(11) NOT NULL,
  `urun_yazisi` text NOT NULL,
  `resim` text NOT NULL,
  `birim` varchar(255) NOT NULL,
  `sonfiyat` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `urunler`
--

INSERT INTO `urunler` (`urun_id`, `urun_kodu`, `urun_adi`, `alis_fiyati`, `satis_fiyati`, `urun_serisi`, `tedarikci_firma`, `stok`, `kdv_orani`, `urun_yazisi`, `resim`, `birim`, `sonfiyat`) VALUES
(3, '83235-1', 'Banyo Bataryası', 1, 2, 'Abronya Serisi', 'Kodifix', 1000, 1, 'Banyo Batarya hakkında yazı', '525e2dd557bea3fa3855fa6c80d6cc5c.jpg', 'adet', 2),
(4, '33823-2', 'Lavabo Bataryası', 700, 2, 'Açelya Serisi', 'Kodifix', 700, 0, 'Lavabo Bataryası hakkında yazı', 'cfd23ef409740a0c47320224ac46f159.jpg', 'adet', 2),
(5, '65073-1', 'Kuğu Evye Bataryası', 600, 1, 'Akasya Serisi', 'Kodifix', 520, 2, 'Kuğu Evye Bataryası hakkında yazı', '099727dae2bb298ab5834fd2516e2a87.jpg', 'adet', 1),
(6, '78199-', 'Kuğu Lavabo Bataryası', 1, 3, 'Defne Serisi', 'Kodifix', 2, 3, 'Kuğu Lavabo Bataryası hakkında yazı', '29e338b34f66ecc33efd9acfcd67594b.jpg', 'adet', 3),
(7, '26144-3', 'Banyo Bataryası', 3, 4, 'Fulya serisi', 'Kodifix', 2, 2, 'Banyo Bataryası', 'ca00459363ae054ebfa4b6712524a6cc.jpg', 'kg', 4),
(8, '52124-4', 'Lavabo Bataryası', 1, 3, 'Fotoselli Serisi', 'Kodifix', 1, 4, 'Lavabo Bataryası hakkında yazı', 'a58cc5572ff2886bda8ecc99a1f6ebe9.jpg', 'm3', 3),
(9, '75993-2', 'Kuğu Evye Bataryası', 2, 4, 'Endüstriyel Serisi', 'Kodifix', 2, 5, 'Kuğu Evye Bataryası', 'bf0e914b9b9ed393ae2b7364bde4fac3.jpg', 'kg', 4),
(10, '78533-5', 'Kuğu Lavabo Bataryası', 2, 3, 'Sipiralli Serisi', 'Kodifix', 30, 5, 'Kuğu Lavabo Bataryası hakkında yazı', 'aaf83704ab85002e5bf50bd7bbdb21db.jpg', 'paket', 3),
(12, '33815-2', 'Lavabo Bataryası', 5, 10, 'Mimoza Serisi', 'Kodifix', 15, 10, 'Lavabo Bataryası hakkında yazı', '7670b2812916dc4e891ecc3be87da6a0.jpg', 'adet', 11),
(13, '23470-6', 'Kuğu Evye Bataryası', 3, 7, 'Gardenya Serisi', 'Kodifix', 510, 10, 'Kuğu Evye Bataryası', '5e622ce777890fa18439baa0e90b154e.jpg', 'adet', 8),
(14, '54366-11', 'Kuğu Lavabo Bataryası', 5, 10, 'Mimoza Serisi', 'Kodifix', 40, 15, 'Kuğu Lavabo Bataryası hakkında yazı', '7896b6469d37361aac3af47b9393a6c9.jpg', 'adet', 12),
(15, '90396-3', 'Lavabo Bataryası', 3, 4, 'Gardenya Serisi', 'Kodifix', 50, 12, 'Lavabo Bataryası hakkında yazı', 'b4b19dba7b263828893dd4f094ce4038.jpg', 'adet', 4),
(17, '47715-ea', 'Kuğu Lavabo Bataryası', 20.4, 19.9, 'Sipiralli Serisi', 'akiş', 8000, 10, 'Üründe çizik yoktur.', '4351fa90a0a6b2a4edb4c86b15a54650.jpg', 'kg', 21.89);

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunmodulu`
--

CREATE TABLE `urunmodulu` (
  `id` int(11) NOT NULL,
  `urun_modulkodu` varchar(255) NOT NULL,
  `urun_serino` int(11) NOT NULL,
  `urun_id` int(11) NOT NULL,
  `urun_aciklamasi` text NOT NULL,
  `onay` int(11) NOT NULL,
  `firma` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunparcalari`
--

CREATE TABLE `urunparcalari` (
  `id` int(11) NOT NULL,
  `urun_modul_id` int(11) NOT NULL,
  `urun_recete_id` int(11) NOT NULL,
  `urun_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunrecetesi`
--

CREATE TABLE `urunrecetesi` (
  `id` int(11) NOT NULL,
  `urun_kodu` varchar(255) NOT NULL,
  `urun_adi` varchar(255) NOT NULL,
  `alis_fiyati` int(11) NOT NULL,
  `stok` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunserileri`
--

CREATE TABLE `urunserileri` (
  `id` int(11) NOT NULL,
  `urun_seriadi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `urunstok`
--

CREATE TABLE `urunstok` (
  `id` int(11) NOT NULL,
  `urun_kodu` int(11) NOT NULL,
  `urun_id` int(11) NOT NULL,
  `urun_stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `ust_menu`
--

CREATE TABLE `ust_menu` (
  `id` int(11) NOT NULL,
  `adi` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `ust_menu`
--

INSERT INTO `ust_menu` (`id`, `adi`, `class`) VALUES
(1, 'Gösterge Paneli', 'gostergepaneli'),
(2, 'Mesajlarım', 'mesajlarim'),
(3, 'Üye Yönetimi', 'uyeyonetimi'),
(4, 'Müşteri Yönetimi', 'musteriyonetimi'),
(5, 'Firma Yönetimi', 'firmayonetimi'),
(6, 'Ürün Yönetimi', 'urunyonetimi'),
(7, 'Muhasebe', 'muhasebe'),
(8, 'Sipariş Yönetimi', 'siparisyonetimi'),
(9, 'iş emirleri', 'isemirleri'),
(10, 'devir işlemleri', 'devirislemleri');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `uyeler`
--

CREATE TABLE `uyeler` (
  `uye_id` int(11) NOT NULL,
  `uye_ad` varchar(255) NOT NULL,
  `uye_soyad` varchar(255) NOT NULL,
  `uye_turu` int(11) NOT NULL,
  `uye_email` varchar(255) NOT NULL,
  `uye_kuladi` varchar(255) NOT NULL,
  `uye_sifre` varchar(255) NOT NULL,
  `uye_foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Tablo döküm verisi `uyeler`
--

INSERT INTO `uyeler` (`uye_id`, `uye_ad`, `uye_soyad`, `uye_turu`, `uye_email`, `uye_kuladi`, `uye_sifre`, `uye_foto`) VALUES
(31, 'Emre', 'Çelik', 4, 'emrecelik1563@hotmail.com', 'yonetici', '123456', '0d2d2f5e1e37d913089bc00a18493978.gif'),
(43, 'Faruk', 'Aygün', 1, 'faruk@hotmail.com', 'faruk', '123456', '199303cf0b3810f2b559c6fefbc6f6c9.jpg'),
(44, 'Bektaş', 'Öztürk', 3, 'bektas@ormix.com', 'bektas', '123', 'b44aa347950f069088a29b23649d5dd5.jpg');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `alt_menu`
--
ALTER TABLE `alt_menu`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `cevap`
--
ALTER TABLE `cevap`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `ekler`
--
ALTER TABLE `ekler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `firmalar`
--
ALTER TABLE `firmalar`
  ADD PRIMARY KEY (`firma_id`);

--
-- Tablo için indeksler `firmaodeme`
--
ALTER TABLE `firmaodeme`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `gelenkutusu`
--
ALTER TABLE `gelenkutusu`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `gonderilenler`
--
ALTER TABLE `gonderilenler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `grup_rutbe`
--
ALTER TABLE `grup_rutbe`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `iadeurunler`
--
ALTER TABLE `iadeurunler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `ilceler`
--
ALTER TABLE `ilceler`
  ADD KEY `fk_il_no` (`il_no`);

--
-- Tablo için indeksler `iller`
--
ALTER TABLE `iller`
  ADD PRIMARY KEY (`il_no`);

--
-- Tablo için indeksler `kasaekle`
--
ALTER TABLE `kasaekle`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kasagiderleri`
--
ALTER TABLE `kasagiderleri`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `malzemestok`
--
ALTER TABLE `malzemestok`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `mesajlasmalar`
--
ALTER TABLE `mesajlasmalar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `musteriler`
--
ALTER TABLE `musteriler`
  ADD PRIMARY KEY (`musteri_id`);

--
-- Tablo için indeksler `odemeler`
--
ALTER TABLE `odemeler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `pazarlamacigider`
--
ALTER TABLE `pazarlamacigider`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `rutbelerim`
--
ALTER TABLE `rutbelerim`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `siparisler`
--
ALTER TABLE `siparisler`
  ADD PRIMARY KEY (`siparis_id`);

--
-- Tablo için indeksler `tedarikcifirmalar`
--
ALTER TABLE `tedarikcifirmalar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `uretimler`
--
ALTER TABLE `uretimler`
  ADD PRIMARY KEY (`uretim_id`);

--
-- Tablo için indeksler `urunler`
--
ALTER TABLE `urunler`
  ADD PRIMARY KEY (`urun_id`);

--
-- Tablo için indeksler `urunmodulu`
--
ALTER TABLE `urunmodulu`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunparcalari`
--
ALTER TABLE `urunparcalari`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunrecetesi`
--
ALTER TABLE `urunrecetesi`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunserileri`
--
ALTER TABLE `urunserileri`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `urunstok`
--
ALTER TABLE `urunstok`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `ust_menu`
--
ALTER TABLE `ust_menu`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `uyeler`
--
ALTER TABLE `uyeler`
  ADD PRIMARY KEY (`uye_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `alt_menu`
--
ALTER TABLE `alt_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Tablo için AUTO_INCREMENT değeri `cevap`
--
ALTER TABLE `cevap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `ekler`
--
ALTER TABLE `ekler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `firmalar`
--
ALTER TABLE `firmalar`
  MODIFY `firma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `firmaodeme`
--
ALTER TABLE `firmaodeme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `gelenkutusu`
--
ALTER TABLE `gelenkutusu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `gonderilenler`
--
ALTER TABLE `gonderilenler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `grup_rutbe`
--
ALTER TABLE `grup_rutbe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Tablo için AUTO_INCREMENT değeri `iadeurunler`
--
ALTER TABLE `iadeurunler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `kasaekle`
--
ALTER TABLE `kasaekle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `kasagiderleri`
--
ALTER TABLE `kasagiderleri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- Tablo için AUTO_INCREMENT değeri `malzemestok`
--
ALTER TABLE `malzemestok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `mesajlasmalar`
--
ALTER TABLE `mesajlasmalar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `musteriler`
--
ALTER TABLE `musteriler`
  MODIFY `musteri_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `odemeler`
--
ALTER TABLE `odemeler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `pazarlamacigider`
--
ALTER TABLE `pazarlamacigider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `rutbelerim`
--
ALTER TABLE `rutbelerim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Tablo için AUTO_INCREMENT değeri `siparisler`
--
ALTER TABLE `siparisler`
  MODIFY `siparis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Tablo için AUTO_INCREMENT değeri `tedarikcifirmalar`
--
ALTER TABLE `tedarikcifirmalar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `uretimler`
--
ALTER TABLE `uretimler`
  MODIFY `uretim_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunler`
--
ALTER TABLE `urunler`
  MODIFY `urun_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Tablo için AUTO_INCREMENT değeri `urunmodulu`
--
ALTER TABLE `urunmodulu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunparcalari`
--
ALTER TABLE `urunparcalari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunrecetesi`
--
ALTER TABLE `urunrecetesi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunserileri`
--
ALTER TABLE `urunserileri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `urunstok`
--
ALTER TABLE `urunstok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Tablo için AUTO_INCREMENT değeri `ust_menu`
--
ALTER TABLE `ust_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Tablo için AUTO_INCREMENT değeri `uyeler`
--
ALTER TABLE `uyeler`
  MODIFY `uye_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Dökümü yapılmış tablolar için kısıtlamalar
--

--
-- Tablo kısıtlamaları `ilceler`
--
ALTER TABLE `ilceler`
  ADD CONSTRAINT `fk_il_no` FOREIGN KEY (`il_no`) REFERENCES `iller` (`il_no`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
