<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class yonetimpaneli extends CI_Controller {

    	
	
	public function __construct(){
		
		parent::__construct();
		$this->security();
		$this->cevap();
		$this->murat();
		//$this->bildirimler();
		ini_set('date.timezone', 'Europe/Istanbul');
	}
	
	
	function security(){
		$kontrol = $this->session->userdata("kontrol");
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		if($kontrol != 1){
		   redirect('yonetim/index');
		}
		
	}
	
	public function murat(){
		
		$this->load->model("selectt");
		$data["cek"] = $this->selectt->toplamonay1();  
		
	}
	
	
	public function getRealIpAddr()  
{  
    if (!empty($_SERVER['HTTP_CLIENT_IP']))  
    {  
        $ip=$_SERVER['HTTP_CLIENT_IP'];  
    }  
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //Proxy den bağlanıyorsa gerçek IP yi alır.
     
    {  
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];  
    }  
    else  
    {  
        $ip=$_SERVER['REMOTE_ADDR'];  
    }  
    return $ip;  
}  
	
	
	
	public function index(){
		
		
		
		$this->load->model("selectt");
		
		
		
		if($this->session->userdata("logs") == 1){
			$this->logs();
			$this->session->unset_userdata("logs");
		}
		
		
		$this->session->userdata("uyebilgi");
		
		
		$data["uyebilgi"] = $this->session->userdata("uyebilgi");
		
		$data["uyegetir"] = $this->selectt->uyegetir($data["uyebilgi"]->uye_id);
		
		$data["logcek"] = $this->selectt->logcek();
		
		$date = date("d.m.Y");
		
		$data["kasalistesi"] = $this->selectt->kasalistesiz($date);
		$data["kasalistesigelir"] = $this->selectt->kasalistesigelir($date);
		
		
		// tüm countlar burada yer alacak //
		
		$data["bekleyensiparislercount"] = $this->selectt->bekleyensiparislercount();
		$data["tumgonderilenmalzemelercount"] = $this->selectt->tumgonderilenmalzemelercount();
		$data["tumyapilantahsilatlarcount"] = $this->selectt->tumyapilantahsilatlarcount();
		$data["encoksatan"] = $this->selectt->encoksatanurunler();
		$data["sonuyecek"] = $this->selectt->sonuyecek();
		$data["depostokcount"] = $this->selectt->depostokcount();
		$data["uretimstokcount"] = $this->selectt->uretimstokcount();
		$data["toplammusteribakiyecount"] = $this->selectt->toplammusteribakiyecount();
		$data["toplamfirmabakiyesicount"] = $this->selectt->toplamfirmabakiyesicount();
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("index",$data);
		$this->load->view("includes/footer");
	}
	
	
	
	public function logs()
    {
		
		
		
		
			
		$this->load->model("insertt");
		
		$data["uyebilgi"] = $this->session->userdata("uyebilgi");
		
		
		$logs = array(
		"ad_soyad" => $data["uyebilgi"]->uye_ad . " " . $data["uyebilgi"]->uye_soyad,
		"yapilan_islem" => "Sisteme giriş yapıldı.",
		"tarih" => $date = date("d.m.y - H:i:s"),
		"ip" => $this->getRealIpAddr(),
		);
		
		$yolla = $this->insertt->girislogekle($logs);
			
			
			$this->session->unset_userdata("logs");
		
		
		
		
        
	    
	   
    }
	
	public function cikis(){
		
		
		$this->load->model("insertt");
		
		$data["uyebilgi"] = $this->session->userdata("uyebilgi");
		
		
		$logs = array(
		"ad_soyad" => $data["uyebilgi"]->uye_ad . " " . $data["uyebilgi"]->uye_soyad,
		"yapilan_islem" => "Sistemden çıkış yapıldı.",
		"tarih" => $date = date("d.m.y - H:i:s"),
		"ip" => $this->getRealIpAddr(),
		);
		
		$yolla = $this->insertt->girislogekle($logs);
		
		
		$kontrol = $this->session->userdata("kontrol");
		$this->session->sess_destroy($kontrol);
		redirect('');
	}
	
	
	public function uyeekle(){
		$this->load->model("Insertt");
	   if($_POST){
			    $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
				if(!$this->upload->do_upload('file')){
					$this->session->set_flashdata('alert','<div class="alert alert-danger"> Kabul edilen türler PNG,JPG,GİF
                                    </div>');
					
					redirect('uyeekle');
				}
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
		
				
				$ad = $this->input->post('ad');                       
				$soy = $this->input->post('soy');                       
				$mail = $this->input->post('mail');                       
				$kuladi = $this->input->post('kuladi');                       
				$sifre = $this->input->post('sifre');                       
				$tur = $this->input->post('tur');                       
				                   
				                       
           
				
				$data = array(
				"uye_ad" => $ad,
				"uye_soyad" => $soy,
				"uye_email" => $mail,
				"uye_kuladi" => $kuladi,
				"uye_foto" =>  $resim,
				"uye_sifre" =>  $sifre,
				"uye_turu" => $tur,
				);
                
				
				
				$result = $this->Insertt->uyeekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('uyeekle');
			   }   
			
		}
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("uyeekle");
		$this->load->view("includes/footer");
	}
	
	public function uyeler(){
		
		
		  $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamuyeler();  
		$config['base_url'] = site_url()."yonetimpaneli/uyeler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->listele($config['per_page'], $offset);  
		
		
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("uyeler",$data);
		$this->load->view("includes/footer");
	}
	
	
	
	public function musteriekle(){
		
		
	   $this->load->model("Insertt");
	   if($_POST){
			 
				$musteri_adi = $this->input->post('musteri_adi');                       
				$musteri_soyadi = $this->input->post('musteri_soyadi');                       
				$musteri_kod = $this->input->post('musteri_kod');                                                                
				$musteri_tel = $this->input->post('musteri_tel');     	 
				$musteri_fax = $this->input->post('musteri_fax');                       
				$musteri_ulke = $this->input->post('musteri_ulke');                       
				$musteri_il = $this->input->post('musteri_il');                       
				$musteri_ilce = $this->input->post('musteri_ilce');                                             
				$musteri_mail = $this->input->post('musteri_mail');  
                $musteri_iskonto = $this->input->post('musteri_iskonto');                       
				$musteri_cari = $this->input->post('musteri_cari');                       
				$musteri_cek = $this->input->post('musteri_cek');                       
				$musteri_senet = $this->input->post('musteri_senet');                                             
				$musteri_nakit = $this->input->post('musteri_nakit');     	 
				$hesap = $this->input->post('hesap');                       
				$adres_detay = $this->input->post('adres_detay');                       
				$musteri_unven = $this->input->post('musteri_unven');                       
				$musteri_vergi = $this->input->post('musteri_vergi');                                             
				$musteri_ilbir = $this->input->post('musteri_ilbir');  
                $musteri_ilcebir = $this->input->post('musteri_ilcebir');                       
				$musteri_daire = $this->input->post('musteri_daire');                       
				$musteri_adres = $this->input->post('musteri_adres');                                                    				
				$firma_adi = $this->input->post('firma_adi');                                                    				
				$musterikuladi = $this->input->post('musterikuladi');                                                    				
				$musterisifre = $this->input->post('musterisifre');                                                    				
				               
				                       
                $tl_formati = number_format($musteri_cari, -2, ',', '.');
				
				$data = array(
				"musteri_adi" => $musteri_adi,
				"musteri_soyadi" => $musteri_soyadi,
				"musteri_kodu" => $musteri_kod,
				"musteri_telefon" => $musteri_tel,
				"musteri_fax" => $musteri_fax,
				"musteri_ulke" => $musteri_ulke,
				"musteri_il" => $musteri_il,
				"musteri_ilce" => $musteri_ilce,
				"musteri_mail" => $musteri_mail,
				"musteri_ceklimit" => $musteri_cek,
				"musteri_senetlimit" => $musteri_senet,
				"musteri_nakitlimit" => $musteri_nakit,
				"musteri_turu" => $hesap,
				"musteri_adres" => $adres_detay,
				"musteri_unvani" => $musteri_unven,
				"musteri_vergino" => $musteri_vergi,
				"musteri_il1" => $musteri_ilbir,
				"musteri_ilce1" => $musteri_ilcebir,
				"musteri_vergidairesi" => $musteri_daire,
				"musteri_adres1" => $musteri_adres,
				"firma_adi" => $firma_adi,
				"musteri_kuladi" => $musterikuladi,
				"musteri_sifre" => $musterisifre
				);
                
				
				
				$result = $this->Insertt->musteriekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('musteriekle');
			   }   
		
	   }
		
		
		
	
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("musteriekle");
		$this->load->view("includes/footer");
	}
	
	public function musterilistesi(){
		
		
		
		  $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplammusteri();  
		$config['base_url'] = site_url()."yonetimpaneli/musterilistesi"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->listelemusteri($config['per_page'], $offset);  
		
		$data["pazarlamacek"] = $this->selectt->pazarlamacicek();
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("musterilistesi",$data);
		$this->load->view("includes/footer");
	}
	
	public function firmaekle(){
		
		
		$this->load->model("Insertt");
	   if($_POST){
			 

				
				$musteri_adi = $this->input->post("musteri_adi");                      
				$musteri_soyadi = $this->input->post("musteri_soyadi");                      
				$musteri_kod = $this->input->post("musteri_kod");                      
				$musteri_tel = $this->input->post("musteri_tel");                      
				$musteri_fax = $this->input->post("musteri_fax");                      
				$musteri_ulke = $this->input->post("musteri_ulke");                      
				$musteri_il = $this->input->post("musteri_il");                      
				$musteri_ilce = $this->input->post("musteri_ilce");                      
				$musteri_mail = $this->input->post("musteri_mail");                      
				$musteri_cari = $this->input->post("musteri_cari");                      
				$hesap = $this->input->post("hesap");                      
				$adres_detay = $this->input->post("adres_detay");                      
				$musteri_unven = $this->input->post("musteri_unven");                      
				$firma_adi = $this->input->post("firma_adi");                      
				$firma = $this->input->post("firma");                      
				$musteri_vergi = $this->input->post("musteri_vergi");                      
				$musteri_ilbir = $this->input->post("musteri_ilbir");                      
				$musteri_ilcebir = $this->input->post("musteri_ilcebir");                      
				$musteri_daire = $this->input->post("musteri_daire");                      
				$musteri_adres = $this->input->post("musteri_adres");                                            
				                   
				                       
                
				$date = date("d.m.Y");
				
				$data = array(
				"firma_kodu" => $musteri_kod,
				"firma_adi" => $firma_adi,
				"firma_turu" => $firma,
				"firma_adresi" => $adres_detay,
				"firma_mail" => $musteri_mail,
				"musteri_adi" => $musteri_adi,
				"musteri_soyadi" => $musteri_soyadi,
				"musteri_telefon" => $musteri_tel,
				"fax" => $musteri_fax,
				"ulke" => $musteri_ulke,
				"il" => $musteri_il,
				"ilce" => $musteri_ilce,
				"hesap_turu" => $hesap,
				"adres" => $adres_detay,
				"musteri_unvani" => $musteri_unven,
				"vergi_no" => $musteri_vergi,
				"il1" => $musteri_ilbir,
				"ilce1" => $musteri_ilcebir,
				"vergi_dairesi" => $musteri_daire,
				
				);
                
				
				
				$result = $this->Insertt->firmaekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('firmaekle');
			   }   
		
	   }
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("firmaekle");
		$this->load->view("includes/footer");
	}
	
	public function firmalar(){
		
		
		$this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamfirma();  
		$config['base_url'] = site_url()."yonetimpaneli/firmalar"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->firmaliste($config['per_page'], $offset);  
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("firmalar",$data);
		$this->load->view("includes/footer");
	}
	
	public function urunekle(){
		
		
		
	   $this->load->model("Insertt");
	   if($_POST){
			 
			     $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				$resim = $data["upload_data"]["file_name"];
				
			    $kod = $this->input->post('kod');                       
				$ad = $this->input->post('ad');                       
				$afiyat = $this->input->post('afiyat');                       
				$sfiyat = $this->input->post('sfiyat');                                             
				$surun = $this->input->post('seri1');       
			    $tfirma = $this->input->post('tedarik');                       
				$stok = $this->input->post('stok');                       
				$kdv = $this->input->post('kdv');                       
				$birim = $this->input->post('birim');                                             
				$urun = $this->input->post('urun');                
				
				$kdvs = $sfiyat * ($kdv / 100);
                $ytutar = $sfiyat + $kdvs;

				$data = array(
				"urun_kodu" => $kod,
				"urun_adi" => $ad,
				"alis_fiyati" => $afiyat,
				"satis_fiyati" => $sfiyat,
				"urun_serisi" => $surun,
				"stok" => $stok,
				"kdv_orani" => $kdv,
				"birim" => $birim,
				"urun_yazisi" => $urun,
				"resim" => $resim,
				"sonfiyat" => $ytutar,
				);
                
				
				
				$result = $this->Insertt->uruneklee($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('urunekleyonlendir/urunekle');
			   }   
		
	   }
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunekle");
		$this->load->view("includes/footer");
	}
	
	public function urunseriekle(){
		
		$this->load->model("Insertt");
		$this->load->model("selectt");
		
		 if($_POST){
			 

				
				 $seri = $this->input->post('seri');                       
			                     
				                   
				                       
             
				
				$data = array(
				"urun_seriadi" => $seri,
		
				);
                
				
				
				$result = $this->Insertt->seri($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('urunseriekle');
			   }   
		
	   }
		
		
		
		
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->urunsericekscount();  
		$config['base_url'] = site_url()."yonetimpaneli/urunseriekle"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["sericek"] = $this->selectt->urunsericeks($config['per_page'], $offset);
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunseriekle",$data);
		$this->load->view("includes/footer");
	}
	
	
	
	
	
	
	
	public function iadeurunekle($id = ""){
		
		
		
		$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->iadeuruncount($id);  
		$config['base_url'] = site_url()."iadeurunekle/".$id; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->iadeurunlistele($config['per_page'], $offset,$id);
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("iadeurunekle",$data);
		$this->load->view("includes/footer");
	}
	
	public function urunler(){
		
		$this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamuruncek();  
		$config['base_url'] = site_url()."yonetimpaneli/urunler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->urunlistele($config['per_page'], $offset);
		
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunler",$data);
		$this->load->view("includes/footer");
	}
	
	public function urunstok(){
		
	    $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] = $this->selectt->toplamstok();  
		$config['base_url'] = site_url()."yonetimpaneli/firmalar"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->listelestok($config['per_page'], $offset);  
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunstok",$data);
		$this->load->view("includes/footer");
	}
	
	
	public function malzemestok(){
		
			  $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamstok1();  
		$config['base_url'] = site_url()."yonetimpaneli/malzemestok"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->listelestok1($config['per_page'], $offset); 
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("malzemestok",$data);
		$this->load->view("includes/footer");
	}
	
	public function teklifekle(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("teklifekle");
		$this->load->view("includes/footer");
	}
	
	public function teklifler(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("teklifler");
		$this->load->view("includes/footer");
	}
	
	public function onaylanmamissiparisler(){
		
			
		$this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamonay();  
		$config['base_url'] = site_url()."yonetimpaneli/onaylanmamissiparisler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->onaylistesi($config['per_page'], $offset);  
		$data1["cek"] = $this->selectt->toplamonay1($config['per_page'], $offset);  
		
		

		$this->load->view("includes/header");
		$this->load->view("includes/sidebar",$data1);
		$this->load->view("onaylanmamissiparisler",$data);
		$this->load->view("includes/footer");
	}
	
	public function onaylanmissiparisler(){
		
		
		 $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamonay11();  
		$config['base_url'] = site_url()."yonetimpaneli/onaylanmissiparis"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->onaylistesi1($config['per_page'], $offset);  
		
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("onaylanmissiparisler",$data);
		$this->load->view("includes/footer");
	}
	
	public function faturaislemleri(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("fatura");
		$this->load->view("includes/footer");
	}
	
	public function kasa(){
		
	 $this->load->model("Insertt");
     if($_POST){
       

        
         $kod = $this->input->post('kod');                       
         $kasaadi = $this->input->post('kasaadi');                       
         $aciklama = $this->input->post('aciklama');                       

        $data = array(
        "kasakodu" => $kod,
        "kasa_adi" => $kasaadi,
        "aciklama" => $aciklama,
        
    
        );
                
        
        
        $result = $this->Insertt->kasakkekle($data);
               
         if($result){
          
          
           $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Gider Eklendi !</div><audio controls autoplay style="display:none;"  ><source 
         src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
          redirect('kasa');
         }   
		
		}
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("kasa");
		$this->load->view("includes/footer");
	
	
	}
	
	
	
	
	
	public function riskrapor(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("riskrapor");
		$this->load->view("includes/footer");
	}
	
	public function senetriskraporu(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("senetriskraporu");
		$this->load->view("includes/footer");
	}
	
	public function musteriodeme($ald){
		$this->load->model("selectt");
		 
		 $data1['cari'] = $this->selectt->caricek($ald);
		$caricek = $data1['cari']->musteri_cari;
		
      
	   $this->load->model("Insertt");
	   if($_POST){
			
		
				
				$hesaptur = $this->input->post('hesaptur');
				
			if($hesaptur==""){ $hesaptur=0;}
			
				
				
				$odetarih = $this->input->post('alistarihi'); 
				
			if($odetarih==""){ $odetarih=0;}
			
			
				$miktar = $this->input->post('ciro');
				$miktar = floatval($miktar);
				
				
				
				
if($miktar==""){ $miktar=0;}

$kesidici = $this->input->post('kesidici');
if($kesidici==""){ $kesidici=0;}

$muhattap = $this->input->post('muhattap');
if($muhattap==""){ $muhattap=0;}
$bankahesabi = $this->input->post('bankahesabi');
if($bankahesabi==""){ $bankahesabi=0;}

				$uyeid = $this->input->post('uyeid'); 


  $banka_adi = $this->input->post('bankaadi');
  
  if($banka_adi==""){ $banka_adi=0;}
  
    $odemeturu = $this->input->post('odemeturu');
  
  if($odemeturu==""){ $odemeturu=0;}

			   
			   
			  
			    if($tl_formati==""){ $tl_formati=0;}
	
	        
			
	
			 $data1['cari'] = $this->selectt->caricek($ald);
                
				$date = date("d.m.Y");
				
				$data = array(
				"musteri_id" =>$ald,
				"odenen_miktar" => $miktar,
				"ekleme_tarihi" =>$date,
				"odeme_tarih" => $odetarih,
				"kesidici" => $kesidici,
				"muhattap" => $muhattap,
				"uye_id" => $uyeid,
				"banka_hesabi" => $bankahesabi,
				"banka_adi" =>$banka_adi,
				"odemeturu" =>$odemeturu,
				);
				
				
				
				
				$result = $this->Insertt->odeekle($data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Ödeme Başarılı</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
			   }   
			
		}
	   	
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("musteriodeme",$data1);
		$this->load->view("includes/footer");
	}

	
	public function alinacaklar(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("alinacaklar");
		$this->load->view("includes/footer");
	}

public function urunterm(){
    $this->load->model("selectt");
    
    $id = $this->input->get("term");
    
    
    $result = $this->selectt->urunterm($id);
    
    foreach($result as $row){
      
      $data[] = array(
	  "value" => $row->urun_adi,
      "urun_adi" => $row->urun_adi,
      );
    }
    
    echo json_encode($data);
    
  }
  public function firmaturcekti(){
    $this->load->model("selectt");
    
    $id = $this->input->get("term");
    
    
    $result = $this->selectt->firmaturcek($id);
    
    foreach($result as $row){
      
      $data[] = array(
	  "value" => $row->firma_adi,
      "firma_adi" => $row->firma_adi,
      );
    }
    
    echo json_encode($data);
    
  }
  public function urunserileri(){
    $this->load->model("selectt");
    
    $id = $this->input->get("term");
    
    
    $result = $this->selectt->seriseri($id);
    
    foreach($result as $row){
      
      $data[] = array(
	  "value" => $row->urun_seriadi,
      "urun_seriadi" => $row->urun_seriadi,
      );
    }
    
    echo json_encode($data);
    
  }
  
    public function mustericekss(){
    $this->load->model("selectt");
    
    $id = $this->input->get("term");
    
    
    $result = $this->selectt->mustericeks($id);
    
    foreach($result as $row){
      
      $data[] = array(
	  "value" => $row->musteri_adi,
      "musteri_adi" => $row->musteri_adi,
      );
    }
    
    echo json_encode($data);
    
  }
  
  
    public function uyesil($al){
	   $this->load->model("deletee");
	 $result =   $this->deletee->uyesill($al);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-success"> Başarıyla silindi.
                                    </div>');
					redirect('uyeler');
	   }
   }
   
   
   public function musterisil($al){
	   $this->load->model("deletee");
	 $result =   $this->deletee->musterisill($al);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-success"> Başarıyla silindi.
                                    </div>');
					redirect('musterilistesi');
	   }
   }
   
   
   
     public function firmasil($al){
	   $this->load->model("deletee");
	 $result =   $this->deletee->firmasill($al);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-success"> Firma silindi.
                                    </div>');
					redirect('firmalar');
	   }
   }
	    public function urunsil($al){
	   $this->load->model("deletee");
	   $result = $this->deletee->urunsill($al);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-success"> Ürün silindi.
                                    </div>');
					redirect('urunler');
	   }
   }
     public function siparissil($al){
	   $this->load->model("deletee");
	 $result =   $this->deletee->siparislersil($al);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-success"> Ürün silindi.
                                    </div>');
					$url = $_SERVER['HTTP_REFERER'];
redirect($url);
					
	   }
   }
	
	
	  public function uyeguncelleme(){
		  $this->load->model('selectt');
		  $this->load->model('updatee');
		  
		  
	  if($_POST){
		  
		  $id = $this->input->post("id");
		  
		  
		        $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
				$this->upload->do_upload('file');
				
				$data = array('upload_data' => $this->upload->data());
				
				$resim = $data["upload_data"]["file_name"];
				
		  
		  
		 $ad = $this->input->post("ad");
		 $soyad = $this->input->post("soyad");
		 $mail = $this->input->post("mail");
	     $sifre = $this->input->post("pas");
	    $eskiresim = $this->input->post("resimcek");
		$resimson = explode("s/",$eskiresim);
  
		  if($resim == ""){
					 $resim = $resimson[1];
				} 
				
		 
		 
		 
		 $data1 = array(
		 "uye_ad" => $ad,
		 "uye_soyad" => $soyad,
		 "uye_email" => $mail,
		 "uye_sifre" => $sifre,
		 "uye_foto" => $resim,
		 );
		 
		 $result = $this->updatee->uyeguncelle($id,$data1);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Üye Güncellendi.
                                    </div>');
					redirect('uyeler');
		 }
		  
	  }
		
		
		  
		  $this->load->view("uyeler");
		  
	  }
	  
	  
	   public function musteriguncelle(){
		  $this->load->model('selectt');
		  $this->load->model('updatee');
		  
		  
	  if($_POST){
		  
		 $id = $this->input->post("id");	  
		 $ad = $this->input->post("ad");
		 $firma = $this->input->post("firma1");
		 $mail = $this->input->post("mail");
	     $tel = $this->input->post("tel");
	     $adres = $this->input->post("adres");
	     $pazarlama = $this->input->post("pazarlamaci");
  
		  
		 
		 
		 
		 $data2 = array(
		 "musteri_adi" => $ad,
		 "firma_adi" => $firma,
		 "musteri_mail" => $mail,
		 "musteri_telefon" => $tel,
		 "musteri_adres" => $adres,
		 "gorevlipazarlama" => $pazarlama,
		 );
		 
		 $result = $this->updatee->musteriguncelle($id,$data2);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Müşteri güncellendi.
                                    </div>');
					redirect('musterilistesi');
		 }
		  
	  }
		
		
		  
		  $this->load->view("musterilistesi");
		  
	  }
	  
	  
	  
	    public function firmaguncelle(){
		  $this->load->model('selectt');
		  $this->load->model('updatee');
		  
		  
	  if($_POST){
		  
		 $id = $this->input->post("id");	  
		 $ad = $this->input->post("ad");
		 $mail = $this->input->post("mail");
	     $adres = $this->input->post("adres");
  
		  
		 
		 
		 
		 $data3 = array(
		 "firma_adi" => $ad,
		 "firma_mail" => $mail,
		 "firma_adresi" => $adres,
		 );
		 
		 $result = $this->updatee->firmaguncelle($id,$data3);
		 
		 if($result){
			 $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Firma Güncellendi.
                                    </div>');
					redirect('firmalar');
		 }
		  
	  }
		
		
		  
		  $this->load->view("firmalar");
		  
	  }
	  
	  function siparisler($idd){
		  $this->load->model("selectt");
		  
		  
		  if($_POST){
			  
			  	 $sericek = $this->input->post("sericeks");	
				
				$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamuruncek1($sericek);  
		$config['base_url'] = site_url()."siparisler/".$idd; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->urunlistele2($config['per_page'], $offset,$sericek);
		
		$data["toplamsiparis"] = $this->selectt->siparisay($idd);
		
		$data["sipariscek"] = $this->selectt->siparisceksen($idd);
			  
		  }
		  else{
		
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamuruncek();  
		$config['base_url'] = site_url()."siparisler/".$idd; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->urunlistele($config['per_page'], $offset);
		
		$data["toplamsiparis"] = $this->selectt->siparisay($idd);
		
		$data["sipariscek"] = $this->selectt->siparisceksen($idd);
		  }
	
		

		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("siparisler",$data);
		$this->load->view("includes/footer");
		  
	  }
	  
	  
	  function siparisekle($musteriid){
		    $this->load->model('Insertt');
			
		    if(isset($_POST)){
			
		
				$datatime = date('d.m.Y H:i:s');
				$urun_id = $this->input->post('urun_id');                                             
				$birim = $this->input->post('birim');                       
				$adet = $this->input->post('adet');                       
				$uye_id = $this->input->post('uye_id');   
				$sonfiyat = $this->input->post('sonfiyat');   
				$iskonto = $this->input->post('iskonto');   
                $onay = 1;				
			
			
				                       
           
				
				$data = array(
				"musteri_id" => $musteriid,
				"tarih" => $datatime,
				"urun_id" => $urun_id,
				"birim" => $birim,
				"adet" =>  $adet,
				"uye_id" =>  $uye_id,
				"onay" =>  $onay,
				"fiyat" =>  $sonfiyat,
				"iskonto" =>  $iskonto,
				);
                
				
				
				$result = $this->Insertt->siparisek($data);
               
			   if($result){
				  
				   $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Sipariş Eklendi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
					redirect('siparisler/'.$musteriid);
			   }   
			
		}
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("siparisler");
		$this->load->view("includes/footer");
		  
	  }
	  
	  
	 public function siparisonay(){
		  $this->load->model('updatee');
		   $id = $this->input->post("alinmis");
		   $musteriid = $this->input->post("musteriid");

		  
		 
		  
		$result =  $this->updatee->siparisonays($id,$musteriid);
		if($result){
			
			   $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Sipariş  Alındı !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
			
		}
		  
	 }
	 
	 public function siparisonayss(){
		  $this->load->model('updatee');
		  
		   $musteriid= $this->input->post("musteriid");
		  
		 
		  
		$result =  $this->updatee->siparisguncelle($musteriid);
		if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Tebrikler Sipariş Hazırlanıyor !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('onaylanmamissiparisler');
			
		}
		  
	 }
	 
	 public function siparissillp($all){
		 
		   $this->load->model("deletee");
		   
		 $result =   $this->deletee->siparissil($all);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-danger"> Sipariş Silindi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/silmesesi.mp3" type="audio/mpeg"></audio>');
					redirect('onaylanmamissiparisler');
	   }
		 
		
		
	 }
	 
	 public function muhasebeonay($all){
		 
		   $this->load->model('updatee');
		   
		 $result =   $this->updatee->muhasebeonay($all);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-info"> Sipariş Onaylandı !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/silmesesi.mp3" type="audio/mpeg"></audio>');
					redirect('onaylanmamissiparisler');
	   }
		 
		
		
	 }
	 
	 public function muhasebeonayla($all){
		 
		   $this->load->model('selectt');
		   
		 $data1['siparisgoster'] =   $this->selectt->siparisgorsun($all);	
		 $data1['ad'] =   $this->selectt->siparisadd($all);	
		

		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("muhasebeonayla",$data1);
		$this->load->view("includes/footer");
	 }
	 
	 public function depo($all){
		 
		   $this->load->model('selectt');
		   
		 $data1['siparisgoster'] =   $this->selectt->siparisgorsun1($all);	
		 $data1['ad'] =   $this->selectt->siparisadd1($all);	
	
		

		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("depo",$data1);
		$this->load->view("includes/footer");
	 }
	 
	 public function tamamlanansiparisdetayi($all){
		 
		   $this->load->model('selectt');
		   
		 $data1['siparisgoster'] =   $this->selectt->tamsiparsdetay($all);	
		 $data1['ad'] =   $this->selectt->siparisadd($all);	
		

		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("tamamlanansiparisdetayi",$data1);
		$this->load->view("includes/footer");
	 }
	 
	  public function tamamlanansiparis(){
		    $this->load->model('Updatee');
		   $this->load->model('selectt');
		   
		   	if($_POST){
				$id = $this->input->post("urunid");
	
		  
		$result =  $this->Updatee->teslimdegistir($id);
				
					if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Teslim Edildi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('tamamlanansiparis');
			
		}
			}
		$config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 4;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->tamsiparis();  
		$config['base_url'] = site_url()."siparisler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["siparisgoster"] = $this->selectt->urunlistele3($config['per_page'], $offset,$sericek);
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("tamamlanansiparis",$data);
		$this->load->view("includes/footer");
	 }
	 
	 public function depoonay(){
		  $this->load->model('updatee');
		  
		   $musteriid= $this->input->post("musteriid");
		  
		 
		  
		$result =  $this->updatee->depoonays($musteriid);
		if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Tebrikler Sipariş Hazırlanıyor !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('onaylanmissiparisler');
			
		}
		  
	 }
	 
	 public function depooll($all){
		 
		   $this->load->model('updatee');
		   
		 $result =   $this->updatee->depoonays($all);
	   if($result){
		   $this->session->set_flashdata("silindi",'<div class="alert alert-info"> Sipariş Onaylandı !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/silmesesi.mp3" type="audio/mpeg"></audio>');
					redirect('onaylanmamissiparisler');
	   }
		 
		
		
	 }
	 
	 
	 public function aramajson(){
	 $this->load->model('selectt');
	
	 $term = $this->input->get('term');
	
	$result =  $this->selectt->arama($term);
	foreach($result as $yaz){
		
	$data[]= array(
    "value" => $yaz->urun_adi,	
    "urun_id" => $yaz->urun_id,	
	"resim" => $yaz->resim,	
	"stok" => $yaz->stok,	
	"birim" => $yaz->birim,	
	"sonfiyat" => $yaz->sonfiyat,	
	"stok" => $yaz->stok,	
	);
	
	
	
	}
	
	echo json_encode($data,true);
	
    }
	
	
	public function tamirs($id){
			
		  $this->load->model('updatee');

		$result =  $this->updatee->teslimdegistir($id);
		if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Tebrikler Sipariş Hazırlanıyor !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('tamamlanansiparis');
			
		}
		  
	 }
	 
	 
	 
	 /******* FEAXER SHOW *******/
	 
	 public function urunmoduluekle(){
		
		$this->load->model("selectt");
		$this->load->model("insertt");
		$this->load->model("updatee");
		
	    $data['sericek'] =$this->selectt->sericek();
		
		if($_POST){
			
			
			$kod = $this->input->post("kod");
			$serino = $this->input->post("serino");
			$icerik = $this->input->post("icerik");
			$receteid = $this->input->post("receteid");
			$stok = $this->input->post("stok");
			$urunid = $this->input->post("urunid");
			$firma = $this->input->post("firma");
			
			
			$data1 = array(
			"urun_modulkodu" => $kod,
			"urun_serino" => $serino,
			"urun_aciklamasi" => $icerik,
			"urun_id" => $urunid,
			);
			
			$result = $this->insertt->urunmodulekle($data1);
			
			$say = -1;
			foreach($receteid as $yaz){
			$say++;	
			$data2 = array(
			"urun_modul_id" => $result,
			"urun_recete_id" => $yaz,
			"urun_stok" => $stok[$say],
			);
			
			$result1 = $this->insertt->urunparcaekle($data2);
			
			
			
			}
			
			
			if($result1){
				
				$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					
					     redirect('urunmoduluekle');
			}
			
		}
		
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunmodulekle",$data);
		$this->load->view("includes/footer");
	}
	 
	 public function urunstokekle(){
		$this->load->model("updatee");
		
		$id = $this->input->post("id");
		$text = $this->input->post("text");
		
		$data = array(
		"stok" => $text,
		);
		
		$sec = $this->updatee->urunstokguncelle($id,$data);
		
		
	}
	 
	 public function malzemestokekle(){
		$this->load->model("updatee");
		$id = $this->input->post("id");
		$text = $this->input->post("text");
		
		$data = array(
		"stok" => $text,
		);
		
		$sec = $this->updatee->malzemestokguncelle($id,$data);
		
	}
	 
	 public function gonderilenler(){
		 
		$this->load->model("selectt");
		 
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] =$this->selectt->gonderilenlercount($uyebilgi->uye_id);  
		$config['base_url'] = site_url()."gonderilenler/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->gonderilenlercek($config['per_page'], $offset,$uyebilgi->uye_id);
		
		
		 
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("gonderilenler",$data);
		$this->load->view("includes/footer");
	 }
	 
	 public function onaylanmamisisemirleri(){
		$this->load->model("selectt");
		 
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] =$this->selectt->onaylanmamisisemirlericount();  
		$config['base_url'] = site_url()."onaylanmamisisemirleri/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->onaylanmamisisemirleri($config['per_page'], $offset); 
		 
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("onaylanmamis",$data);
		$this->load->view("includes/footer");
	}
	 
	 public function autocomplete(){
		 
		 $this->load->model("selectt");
		 
		 $uyebilgi = $this->session->userdata("uyebilgi");
		 
		 $term = $this->input->get("term");
		 
		 $result = $this->selectt->uyejson($term,$uyebilgi->uye_id);
		 
		 foreach($result as $row){
      
          $data[] = array(
	      "value" => $row->uye_ad . " " .$row->uye_soyad . " <" . $row->uye_kuladi . ">",
          "uye_id" => $row->uye_id,
          );
          }
    
          echo json_encode($data);
		 
	 }
	 
	 public function parcajson(){
		 $this->load->model("selectt");
		 
		 $term = $this->input->get("term");
		 
		 $result = $this->selectt->urunrecetesijson($term);
		 
		 foreach($result as $row){
      
          $data[] = array(
	      "value" => $row->urun_adi,
          "id" => $row->id,
		  "stok" => $row->stok
          );
          }
		  
		  echo json_encode($data);
		 
	 }
	 
	 public function urunlerjson(){
		 $this->load->model("selectt");
		 
		 $term = $this->input->get("term");
		 
		 $result = $this->selectt->pipo($term);
		 
		 foreach($result as $row){
      
          $data[] = array(
	      "value" => $row->urun_adi,
          "id" => $row->urun_id,
		  "kod" => $row->urun_kodu,
          );
          }
		  
		  echo json_encode($data);
		 
	 }
	 
	 public function mesajolustur(){
		 
		$this->load->model("insertt");
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		if($_POST){
			ini_set('date.timezone', 'Europe/Istanbul');

			$date = date("d.m.y - H:i:s");
			
			
			$id = $this->input->post("id");
			$konu = $this->input->post("konu");
			$icerik = $this->input->post("icerik");
			
			$parcala = explode(",",$id);
			
			
			$adi = $_FILES["file"]["name"];
			$dosyaup = "./uploads/";
			$datem = new DateTime();
			$dosyalar = "";
			$say = count($_FILES["file"]["name"]);
			
			if(!empty($adi[0])){
				
				
				
				
				for($j=0;$j < $say ; $j++){
					
					
				
				
				$uzanti = explode(".",$adi[$j]);
				$yeniadi = rand(0,9999).$datem->getTimestamp().".".$uzanti[1];
				
				if($_FILES["file"]["size"][$j] > 31457280){
					echo "Dosyanızın Boyutu 30 MB'den küçük olmalıdır.";
				}
				else{
					
					if($_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $_FILES["file"]["type"][$j] == "application/vnd.ms-excel" || $_FILES["file"]["type"][$j] == "application/msword" || $_FILES["file"]["type"][$j] == "image/png" || $_FILES["file"]["type"][$j] == "application/powerpoint" || $_FILES["file"]["type"][$j] == "image/jpeg" || $_FILES["file"]["type"][$j] == "application/pdf" || $_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["file"]["type"][$j] == "image/gif" || $_FILES["file"]["type"][$j] == "application/vnd.ms-powerpoint" || $_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
						
					}
					else{
						
						$this->session->set_flashdata('alert','<div class="alert alert-danger"> Dosya tipiniz docx,pptx,gif,doc,png,jpg,ppt,pdf,xls,xlsx olmalıdır.
                                    </div>');
					
					     redirect('mesajolustur');
						
					}
					
				}
				
				}
				
				
				
				
			

			
			for($j=0;$j < $say ; $j++){
				
				$uzanti = explode(".",$adi[$j]);
				$yeniadi = rand(0,9999).$datem->getTimestamp().".".$uzanti[1];
				
				if($_FILES["file"]["size"][$j] > 31457280){
					echo "Dosyanızın Boyutu 30 MB'den küçük olmalıdır.";
					$this->session->set_flashdata('alert','<div class="alert alert-alert"> Dosyanızın boyutu 30 MB den küçük olmalıdır.
                                    </div>');
					
					     redirect('mesajolustur');
				}
				else{
					
					
					
					
					
					if($_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $_FILES["file"]["type"][$j] == "application/vnd.ms-excel" || $_FILES["file"]["type"][$j] == "application/msword" || $_FILES["file"]["type"][$j] == "image/png" || $_FILES["file"]["type"][$j] == "application/powerpoint" || $_FILES["file"]["type"][$j] == "image/jpeg" || $_FILES["file"]["type"][$j] == "application/pdf" || $_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" || $_FILES["file"]["type"][$j] == "image/gif" || $_FILES["file"]["type"][$j] == "application/vnd.ms-powerpoint" || $_FILES["file"]["type"][$j] == "application/vnd.openxmlformats-officedocument.presentationml.presentation"){
						
						$tasi = move_uploaded_file($_FILES["file"]["tmp_name"][$j],$dosyaup.$yeniadi);
						
						
						
						if($tasi){
							
							
							$datar = array(
							"dosya_adi" => $yeniadi,
							);
							
							$ekle = $this->insertt->dosyaekle($datar);
						    
							
							$dosyalar = $dosyalar.",".$ekle;
							
							
						}
						
						
					}
					else{
						$this->session->set_flashdata('alert','<div class="alert alert-alert"> Dosya tipiniz docx,pptx,gif,doc,png,jpg,ppt,pdf,xls,xlsx olmalıdır.
                                    </div>');
					
					     redirect('mesajolustur');
					}
					
				}
				
			}
			
			
			
			
		    }
			
			
			
			foreach($parcala as $yaz){
			
			$data1 = array(
			"gonderen_id" => $uyebilgi->uye_id,
			"alan_id" => $yaz,
			"konu" => $konu,
			"icerik" => $icerik,
			"tarih" => $date,
			"gonderen_sil" => "0",
			"alan_sil" =>  "0",
			"ek_id" => @$dosyalar,
			);
			
			$result1 = $this->insertt->gelenkutusuekle($data1);
			
			
			$data = array(
			"gonderen_id" => $uyebilgi->uye_id,
			"alan_id" => $yaz,
			"konu" => $konu,
			"icerik" => $icerik,
			"tarih" => $date,
			"gonderen_sil" => "0",
			"alan_sil" =>  "0",
			"ek_id" => @$dosyalar,
			"mesaj_goster_id" => $result1,
			);
			
            $result = $this->insertt->gonderilenlerekle($data);
			
			

			}
			
			
			if($result){
				$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Gönderilmiştir.
                                    </div>');
									redirect('mesajolustur');
			}
			
			
		}
		 
		 
		 
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("mesajolustur");
		$this->load->view("includes/footer");
	}
	
	public function gelenkutusu(){
		
		$this->load->model("selectt");
		
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] =$this->selectt->gelenkutusucount($uyebilgi->uye_id);  
		$config['base_url'] = site_url()."gelenkutusu/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->gelenkutusucek($config['per_page'], $offset,$uyebilgi->uye_id);
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("gelenkutusu",$data);
		$this->load->view("includes/footer");
	}
	
	
	public function mesajgoster($id){
		$this->load->model("selectt");
		$this->load->model("updatee");
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		$data["first"] = $this->selectt->mesajgoster($id);
		
		// mesaj kimler arasında.
		$data["uye"] = $this->selectt->alanidcektekli($data["first"]->gonderen_id);
		$data["uye1"] = $this->selectt->alanidcektekli($data["first"]->alan_id);
		
		$rifki = $this->selectt->sonmesajlasmacek($id);
		
		
		
		if($uyebilgi->uye_id == $rifki->mesaj_gonderen_id){
			
		}
		else{
			$this->updatee->mesajgoruldu($id);
		}
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("mesajgoster",$data);
		$this->load->view("includes/footer");
	}
	
	public function gonderensil($id){
		$this->load->model("deletee");
		$this->load->model("selectt");
		$this->load->model("updatee");
		
		$cek = $this->selectt->gonderenteklicek($id);
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		if($uyebilgi->uye_id == $cek->gonderen_id){
			
			$data = array(
		    "gonderen_sil" => "1",
		     );
			 
		}
		else{
			
			$data = array(
		    "alan_sil" => "1",
		     );
		}
		
		
		
		$result = $this->updatee->gonderensil($data,$id);
		
		if($result){
			echo "1";
		}
			
		
	}
	
	public function gelensil($id){
		
		$this->load->model("deletee");
		$this->load->model("selectt");
		$this->load->model("updatee");
		
		$cek = $this->selectt->gelenteklicek($id);
		
		
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		if($uyebilgi->uye_id == $cek->gonderen_id){
			
			$data = array(
		    "gonderen_sil" => "1",
		     );
			 
		}
		else{
			
			$data = array(
		    "alan_sil" => "1",
		     );
		}
		
		
		
		$result = $this->updatee->gelenkutususil($data,$id);
		
		if($result){
			echo "1";
		}
		
	}
	
	
	public function mesajgnd($id){
		$this->load->model("insertt");
		$mesaj = $this->input->post("mesaj");
		$var = $this->input->post("id");
		$uyebilgi = $this->session->userdata("uyebilgi");
		$date = date('d.m.Y H:i:s');
		
		
		
		$data = array(
		"mesaj_id" => $id,
		"mesaj_gonderen_id" => $uyebilgi->uye_id,
		"mesaj_alan_id" => $var,
		"mesaj_tarihi" => $date,
		"mesaj_icerik" => $mesaj,
		);
		
		$result = $this->insertt->mesajekle($data);
		
	}
	
	public function konusma($id){
		$this->load->model("selectt");
		$uyebilgi = $this->session->userdata("uyebilgi");
		$data = $this->selectt->konusmacek($id);
		
		
		foreach($data as $yaz){
			
			$uye =  $this->selectt->alanidcektekli($yaz->mesaj_gonderen_id);
			echo $yaz->mesaj_gonderen_id ==  $uyebilgi->uye_id ? "<div class='alan'>" : "<div class='gonderen'>";
			echo '<p class="gdbilgi">' .$yaz->mesaj_tarihi . " - ".$uye->uye_ad . " " . $uye->uye_soyad . '</p>';
			echo $yaz->mesaj_gonderen_id ==  $uyebilgi->uye_id ? '<p class="gdmesaj1">' : '<p class="gdmesaj">'; echo $yaz->mesaj_icerik .'</p>';
			echo '</div>'; 
		}
	}
	
	public function cevap(){
		
		 error_reporting(0);
		
		$this->load->model("selectt");
		$this->load->model("insertt");
		$uyebilgi = $this->session->userdata("uyebilgi");
		$data = $this->selectt->cevapmesajlasmalar($uyebilgi->uye_id);		
		
		foreach($data as $yaz){
			// gelen kutusundan eşleşen mesajlar.
			$gelenkutusu = $this->selectt->cevapgelenkutusu($yaz->mesaj_id,$uyebilgi->uye_id);
			
			
			if($gelenkutusu == null){
				
			}
			else{
				$data = array(
			"id" => $gelenkutusu->id,
			"gonderen_id" => $gelenkutusu->alan_id,
			"alan_id" => $gelenkutusu->gonderen_id,
			"konu" => $gelenkutusu->konu,
			"icerik" => $gelenkutusu->icerik,
			"tarih" => $gelenkutusu->tarih,
			"gonderen_sil" => $gelenkutusu->gonderen_sil,
			"alan_sil" => $gelenkutusu->alan_sil,
			"ek_id" => $gelenkutusu->ek_id,
			"alan_gordumu" => $gelenkutusu->alan_gordumu,
			"gonderen_gordumu" => $gelenkutusu->gonderen_gordumu,
			);
			
			$cek = $this->selectt->cevapcheck($gelenkutusu->id);
			
			if(!empty($cek)){
				
			}
			else{
				$result = $this->insertt->gelenkutusucevap($data);
			}
			}
			
			
			
			
		
			
		}
	}
	
	
	public function urunrecetesiekle(){
		
		
			$this->load->model("Insertt");
		    if($_POST){
			 

				
				 $kod = $this->input->post('kod');                       
				 $ad = $this->input->post('ad');                       
				 $alis = $this->input->post('alis');                       
				 $stok = $this->input->post('stok');                       
			                     
				                   
				                       
             
				
				$data = array(
				"urun_kodu" => $kod,
				"urun_adi" => $ad,
				"alis_fiyati" => $alis,
				"stok" => $stok,
		
				);
                
				
				
				$result = $this->Insertt->recete($data);
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla eklendi.
                                    </div>');
					redirect('urunekleyonlendir/recete');
			   }   
		
	   }
		
		
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("urunrecetesiekle");
		$this->load->view("includes/footer");
	}
	
	
	public function isemirionayla($id){
		
		$this->load->model("updatee");
		
		$urunid = $this->input->post("uretimid");
		
		$result = $this->updatee->isemirionayla($id);
		
		$result1 = $this->updatee->urunmodulstokguncelle($urunid);
		
		$stokguncelle = $this->selectt->urunstokgetir($urunid,$id);
		
		
		if($result){
			echo "1";
		} 
		
	}
	
	public function isemirionaylama($id){
		$this->load->model("updatee");
		$result = $this->updatee->isemirionaylama($id);
		
		if($result){
			echo "1";
		}
		
	}
	
	public function isemirisil($id){
		$this->load->model("deletee");
		$result = $this->deletee->isemirisil($id);
		
		if($result){
			echo "1";
		}
		
	}
	
	
	
	
	public function onaylanmisisemirleri(){
		
		$this->load->model("selectt");
		
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] =$this->selectt->onaylanmisisemirlericount();  
		$config['base_url'] = site_url()."onaylanmisisemirleri/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->onaylanmisisemirleri($config['per_page'], $offset);
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("onaylanmis",$data);
		$this->load->view("includes/footer");
	}
	
	
	
	public function profilduzenle(){
		
		$this->load->model("updatee");
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		
		
		if($_POST){
			
			
			    $path = "uploads/";
			    $config['upload_path']          = $path;
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000000;
                $config['encrypt_name'] = TRUE;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
				
				
				
				if($_FILES["file"]["name"]){
				
				if ( ! $this->upload->do_upload('file'))
				{
                        $error = array('error' => $this->upload->display_errors());
                        
                        $this->session->set_flashdata('alert','<div class="alert alert-danger"> '.$error["error"].'
                                    </div>');
					     redirect('profilduzenle');
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
						$uyebilgi->uye_foto = $data["upload_data"]["file_name"];
						
                }
				
				}
			
			$uyead = $this->input->post("uyead");
			$uyesoyad = $this->input->post("uyesoyad");
			$uyemail = $this->input->post("uyemail");
			$uyesifre = $this->input->post("uyesifre");
			$uyekuladi = $this->input->post("uyekuladi");
			$uyerutbe = $this->input->post("uyerutbe");
			
			
			
			$data = array(
			"uye_ad" => $uyead,
			"uye_soyad" => $uyesoyad,
			"uye_turu" => $uyerutbe,
			"uye_email" => $uyemail,
			"uye_kuladi" => $uyekuladi,
			"uye_sifre" => $uyesifre,
			"uye_foto" => $uyebilgi->uye_foto,
			);
			
			$result = $this->updatee->profilduzenleupdate($uyebilgi->uye_id,$data);
			
			if($result){
				$this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Güncellendi.
                                    </div>');
					redirect('profilduzenle');
			}
			
		}
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("profilduzenle");
		$this->load->view("includes/footer");
	}
	
	
	
	public function bildirimler(){
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		$this->load->model("selectt");
		
		$data = $this->selectt->bildirimler($uyebilgi->uye_id);
		
		foreach($data as $yaz){
			$adsoyad =  $this->selectt->gonderencek($yaz->mesaj_gonderen_id);
			
			
			
			echo '<li class="list-group-item">';
			echo '<a href='.base_url("/mesajgoster/".$yaz->mesaj_id) .' role="button" tabindex="0" class="media">';
			echo '<span class="pull-left media-object thumb thumb-sm"><img src="'; echo base_url("uploads/").$adsoyad->uye_foto;
			echo '"></span><div class="media-body"><span class="block">';
			echo $adsoyad->uye_ad . " ".$adsoyad->uye_soyad;
			echo ' sana bir mesaj gönderdi</span>';
			echo '<small class="text-muted">'; echo $yaz->mesaj_icerik; 
			echo '</small></div></a></li>';
		}
		
	}
	
	
	public function uretimekle(){
		
			$this->load->model("Insertt");
			$this->load->model("selectt");
			$this->load->model("updatee");
			
			$data['sericek'] =$this->selectt->sericek();
			
		    if($_POST){
			 

				
				 $kod = $this->input->post('kod');                       
				 $seri = $this->input->post('serino');                                             
				 $adet = $this->input->post('adet');                       
				 $aciklama = $this->input->post('aciklama');                       
				 $urunid = $this->input->post('urunid');                       
			                     
				                   
				                       
             
				
				$data = array(
				"uretim_kodu" => $kod,
				"uretim_seriadi" => $seri,
				"urun_adet" => $adet,
				"urun_aciklamasi" => $aciklama,
		        "urun_id" => $urunid, 
				);
                
				
				
				
				$result = $this->Insertt->uretimeklese($data);
				
				
				
				
				
				
               
			   if($result){
				  
				   $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi.
                                    </div>');
					redirect('uretimekle');
			   }   
		
	   }
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("uretimekle",$data);
		$this->load->view("includes/footer");
	}
	
	/*** FEAXER SHOW 2 ***/
	
	public function tedarikcifirmalar($id){
		
		$this->load->model("insertt");
		$this->load->model("updatee");
		
		if($_POST){
			
			
			$urunadi = $this->input->post("urunadi");
			$birim = $this->input->post("birim");
			$adet = $this->input->post("adet");
			$fiyat = $this->input->post("fiyat");
			$kdv = $this->input->post("kdv");
			
			
			$say = -1;
			foreach($urunadi as $yaz){
				$say++;
				
				$data = array(
				"urun_adi" => $urunadi[$say],
				"birim" => $birim[$say],
				"adet" => $adet[$say],
				"fiyat" => $fiyat[$say],
				"kdv" => $kdv[$say],
				"firma" => $id,
				);
				
				$result = $this->insertt->tedarikciekle($data);
				
				$result1 = $this->updatee->tedarikcistok($urunadi[$say],$adet[$say]);
				
				
				
			}
			
			
			if($result){
				 $this->session->set_flashdata('alert','<div class="alert alert-success"> Başarıyla Eklendi.
                                    </div>');
					redirect('tedarikcifirmalar/'.$id);
			}
			
			
			
		}
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("tedarikci",$data);
		$this->load->view("includes/footer");
		
	}
	
	public function tederakciajax($id){
		$this->load->model("selectt");
		
		$sec = $this->selectt->firmasecici($id);
		
		foreach($sec as $yaz){
			echo "<option value='".$yaz->firma_turu."'>".$yaz->firma_adi."</option>";
		}
		
	}
	
	public function tedarikciistatistik($id){
		
		$this->load->model("selectt");
		
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] =$this->selectt->tedarikciistatistikcount($id);  
		$config['base_url'] = site_url()."tedarikciistatistik/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->tedarikciistatistik($config['per_page'], $offset,$id);
		
		echo "<table> 
		<th>id</th>
		<th>Ürün Adı</th>
		<th>Birim</th>
		<th>Adet</th>
		<th>Fiyat</th>
		<th>Kdv</th>	
		";
		
		foreach($data["veriler"] as $yaz){
			
			$urun = $this->selectt->uruncekecen($yaz->urun_adi);
			
			echo "<tr><td>".$yaz->firma."</td>";
			echo "<td>".$urun->urun_adi."</td>";
			echo "<td>".$yaz->birim."</td>";
			echo "<td>".$yaz->adet."</td>";
			echo "<td>".$yaz->fiyat."</td>";
			echo "<td>".$yaz->kdv."</td>";
			echo "</tr>";
		}
		     
		
		
		echo "</table>";
		
		
		echo "
	  
	  <style type='text/css'>
	  
	  table tr td{
		  border:1px solid #e1e1e1;
		  padding:5px;
	  }
	  
	  th{
		  border:1px solid #e1e1e1;
		  padding:5px;
	  }
	  
	  </style>
	  
	  ";
		
		
	}
	
	
	public function siparisgecmisi(){
		
		$this->load->model("selectt");
		
		$uyebilgi = $this->session->userdata("uyebilgi");
		
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 10;   
		$config['total_rows'] = $this->selectt->siparisgecmisicount($uyebilgi->musteri_id);  
		$config['base_url'] = site_url()."siparisgecmisi/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->siparisgecmisi($config['per_page'], $offset,$uyebilgi->musteri_id);
		
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("siparisgecmisi",$data);
		$this->load->view("includes/footer");
	}
	
	public function kasayonetimi(){
    
    $this->load->model("Selectt");
    $this->load->model("Insertt");
     
     $data1['kasaadicek'] = $this->selectt->kasaadcek();
    
  
     
    
    
     if($_POST){
       $hesaptur = $this->input->post('hesaptur');
        
      if($hesaptur==""){ $hesaptur=0;}
      
        
        
        $odetarih = $this->input->post('alistarihi'); 
        
      if($odetarih==""){ $odetarih=0;}
      
      
        $miktar = $this->input->post('ciro');
        
        
        
        
if($miktar==""){ $miktar=0;}

$kesidici = $this->input->post('kesidici');
if($kesidici==""){ $kesidici=0;}

$muhattap = $this->input->post('muhattap');
if($muhattap==""){ $muhattap=0;}
$bankahesabi = $this->input->post('bankahesabi');
if($bankahesabi==""){ $bankahesabi=0;}

        $uyeid = $this->input->post('uyeid'); 


  $banka_adi = $this->input->post('bankaadi');
  
  if($banka_adi==""){ $banka_adi=0;}
  
    $hesapturu = $this->input->post('hesapturu');
    $kasacek = $this->input->post('kasacek');
    $odemeturu = $this->input->post('odemeturu');
  
  if($odemeturu==""){ $odemeturu=0;}

         $tl_formati = number_format($miktar, -2, ',', '.');
          if($tl_formati==""){ $tl_formati=0;}
  
      
                
        $date = date("d.m.Y");
        
        $data = array(
         "musteri_id" =>$uyeid,
        "odenen_miktar" =>$tl_formati,
        "ekleme_tarihi" =>$date,
        "odeme_tarih" => $odetarih,
        "kesidici" => $kesidici,
        "muhattap" => $muhattap,
        "uye_id" => $uyeid,
        "banka_hesabi" =>$bankahesabi,
        "banka_adi" =>$banka_adi,
        "odemeturu" =>$odemeturu,
        "hesapturu" =>$hesapturu,
        "kasaadi" =>$kasacek,
        );
                
        
        
        $result = $this->Insertt->odegider($data);
               
         if($result){
          
           $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Ödeme Başarılı</div><audio controls autoplay style="display:none;"  ><source 
         src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
         }   
      
    }

    $this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("kasayonetimi",$data1);
    $this->load->view("includes/footer");
  }
  
  public function urunserisil($al){
	  $this->load->model("deletee");
	  $result = $this->deletee->urunserisil($al);
	  
	     if($result){
          
           $this->session->set_flashdata("alert",'<div class="alert alert-success"> Seri Silindi</div>');
		   redirect('urunseriekle');
         }  
	  
  }
  
  
  public function tekliurunsericek($al){
	  $this->load->model("selectt");
	  
	  $result = $this->selectt->tekliurunsericek($al);
	  
	  echo $result->urun_seriadi .",";
	  echo $result->id;
	  
  }
  
  public function urunseriduzenle(){
	  $this->load->model("updatee");
	  
	  $id = $this->input->post("urunseridduzenle");
	  $deger = $this->input->post("seri");
	  
	  $data = array(
	  "id" => $id,
	  "urun_seriadi" => $deger,
	  );
	  
	  $result = $this->updatee->urunseriduzenle($id,$deger);
	  
	  if($result){
          
           $this->session->set_flashdata("alert",'<div class="alert alert-success"> Seri Düzenlendi</div>');
		   redirect('urunseriekle');
         }
	  
  }
  
  public function kasalistesi(){
	  
	    $this->load->model("selectt");
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->kasalistesicount();  
		$config['base_url'] = site_url()."kasalistesi/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["kasa"] = $this->selectt->kasagiderleri($config['per_page'], $offset);
	
	
	  
	  
	$this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("kasalistesi",$data);
    $this->load->view("includes/footer");
  }
  
  
  
  public function firmaodeme($ald){
    
    
    $this->load->model("selectt");
     
     $data1['caris'] = $this->selectt->firmacari($ald);
    $caricek = $data1['cari']->firma_cari;
    
      
     $this->load->model("Insertt");
     if($_POST){
       $hesaptur = $this->input->post('hesaptur');
        
      if($hesaptur==""){ $hesaptur=0;}
      
        
        
        $odetarih = $this->input->post('alistarihi'); 
        
      if($odetarih==""){ $odetarih=0;}
      
      
        $miktar = $this->input->post('ciro');
        
        
        
        
if($miktar==""){ $miktar=0;}

$kesidici = $this->input->post('kesidici');
if($kesidici==""){ $kesidici=0;}

$muhattap = $this->input->post('muhattap');
if($muhattap==""){ $muhattap=0;}
$bankahesabi = $this->input->post('bankahesabi');
if($bankahesabi==""){ $bankahesabi=0;}

        $uyeid = $this->input->post('uyeid'); 


  $banka_adi = $this->input->post('bankaadi');
  
  if($banka_adi==""){ $banka_adi=0;}
  
    $odemeturu = $this->input->post('odemeturu');
  
  if($odemeturu==""){ $odemeturu=0;}

         $tl_formati = number_format($miktar, -2, ',', '.');
          if($tl_formati==""){ $tl_formati=0;}
  
                           
       $data1['cari'] = $this->selectt->firmacari($ald);
                
        $date = date("d.m.Y");
        
        $data = array(
         "musteri_id" =>$ald,
        "odenen_miktar" =>$tl_formati,
        "ekleme_tarihi" =>$date,
        "odeme_tarih" => $odetarih,
        "kesidici" => $kesidici,
        "muhattap" => $muhattap,
        "uye_id" => $uyeid,
        "banka_hesabi" =>$bankahesabi,
        "banka_adi" =>$banka_adi,
        "odemeturu" =>$odemeturu,
        );
                
        
        
        $result = $this->Insertt->odeeklefirma($data);
               
         if($result){
          
           $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Ödeme Başarılı</div><audio controls autoplay style="display:none;"  ><source 
         src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
         }   
      
    }
	
	$this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("firmaodeme",$data1);
    $this->load->view("includes/footer");
	
	
  }
  
  
  public function urunekleyonlendir(){

    $this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("urunekleyonlendir");
    $this->load->view("includes/footer");
  }
  
  public function iadeedilecekurun($id){
	  
	$this->load->model("selectt");
	$this->load->model("insertt");
	$this->load->model("updatee");
	
	$emre["urun"] = $this->selectt->sipariscektekli($id);
	
	
	
	
	if($_POST){
	   
	  
	  $notekle = $this->input->post("aciklama");
	  $urunid = $this->input->post("urunid");
	  $siparisid = $this->input->post("siparisid");
	  $iadeadet = $this->input->post("iadeadet");
	  $mevcutadet = $this->input->post("mevcutadet");
	  
	  $data = array(
	  "siparis_id" => $siparisid,
	  "urun_id" => $urunid,
	  "iade_adet" => $iadeadet,
	  "mevcut_adet" => $mevcutadet,
	  "notekle" => $notekle,
	  );
	  
	 $adet = $mevcutadet - $iadeadet;
	 $sonuc = $emre["urun"]->fiyat * $adet; 
	
	  $result = $this->insertt->iadeurunekle($data);
	  $result1 = $this->updatee->iadeurunupdate($id,$iadeadet);
	  $result2 = $this->updatee->iademustericari($emre["urun"]->musteri_id,$sonuc);
	  
	  if($result){
		   $this->session->set_flashdata("alert",'<div class="alert alert-success"> İade Ürün Eklendi.</div>');
		   redirect('iadeedilecekurun/'.$id);
	  }
	  
	   
	}
	
  
    $this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("iadedilecek",$emre);
    $this->load->view("includes/footer");
  }
  
  
  public function iadeurunlistesi(){
	  
	  
	    $config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->iadeuruncounts();  
		$config['base_url'] = site_url()."iadeurunlistesi/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->iadeurunlistesi($config['per_page'], $offset);  
	  
	  
   
    $this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("iadeurunlistesi",$data);
    $this->load->view("includes/footer");   

	
  }
  
  public function uretimmodululistesi(){
	  
	$this->load->model("selectt");
	
	
	$data["modul"] = $this->selectt->urunmodulcek();
	
	
	
	
	$this->load->view("includes/header");
    $this->load->view("includes/sidebar");
    $this->load->view("uretimlistesi",$data);
    $this->load->view("includes/footer");
  }
  
  public function urunparcalistesi($id){
	  
	  $this->load->model("selectt");
	  
	  $urun = $this->selectt->urunmodultekli($id);
	  
	  $urunadi = $this->selectt->uruncekecen($urun->urun_id);
	  echo "<table>";
	  echo "<th>".$urunadi->urun_adi . "</th>";
	  
	  $parca = $this->selectt->urunparcacek($id);
	  
	  foreach($parca as $yaz){
		  echo "<tr>";
		  $kad = $this->selectt->uruncetesicek($yaz->urun_recete_id);
		  echo "<td>".$kad->urun_adi . "</td>";
		  echo "</tr>";
	  }
	  
	  echo "</table>";
	  
	  echo "
	  
	  <style type='text/css'>
	  
	  table tr td{
		  border:1px solid #e1e1e1;
	  }
	  
	  </style>
	  
	  ";
	  
  }
  
  
  public function senetcek(){
	$url = $this->uri->segment(2);
	  
	  	$this->load->model("Updatee");
	  
	    $this->load->model("selectt");
		
		if($url=="vcekliste"){
			
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->cekcek();  
		$config['base_url'] = site_url()."senetcek/vcekliste/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->cekliste($config['per_page'], $offset);
	
		}
		else if($url=="vsenetliste"){
			
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->cekceks();  
		$config['base_url'] = site_url()."senetcek/vsenetliste/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->ceklistes($config['per_page'], $offset);
			
			
			
			
			
		}else if($url=="acekliste"){
			
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->cekcekss();  
		$config['base_url'] = site_url()."senetcek/acekliste/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->ceklistess($config['per_page'], $offset);
			
			
			
			
			
		}else if($url=="asenetliste"){
			
		$config['full_tag_open']    = '<ul class="paginationer">';
		$config['full_tag_close']   = "</ul>";
		$config['cur_tag_open']     = "<a class='active'>";
        $config['cur_tag_close']    = "</a>";
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 2;  
		$config['per_page'] = 5;   
		$config['total_rows'] = $this->selectt->cekceksss();  
		$config['base_url'] = site_url()."senetcek/asenetliste/"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}
		
		else
		{
			$offset = $sayfa; 
		}
		
		$data["veriler"] = $this->selectt->ceklistessa($config['per_page'], $offset);
			
			
			
			
			
		}
		 
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("senetcek",$data);
		$this->load->view("includes/footer");
	 }

	 
  
 public function vceklistes($id){
			  			  $this->load->model('updatee');

		$result =  $this->updatee->cekupdate($id);
				
					if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Çek Ödendi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('senetcek/vcekliste');
			
		
			}
	  
		  
	 }
	 
	 
	 public function vsenetlistes($id){
			  			  $this->load->model('updatee');

		$result =  $this->updatee->cekupdates($id);
				
					if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Senet Ödendi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('senetcek/vsenetliste');
			
		
			}
	  
		  
	 }
	public function aceklistes($id){
			  			  $this->load->model('updatee');

		$result =  $this->updatee->cekupdatess($id);
				
					if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Çek Ödendi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('senetcek/acekliste');
			
		
			}
	  
		  
	 }
		public function asenetliste($id){
			  			  $this->load->model('updatee');

		$result =  $this->updatee->cekupdatesss($id);
				
					if($result){
			  $this->session->set_flashdata("basarili",'<div class="alert alert-info"> Senet Ödendi !</div><audio controls autoplay style="display:none;"  ><source 
			   src="'.base_url('').'/uploads/alarm.mp3" type="audio/mpeg"></audio>');
				
				redirect('senetcek/asenetliste');
			
		
			}
	  
		  
	 }


  
  
  public function beklemedekisiparisler(){
		
			
		  $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->beklemecount();  
		$config['base_url'] = site_url()."yonetimpaneli/beklemedekisiparisler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->beklemelistesi($config['per_page'], $offset);  
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("beklemedekisiparisler",$data);
		$this->load->view("includes/footer");
		
	}
	
	
	public function beklemeyeal($id){
		$this->load->model("updatee");
		
		$result = $this->updatee->beklemeyeal($id);
		
		if($result){
		   $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Bekleme Listesine eklendi </div>');
		   redirect('onaylanmamissiparisler');
	  }
		
	}
	
	public function beklemedencikart($id){
		$this->load->model("updatee");
		
		$result = $this->updatee->beklemedencikart($id);
		
		if($result){
		   $this->session->set_flashdata("basarili",'<div class="alert alert-success"> Bekleme Listesinden Çıkarıldı </div>');
		   redirect('beklemedekisiparisler');
	  }
	}
	
	function devirislemleri(){
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("devirislemleri");
		$this->load->view("includes/footer");
	}
	
	
	public function firmadevri(){
		
		
		$this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->toplamfirma();  
		$config['base_url'] = site_url()."yonetimpaneli/firmalar"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->firmaliste($config['per_page'], $offset);  
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("firmadevri",$data);
		$this->load->view("includes/footer");
	}
	
	
	function firmattir(){
		$this->load->model("updatee");
		
		echo $id = $this->input->post("id");
		echo $text = $this->input->post("text");
		
		$data = array(
		"cari" => $text,
		);
		
		$sec = $this->updatee->firmattir($id,$data);
	}
	
	
	public function musteridevri(){
		
		
		$this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->musteritoplam();  
		$config['base_url'] = site_url()."yonetimpaneli/firmalar"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["verilers"] = $this->selectt->musteriliste($config['per_page'], $offset);  
		
		
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("musteridevri",$data);
		$this->load->view("includes/footer");
	}
  
  
	
	public function musteriattir(){
		$this->load->model("updatee");
		
		$id = $this->input->post("id");
		$text = $this->input->post("text");
		
		$data = array(
		"musteri_cari" => $text,
		);
		
		$sec = $this->updatee->musteriattir($id,$data);
	}
	
	public function gecmissiparisler(){
		
		$uyebilgi = $this->session->userdata("uyebilgi");
	    
	    $this->load->model("selectt");
        $config["first_tag_open"] = '<ul class="pagination">';
		$config["last_link"] = "Son";
		$config["first_link"] = "İlk";
		$config['uri_segment'] = 3;  
		$config['per_page'] =10;   
		$config['total_rows'] =$this->selectt->musterigecmiscounts($uyebilgi->musteri_id);  
		$config['base_url'] = site_url()."yonetimpaneli/onaylanmamissiparisler"; 
		$config['use_page_numbers'] = TRUE;  
		$this->pagination->initialize($config);  
		$data["linkler"] = $this->pagination->create_links();  
		$sayfa = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0; 
	
	   
	   

		if($sayfa > 0)  
		{
			$offset = ($sayfa*$config['per_page']) - $config['per_page'];  
		}else
		{
			$offset = $sayfa; 
		}
		$data["veriler"] = $this->selectt->musterigecmisliste($config['per_page'], $offset,$uyebilgi->musteri_id);  
		 
		
		

		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("musterigecmis",$data);
		$this->load->view("includes/footer");
	}
	
	public function ayarlar(){
		
		$this->load->model("selectt");
		
		$data["rutbe"] = $this->selectt->rutbelist();
		
		$this->load->view("includes/header");
		$this->load->view("includes/sidebar");
		$this->load->view("ayarlar",$data);
		$this->load->view("includes/footer");
	}
	
	
	public function rutbeekle(){
		$this->load->model("Insertt");
		
		$ad = $this->input->post("rutbeadi");
		
		$result = $this->insertt->rutbeklem($ad);
		
		if($result){
			echo "1";
		}
		
	}
	
	
	public function rutbegroup($id){
		
		$this->load->model("selectt");
		
		$cek = $this->selectt->rutbegroup($id);
		
		foreach($cek as $yaz){
			echo $yaz->durum . ",";
		}
		
	}
	
	public function rutbedit(){
		
		$this->load->model("updatee");
		$this->load->model("selectt");
		
		$rutbe = $this->input->post("rutbe");
		
		
		
		
		$a[1] = $this->input->post("mesajlarim");
		$a[2] = $this->input->post("uyeyonetimi");
		$a[3] = $this->input->post("musteriyonetimi");
		$a[4] = $this->input->post("firmayonetimi");
		$a[5] = $this->input->post("urunyonetimi");
		$a[6] = $this->input->post("muhasebe");
		$a[7] = $this->input->post("siparisyonetimi");
		$a[8] = $this->input->post("isemirleri");
		$a[9] = $this->input->post("devirislemleri");
		
			
		
			
			
		$cek = $this->selectt->rutbegroup($rutbe);
		
		$say = 0;
		foreach($cek as $yaz){
			
			$say++;
			
			$this->updatee->rutbedit($yaz->id,$a[$say]);
			
		}
		
	}
	
	
    public function submenuedit(){
		
		$this->load->model("updatee");
		$this->load->model("selectt");
	
    }
	
	
	
	
	
	
	
	
	
	 

}