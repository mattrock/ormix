<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class yonetim extends CI_Controller {
	

	public function index()
	{
		$this->load->view('giris');
	}
	
	public function logincheck(){
	   
	   $this->load->model("selectt"); 
	   
	   $this->form_validation->set_rules('kuladi','Kullanıcı adı','trim|required|xss_clean');
	   $this->form_validation->set_rules('sifre','Şifre','trim|required|xss_clean');
	   $this->form_validation->set_message('required','<div class="alert alert-danger"> Hata!
                                    %s Boş Olamaz. </div>');
	   
	   if($this->form_validation->run()){
		   
		   $kuladi = $this->input->post("kuladi",1);
		   $sifre = $this->input->post("sifre",1);
		   
		   $result = $this->selectt->logincheck($kuladi,$sifre);
		   $result1 = $this->selectt->logincheck1($kuladi,$sifre);
		   
		   if($result){
			   
			   $kontrol = $this->session->set_userdata("kontrol",1);
			   $uyebilgi = $this->session->set_userdata("uyebilgi",$result);
			   $this->session->set_userdata("logs",1);
			   redirect('yonetimpaneli');
		   }
		   else{
			   
			   if($result1){
				   $kontrol = $this->session->set_userdata("kontrol",1);
			       $uyebilgi = $this->session->set_userdata("uyebilgi",$result1);
				   $this->session->set_userdata("logs",1);
				   redirect('yonetimpaneli');
			   }
			   
			   
			   $this->session->set_flashdata('alert','<div class="alert alert-danger"> Hata!
                                    Kullanıcı adı veya Şifre Yanlış. </div>');
			   redirect('yonetim/index');
		   }
		   
	   }
	   
	   $this->load->view("giris");
	   
	}
}
