<?php 

class selectt extends CI_Model{
	
	
	function logincheck($kuladi,$sifre){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_kuladi",$kuladi)
		->where("uye_sifre",$sifre)
		->get()
		->row();
		 return $result;
	}
	
	function logincheck1($kuladi,$sifre){
		$result = $this->db->select("*")
		->from("musteriler")
		->where("musteri_kuladi",$kuladi)
		->where("musteri_sifre",$sifre)
		->get()
		->row();
		return $result;
	}
	
	
	
	function uyegetir($id){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_id",$id)
		->get()
		->row();
		return $result;
	}
	function toplamuyeler(){
	$result = $this->db->select("*")
	->from("uyeler")
	->get()
	->result();
	return count($result);
}


function listele($limit, $start)   
	{
		$this->db->order_by("uye_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("uyeler");
		
		return $veriler->result(); 
	}
	
	function ilgetir(){
		$result = $this->db->select("*")
		->from("iller")
		->get()
		->result();
		return $result;
	}
	
	function ilcegetir($id){
		$result = $this->db->select("*")
		->from("ilceler")
		->where("il_no",$id)
		->get()
		->result();
		return $result;
	}



function firmaliste($limit, $start)   
	{
		$this->db->order_by("firma_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("firmalar");
		
		return $veriler->result(); 
	}
	
function musteriliste($limit, $start)   
	{
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("musteriler");
		
		return $veriler->result(); 
	}

function toplamfirma(){
	$result = $this->db->select("*")
	->from("firmalar")
	->get()
	->result();
	return count($result);
}

function musteritoplam(){
	$result = $this->db->select("*")
	->from("musteriler")
	->get()
	->result();
	return count($result);
}

	function sericek(){
	$result = $this->db->select("*")
	->from("urunserileri")
	->get()
	->result();
	return $result;
}
function uruncekcek(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	return $result;
}

function cekliste($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->where("tursec","cek");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("firmaodeme");
		return $veriler->result(); 
	}

	function cekceks(){
	$result = $this->db->select("*")
	->from("firmaodeme")
	->where("tursec","senet")
	->get()
	->result();
	return count($result);
}


function ceklistes($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->where("tursec","senet");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("firmaodeme");
		return $veriler->result(); 
	}
	
  	function cekcekss(){
	$result = $this->db->select("*")
	->from("odemeler")
	->where("tursec","cek")
	->get()
	->result();
	return count($result);
}
function ceklistess($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->where("tursec","cek");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("odemeler");
		return $veriler->result(); 
	}
	function cekceksss(){
	$result = $this->db->select("*")
	->from("odemeler")
	->where("tursec","senet")
	->get()
	->result();
	return count($result);
}
function ceklistessa($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->where("tursec","senet");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("odemeler");
		return $veriler->result(); 
	}


function cekcek(){
	$result = $this->db->select("*")
	->from("firmaodeme")
	->where("tursec","cek")
	->get()
	->result();
	return count($result);
}


function listelestok($limit, $start)   
	{
		$this->db->order_by("urun_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunler");
		
		return $veriler->result(); 
	}
function toplamstok(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	return count($result);
}

function listelestok1($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunrecetesi");
		
		return $veriler->result(); 
	}
	
function toplamstok1(){
	$result = $this->db->select("*")
	->from("urunrecetesi")
	->get()
	->result();
	return count($result);
}

function urunadicek($id){
		$result = $this->db->select("*")
		->from("urunler")
		->where("urun_id",$id)
		->get()
		->row();
		return $result;
	}

function urunterm($id){
		$result = $this->db->query("SELECT * FROM urunler WHERE urun_adi LIKE '$id%' ");
		return $result->result();
	}
function urunseriterm($id){
		$result = $this->db->query("SELECT * FROM urunserileri WHERE urun_seriadi LIKE '$id%' ");
		return $result->result();
	}
	function firmaturcek($id){
		$result = $this->db->query("SELECT * FROM firmalar WHERE firma_turu='2' and firma_adi LIKE '$id%' ");
		return $result->result();
	}
	function seriseri($id){
		$result = $this->db->query("SELECT * FROM urunserileri WHERE urun_seriadi LIKE '$id%' ");
		return $result->result();
	}
	function toplamuruncek(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	return count($result);
}
function urunlistele($limit, $start)   
	{
		$this->db->order_by("urun_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunler");
		
		return $veriler->result(); 
	}
	function mustericeks($id){
		$result = $this->db->query("SELECT * FROM musteriler WHERE musteri_adi LIKE '$id%' ");
		return $result->result();
	}
	
	
function listelemusteri($limit, $start)   
	{
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("musteriler");
		
		return $veriler->result(); 
	}
		function toplammusteri(){
	$result = $this->db->select("*")
	->from("musteriler")
	->get()
	->result();
	return count($result);
}

function uyecekk($id){

	  $result = $this->db->select("*")
	  ->from("uyeler")
	  ->where("uye_id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  function caricek($ald){

	  $result = $this->db->select("*")
	  ->from("musteriler")
	  ->where("musteri_id",$ald)
	  ->get()
	  ->row();
	  return $result;
  }
  function siparisay($idd){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("musteri_id",$idd)
	->where_not_in('satis_onay',"1")
	->get()
	->result();
	return count($result);
}
  function siparisceksen($idd){
	  
	$result = $this->db->select("*")
	->from("siparisler")
	->where("musteri_id",$idd)
	->where_not_in('satis_onay',"1")
	->get()
	->result();
  return $result;
}
  function urungetircek($urun_id){
	$result = $this->db->select("*")
	->from("urunler")
	->where("urun_id",$urun_id)
	->get()
	->row();
  return $result;
}
function onaylistesi($limit, $start)   
	{
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$this->db->where_not_in("bekleme",1);
		$this->db->where("satis_onay",1);
	    $this->db->where("muhasebe_onay",0);
	    $this->db->group_by('musteri_id');
		$veriler = $this->db->get("siparisler");
		
		return $veriler->result(); 
	}			function musterigecmisliste($limit,$start,$id)   	{		$this->db->order_by("musteri_id DESC");		$this->db->limit($limit, $start); 	    $this->db->where("musteri_id",$id);	    $this->db->where("teslim",1);		$veriler = $this->db->get("siparisler");				return $veriler->result(); 	}

function toplamonay(){
	$result = $this->db->select("*")
	->from("siparisler")	
	->get()
	->result();
	return count($result);
}function musterigecmiscounts($id){	$result = $this->db->select("*")	->from("siparisler")    ->where("musteri_id",$id)    ->where("teslim",1)	->get()	->result();	return count($result);}

function beklemecount(){
	$result = $this->db->select("*")
	->from("siparisler")
    ->where("bekleme",1)	
    ->where("satis_onay",1)	
	->get()
	->result();
	return count($result);
}

function toplamonay1(){
	$result = $this->db->select("*")
	->from("siparisler")	
	->where("satis_onay",1)
	->where("muhasebe_onay",0)
	->group_by('musteri_id')
	->get()
	->result();
	return count($result);
}
function pazarlamaci($id){
	$result = $this->db->select("*")
	->from("uyeler")	
	->where("uye_id",$id)
	->get()
	->row();
  return $result;
}
function mustericekse($id){
	$result = $this->db->select("*")
	->from("musteriler")	
	->where("musteri_id",$id)
	->get()
	->row();
  return $result;
}
function siparisgorsun($all){
	$result = $this->db->select("*")
	->from("siparisler")	
	->where("musteri_id",$all)
	->where("satis_onay",1)
	->where("muhasebe_onay",0)
	->get()
	->result();
  return $result;
}
function siparisadd($all){
	$result = $this->db->select("*")
	->from("musteriler")	
	->where("musteri_id",$all)
	->get()
	->row();
  return $result;
}
  function siparisresmi($urun_id){
	$result = $this->db->select("*")
	->from("urunler")
	->where("urun_id",$urun_id)
	->get()
	->row();
  return $result;
}


function onaylistesi1($limit, $start)   
	{
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$this->db->where("satis_onay",1);
	    $this->db->where("muhasebe_onay",1);
	    $this->db->group_by('musteri_id');
		$veriler = $this->db->get("siparisler");
		
		return $veriler->result(); 
	}
	
function beklemelistesi($limit, $start)   
	{
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$this->db->where("satis_onay",1);
	    $this->db->where("muhasebe_onay",0);
		$this->db->where("bekleme",1);
	    $this->db->group_by('musteri_id');
		$veriler = $this->db->get("siparisler");
		
		return $veriler->result(); 
	}
	

	
	function toplamonay11(){
	$result = $this->db->select("*")
	->from("siparisler")
    ->where_not_in("bekleme",1)	
	->get()
	->result();
	return count($result);
}
function siparisgorsun1($all){
	$result = $this->db->select("*")
	->from("siparisler")	
	->where("musteri_id",$all)
	->where("satis_onay",1)
	->where("muhasebe_onay",1)
	->get()
	->result();
  return $result;
}
function siparisadd1($all){
	$result = $this->db->select("*")
	->from("musteriler")	
	->where("musteri_id",$all)
	->get()
	->row();
  return $result;
}
function arama($urunad){
	
	$result = $this->db->select("*")
	->from("urunler")	
	->like('urun_adi',$urunad)
	->get()
	->result();
  return $result;
	
	
}
 
 
 /*** FEAXER SHOW ***/
 
 function uyejson($gelen,$id){
	 $this->db->where_not_in('uye_id', $id);
	 $result = $this->db->select("*")
	 ->from("uyeler")
	 ->like("uye_ad",$gelen)
	 ->or_like("uye_soyad",$gelen)
	 ->get()
	 ->result();
	 $this->db->where_not_in('uye_id', $id);
	 return $result;
 }
 
 
 function gonderilenlercek($limit, $start,$id)   
	{
		$this->db->where_not_in("gonderen_sil","1");
	    $this->db->where_not_in("alan_sil","1");
		$this->db->where("gonderen_id",$id);
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("gonderilenler");
		
		return $veriler->result(); 
	}
	
	
	function gelenkutusucek($limit, $start,$id)   
	{
		
		
		
		$this->db->where("alan_id",$id);
		$this->db->where_not_in("gonderen_sil","1");
	    $this->db->where_not_in("alan_sil","1");
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler1 = $this->db->get("gelenkutusu");
		
		$this->db->where("alan_id",$id);
		$this->db->where_not_in("gonderen_sil","1");
	    $this->db->where_not_in("alan_sil","1");
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler2 = $this->db->get("cevap");
		
		
		
		return array_merge($veriler1->result(),$veriler2->result()); 
	}
	
  function gonderilenlercount($id){
	  $result = $this->db->select("*")
	  ->from("gonderilenler")
	  ->where("gonderen_id",$id)
	  ->where_not_in("gonderen_sil","1")
	  ->where_not_in("alan_sil","1")
	  ->get()
	  ->result();
	  
	  
	  return count($result);
  }
  
  function gelenkutusucount($id){
	  $result1 = $this->db->select("*")
	  ->from("gonderilenler")
	  ->where("gonderen_id",$id)
	  ->where_not_in("gonderen_sil","1")
	  ->where_not_in("alan_sil","1")
	  ->get()
	  ->result();
	  
	  $result2 = $this->db->select("*")
	  ->from("cevap")
	  ->where("gonderen_id",$id)
	  ->where_not_in("gonderen_sil","1")
	  ->where_not_in("alan_sil","1")
	  ->get()
	  ->result();
	  
	  return count($result1+$result2);
  }
  
  // mail alan kişinin bilgilerini çeker tekli.
  function alanidcektekli($id){
	  $result = $this->db->select("*")
	  ->from("uyeler")
	  ->where("uye_id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function mesajgoster($id){
	  $result = $this->db->select("*")
	  ->from("gelenkutusu")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function teklidosyacek($id){
	  $result = $this->db->select("*")
	  ->from("ekler")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function gonderenteklicek($id){
	  $result = $this->db->select("*")
	  ->from("gonderilenler")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function gelenteklicek($id){
	  $result = $this->db->select("*")
	  ->from("gelenkutusu")
	  ->where("id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function konusmacek($id){
	  $result = $this->db->select("*")
	  ->from("mesajlasmalar")
	  ->where("mesaj_id",$id)
	  ->get()
	  ->result();
	  return $result;
  }
  
  
  function birimgetir($id){
	  $result = $this->db->select("*")
	  ->from("siparisler")
	  ->where("urun_id",$id)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function cevapmesajlasmalar($id){
	  $result = $this->db->select("*")
	  ->from("mesajlasmalar")
	  ->where("goruldu","0")
	  ->where_not_in("mesaj_gonderen_id",$id)
	  ->get()
	  ->result();
	  return $result;
  }
  
  function cevapgelenkutusu($id,$uyeid){
	  $result = $this->db->select("*")
	  ->from("gelenkutusu")
	  ->where("id",$id)
	  ->where("gonderen_id",$uyeid)
	  ->get()
	  ->row();
	  return $result;
  }
  
  function urunrecetesijson($term){
	  $result = $this->db->select("*")
	  ->from("urunrecetesi")
	  ->like("urun_adi",$term)
	  ->get()
	  ->result();
	  return $result;
  }
  
  function pipo($term){
	  $result = $this->db->select("*")
	  ->from("urunler")
	  ->like("urun_adi",$term)
	  ->get()
	  ->result();
	  return $result;
  }
  
  
  function onaylanmamisisemirleri($limit, $start)   
	{
		$this->db->where("onay","0");
		$this->db->order_by("uretim_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("uretimler");
		
		return $veriler->result(); 
	}
	
	function onaylanmamisisemirlericount(){
		$result = $this->db->select("*")
		->from("uretimler")
		->where("onay","0")
		->get()
		->result();
		return count($result);
	}
	
	function onaylanmisisemirlericount(){
		$result = $this->db->select("*")
		->from("uretimler")
		->where("onay","1")
		->get()
		->result();
		return count($result);
	}
	
	function onaylanmisisemirleri($limit, $start)   
	{
		$this->db->where("onay","1");
		$this->db->order_by("uretim_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("uretimler");
		
		return $veriler->result(); 
	}
	
	function urunserisicek($id){
		$result = $this->db->select("*")
		->from("urunserileri")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function modultipicek($id){
		$result = $this->db->select("*")
		->from("urunserileri")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function modulparcasayisicount($id){
		
		
		$result = $this->db->select("*")
		->from("urunmodulu")
		->where("urun_id",$id)
		->get()
		->row();
		
		$result2 = $this->db->select("*")
		->from("urunparcalari")
		->where("urun_modul_id",$result->id)
		->get()
		->result();
		
		return count($result2);
		
	}
	
	function cevapcheck($id){
		$result = $this->db->select("*")
		->from("cevap")
		->where("id",$id)
		->get()
		->row();
		return $result;
	}
	
	function sonmesajlasmacek($id){
		$result = $this->db->select("*")
		->from("mesajlasmalar")
		->where("mesaj_id",$id)
		->where("goruldu","0")
	    ->order_by("id DESC")
		->get()
		->row();
		return $result;
	}
	
	function bildirimler($id){
		$result = $this->db->select("*")
		->from("mesajlasmalar")
		->where("mesaj_alan_id",$id)
		->where("goruldu","0")
		->get()
		->result();
		return $result;
	}
	
	function gonderencek($id){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_id",$id)
		->get()
		->row();
		return $result;
	}
	
	function urunstokgetir($id,$urunid){
		
		$result1 = $this->db->select("")
		->from("uretimler")
		->where("uretim_id",$urunid)
		->get()
		->row();
		
		$result2 = $this->db->select("")
		->from("urunstok")
		->where("urun_id",$id)
		->get()
		->row();
		
		$yenideger =  $result1->urun_adet + $result2->urun_stok;
		
		$this->db->where("urun_id",$id);
		$result3 = $this->db->update("urunstok",array("urun_stok" => $yenideger ));
		
		
		return $result3;
		
	}
  function pazarlamacicek(){

	  $result = $this->db->select("*")
	  ->from("uyeler")
	  ->where("uye_turu",1)
	  ->get()
	  ->result();
	  return $result;
  }
  
  /*** FEAXER SHOW 2 ***/
  
  function firmasecici($id){
	  
	  $result = $this->db->select("*")
	  ->from("firmalar")
	  ->where("firma_turu",$id)
	  ->get()
	  ->result();
	  return $result;
	  
  }
  
  function tedarikciistatistikcount($id){
		$result = $this->db->select("*")
		->from("tedarikcifirmalar")
		->where("firma",$id)
		->get()
		->result();
		return count($result);
  }
  
  function tedarikciistatistik($limit, $start,$id)   
	{
		$this->db->order_by("id DESC");
		$this->db->where("firma",$id);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("tedarikcifirmalar");
		return $veriler->result(); 
	}
	
	
	
	
	function tedarikcicek1($id){
		$result = $this->db->select("*")
		->from("firmalar")
		->where("firma_turu",$id)
		->get()
		->row();
		return $result;
	}
	
	function urunadicek1($id){
		$result = $this->db->select("*")
		->from("urunler")
		->where("urun_id",$id)
		->get()
		->row();
		return $result;
	}
	
	function serseri(){

	  $result = $this->db->select("*")
	  ->from("urunler")
	  ->group_by("urun_serisi")
	  ->get()
	  ->result();
	  return $result;
  }
  
  function toplamuruncek1($id){
	$result = $this->db->select("*")
	->from("urunler")
	->where("urun_serisi",$id)
	->get()
	->result();
	return count($result);
}

function urunlistele2($limit, $start,$id)   
	{
		$this->db->where("urun_serisi",$id);
		$this->db->order_by("urun_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunler");
		
		return $veriler->result(); 
	}
	
function uyeadicek($id){
		$result = $this->db->select("*")
		->from("uyeler")
		->where("uye_id",$id)
		->get()
		->row();
		return $result;
	}
	
function siparisgecmisicount($musterid){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("musteri_id",$musterid)
	->get()
	->result();
	return count($result);
}


function siparisgecmisi($limit, $start,$musterid)   
	{
		
		$this->db->where("musteri_id",$musterid);
		$this->db->order_by("siparis_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("siparisler");
		return $veriler->result(); 
	}
	
/*** 16.03.2018 : 15:52 FEAXER ****/

function bekleyensiparislercount(){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("muhasebe_onay",0)
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->fiyat;
	}
	
	if($sonuc == null){
		$sonuc = 0;
	}
	
	return $sonuc;
	
}

function tumgonderilenmalzemelercount(){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("satis_onay",1)
	->where("muhasebe_onay",2)
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->fiyat;
	}
	
	if($sonuc == null){
		$sonuc = 0;
	}
	
	return $sonuc;
}

function tumyapilantahsilatlarcount(){
	$result = $this->db->select("*")
	->from("odemeler")
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->odenen_miktar;
	}
	
	if($sonuc == null){
		$sonuc = 0;
	}
	
	return $sonuc;
}

function tamsiparis(){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("satis_onay",1)
	->where("muhasebe_onay",2)
	->order_by("musteri_id DESC")
	->get()
	->result();
	return count($result);
}

function urunlistele3($limit, $start)   
	{
		$this->db->where("satis_onay",1);
	    $this->db->where("muhasebe_onay",2);
		$this->db->order_by("musteri_id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("siparisler");
		
		return $veriler->result(); 
	}
	

function tamsiparsdetay($all){
	$result = $this->db->select("*")
	->from("siparisler")	
	->where("musteri_id",$all)
	->where("satis_onay",1)
	->where("muhasebe_onay",2)
	->get()
	->result();
  return $result;
}

function encoksatanurunler(){
	
	$result = $this->db->query("select count(urun_id),urun_id from siparisler group by urun_id LIMIT 0,4");
	return $result->result();
}

function uruncekecen($id){
	$result = $this->db->select("*")
	->from("urunler")
	->where("urun_id",$id)
	->get()
	->row();
	return $result;
}

function sonuyecek(){
	$result = $this->db->query("SELECT * FROM musteriler ORDER BY musteri_id DESC limit 0,5");
	return $result->result();
}

function kasaadcek(){
    $result = $this->db->select("*")
    ->from("kasaekle")
    ->get()
    ->result();
    return $result;
  }

function logcek(){
	$result = $this->db->query("SELECT * FROM logs order by id DESC LIMIT 0,10");
	return $result->result();
}

function urunsericekscount(){
	$result = $this->db->select("*")
	->from("urunserileri")
	->get()
	->result();
	return count($result);
}

function urunsericeks($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("urunserileri");
		return $veriler->result(); 
	}

function tekliurunsericek($id){
	$result = $this->db->select("*")
	->from("urunserileri")
	->where("id",$id)
	->get()
	->row();
	return $result;
}


function kasalistesicount(){
	$result = $this->db->select("*")
	->from("kasagiderleri")
	->get()
	->result();
	return count($result);
}


function kasalistesiz($ic){
	
	
	$result = $this->db->query("SELECT * FROM kasagiderleri WHERE ekleme_tarihi = '$ic' LIMIT 0,10");
	return $result->result();
}


function kasalistesigelir($ic){
	
	
	$result = $this->db->query("SELECT * FROM odemeler WHERE ekleme_tarihi = '$ic' LIMIT 0,10");
	return $result->result();
}



function teklimustericek($id){
	$result = $this->db->select("*")
	->from("uyeler")
	->where("uye_id",$id)
	->get()
	->row();
	return $result;
}


function teklicekic($id){
	$result = $this->db->select("*")
	->from("musteriler")
	->where("musteri_id",$id)
	->get()
	->row();
	return $result;
}

function kasagiderleri($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("kasagiderleri");
		return $veriler->result(); 
	}
	
function firmacari($ald){

    $result = $this->db->select("*")
    ->from("firmalar")
    ->where("firma_id",$ald)
    ->get()
    ->row();
    return $result;
  }
  
function iadeuruncount($id){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("musteri_id",$id)
	->where("satis_onay",1)
	->where("muhasebe_onay",2)
	->get()
	->result();
	return count($result);
}

function iadeurunlistele($limit, $start,$id)   
	{
		$this->db->order_by("siparis_id DESC");
		$this->db->where("musteri_id",$id);
		$this->db->where("satis_onay",1);
	     $this->db->where("muhasebe_onay",2);
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("siparisler");
		return $veriler->result(); 
	}
	
function sipariscektekli($id){
	$result = $this->db->select("*")
	->from("siparisler")
	->where("siparis_id",$id)
	->get()
	->row();
	return $result;
}

function iadeuruncounts(){
	$result = $this->db->select("*")
	->from("iadeurunler")
	->get()
	->result();
	return count($result);
}


function iadeurunlistesi($limit, $start)   
	{
		$this->db->order_by("id DESC");
		$this->db->limit($limit, $start); 
		$veriler = $this->db->get("iadeurunler");
		return $veriler->result(); 
	}

function urunmodulcek(){
	$result = $this->db->select("*")
	->from("urunmodulu")
	->get()
	->result();
	return $result;
}

function urunparcacek($id){
	$result = $this->db->select("*")
	->from("urunparcalari")
	->where("urun_modul_id",$id)
	->get()
	->result();
	
	return $result;
	
}

function uruncetesicek($id){
	$result1 = $this->db->select("*")
	->from("urunrecetesi")
	->where("id",$id)
	->get()
	->row();
	return $result1;
}

function urunmodultekli($id){
	$result = $this->db->select("*")
	->from("urunmodulu")
	->where("id",$id)
	->get()
	->row();
	return $result;
}

function depostokcount(){
	$result = $this->db->select("*")
	->from("urunler")
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->satis_fiyati;
	}
	
	return $sonuc;
	
}

function uretimstokcount(){
	$result = $this->db->select("*")
	->from("urunrecetesi")
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->alis_fiyati;
	}
	
	return $sonuc;
	
}

function toplammusteribakiyecount(){
	$result = $this->db->select("*")
	->from("musteriler")
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->musteri_cari;
	}
	
	return $sonuc;
	
}

function toplamfirmabakiyesicount(){
	$result = $this->db->select("*")
	->from("firmalar")
	->get()
	->result();
	
	foreach($result as $yaz){
		$sonuc = $sonuc + $yaz->cari;
	}
	
	return $sonuc;
	
}

function rutbelist(){
	$result = $this->db->select("*")
	->from("rutbelerim")
	->get()
	->result();
	return $result;
}

function rutbegroup($id){
	$result = $this->db->select("*")
	->from("grup_rutbe")
	->where("rutbe_id",$id)
	->get()
	->result();
	return $result;
}

function ustmenu($id){
	$result = $this->db->select("*")
	->from("ust_menu")
	->where("id",$id)
	->get()
	->row();
	return $result;
}

function teklir($id){
	$result = $this->db->select("*")
	->from("grup_rutbe")
	->where("rutbe_id",$id)
	->get()
	->row();
	return $result;
}



	



	
	
  



  
}