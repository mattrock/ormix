<?php 

class updatee extends CI_Model{
	
	  function uyeguncelle($id,$data1){
	  $this->db->where("uye_id",$id);
	  $result = $this->db->update("uyeler",$data1);
	  return $result;
  }
  	  function musteriguncelle($id,$data2){
	  $this->db->where("musteri_id",$id);
	  $result = $this->db->update("musteriler",$data2);
	  return $result;
  }
  
  function cekupdate($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("firmaodeme", array("durum" => 1));
	   return $result;
   }
	     function cekupdates($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("firmaodeme", array("durum" => 1));
	   return $result;
   }
	 
    function cekupdatess($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("odemeler", array("durum" => 1));
	   return $result;
   }
     function cekupdatesss($id){
	   $this->db->where("id",$id);
	   $result = $this->db->update("odemeler", array("durum" => 1));
	   return $result;
   }

  
    	  function firmaguncelle($id,$data3){
	  $this->db->where("firma_id",$id);
	  $result = $this->db->update("firmalar",$data3);
	  return $result;
  }
  	  function siparisonays($musteriid,$id){
	  $this->db->where("urun_id",$musteriid);
	  $this->db->where("musteri_id",$id);
	  $result = $this->db->update("siparisler",array("satis_onay" => 1));
	  return $result;
  }
  
  
  function siparisguncelle($musteriid){
	  $this->db->where("musteri_id",$musteriid);
	  $this->db->where("satis_onay",1);
	  $result = $this->db->update("siparisler",array("muhasebe_onay" => 1));
	  return $result;
	  
  }
    function muhasebeonay($all){
	  $this->db->where("musteri_id",$all);
	  $result = $this->db->update("siparisler",array("muhasebe_onay" => 1));
	  return $result;
  }
    function depoonays($musteriid){
	  $this->db->where("musteri_id",$musteriid);
	  $this->db->where("satis_onay",1);
	  $this->db->where("muhasebe_onay",1);
	  $result = $this->db->update("siparisler",array("muhasebe_onay" => 2));
	  return $result;
	  
  }
  
  /*** FEAXER SHOW ***/
  
  function gonderensil($data,$id){
	  $this->db->where("id",$id);
	  $result = $this->db->update("gonderilenler",$data);
	  return $result;
  }
  
  function gelenkutususil($data,$id){
	  $this->db->where("id",$id);
	  $result = $this->db->update("gelenkutusu",$data);
	  return $result;
  }
  
  function mesajekle($data){
	  $result = $this->db->insert("mesajlasma",$data);
	  return $result;
  }
  
  function mesajgoruldu($id){
	    $this->db->where("mesaj_id",$id);
		$result = $this->db->update("mesajlasmalar",array("goruldu" => 1));
		return $result; 
	}
	
  
  function urunstokguncelle($id,$data){
	  $this->db->where("urun_id",$id);
	  $result = $this->db->update("urunler",$data);
	  return $result;
  }
  
  function firmattir($id,$data){
	  $this->db->where("firma_id",$id);
	  $result = $this->db->update("firmalar",$data);
	  return $result;
  }
  
  function musteriattir($id,$data){
	  $this->db->where("musteri_id",$id);
	  $result = $this->db->update("musteriler",$data);
	  return $result;
  }
  
  function malzemestokguncelle($id,$data){
	  $this->db->where("id",$id);
	  $result = $this->db->update("urunrecetesi",$data);
	  return $result;
  }
  
  function urunmodulstokguncelle($id){
	 
	  $result1 = $this->db->select("*")
	  ->from("urunmodulu")
	  ->where("urun_id",$id)
	  ->get()
	  ->row();
	  
	  $result2 = $this->db->select("*")
	  ->from("urunparcalari")
	  ->where("urun_modul_id",$result1->id)
	  ->get()
	  ->result();
	  
	  foreach($result2 as $yaz){
		  
		  $result3 = $this->db->select("*")
		  ->from("urunrecetesi")
		  ->where("id",$yaz->urun_recete_id)
		  ->get()
		  ->row();
		  
		  $yenideger = $result3->stok - $yaz->urun_stok;
		  
		  $this->db->where("id",$yaz->urun_recete_id);
		  $result4 = $this->db->update("urunrecetesi",array("stok" => $yenideger));
		  
		  
	  }
	  
	  return $result4;
	  
  }
  
  function isemirionayla($id){
	  $this->db->where("uretim_id",$id);
	  $result = $this->db->update("uretimler",array("onay" => 1));
	  return $result;
  }
  
  function isemirionaylama($id){
	  $this->db->where("uretim_id",$id);
	  $result = $this->db->update("uretimler",array("onay" => 0));
	  return $result;
  }
  
  function profilduzenleupdate($id,$data){
		  $this->db->where("uye_id",$id);
		  $result = $this->db->update("uyeler",$data);
		  return $result;
	  }

  /*** FEAXER SHOW 2 ***/
  
  function tedarikcistok($urunadi,$adet){
	   $result = $this->db->select("*")
	   ->from("urunler")
	   ->where("urun_id",$urunadi)
	   ->get()
	   ->row();
	   
	   $yenideger = $result->stok + $adet;
	   
	   $this->db->where("urun_id",$urunadi);
	   $result2 = $this->db->update("urunler",array("stok" => $yenideger));
	   return $result2;
	   
   }
   
   
   function urunseriduzenle($id,$deger){
	   $this->db->where("id",$id);
	   $result = $this->db->update("urunserileri", array("urun_seriadi" => $deger));
	   return $result;
   }
   
   function iadeurunupdate($id,$adet){
	   $this->db->where("siparis_id",$id);
	   $result = $this->db->update("siparisler",array("adet" => $adet));
	   return $result;
   }
   
   function iademustericari($musteriid,$cari){
	   $result1 = $this->db->select("*")
	   ->from("musteriler")
	   ->where("musteri_id",$musteriid)
	   ->get()
	   ->row();
	   
	   $soncari = $result1->musteri_cari + $cari;
	   
	   $this->db->where("musteri_id",$musteriid);
	   $result = $this->db->update("musteriler",array("musteri_cari" => $soncari ));
   }
   
   
   function teslimdegistir($id){
	   $this->db->where("musteri_id",$id);
	   $this->db->where("satis_onay",1);
	   $this->db->where("muhasebe_onay",2);
	   $result = $this->db->update("siparisler", array("teslim" => 1));
	   return $result;
   }
   
   function beklemeyeal($id){
	  $this->db->where("siparis_id",$id);
	  $result = $this->db->update("siparisler",array("bekleme" => 1));
	  return $result;
   }
   
   function beklemedencikart($id){
	  $this->db->where("siparis_id",$id);
	  $result = $this->db->update("siparisler",array("bekleme" => 0));
	  return $result;
   }
   
   function rutbedit($id,$durum){
	   $this->db->where("id",$id);
	   $result = $this->db->update("grup_rutbe",array("durum" => $durum));
	   return $result;
   }
	  
  


}