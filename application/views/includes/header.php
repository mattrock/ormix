<?php $uyebilgi = $this->session->userdata("uyebilgi"); ?>
<!doctype html>
<html class="no-js " lang="tr">
<head>
<meta charset="utf-8">

<title>Ormix Su Armatürleri</title>
<link rel="icon" type="image/ico" href="<?php echo base_url(); ?>assets/assets/images/favicon.ico">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- vendor css files -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/js/vendor/bootstrap/bootstrap.min.css">    
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/vendor/animsition.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/js/vendor/morris/morris.css">      
<!-- project main css files -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/simplelightbox.min.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
</head>
<body id="falcon" class="main_Wrapper">
    <div id="wrap" class="animsition">
        <!-- HEADER Content -->
        <div id="header">
            <header class="clearfix">
                <!-- Branding -->
                <div class="branding">
                    <a class="brand" href="<?php echo base_url('yonetimpaneli'); ?>">
                        <span>Ormix Su Armatürleri</span>
                    </a>
                    <a role="button" tabindex="0" class="offcanvas-toggle visible-xs-inline">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
                <!-- Branding end -->

                <!-- Left-side navigation -->
                <ul class="nav-left pull-left list-unstyled list-inline">
                    <li class="leftmenu-collapse">
                        <a role="button" tabindex="0" class="collapse-leftmenu">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </li>
                   
                    
                </ul>
               

                
                <ul class="nav-right pull-right list-inline">
                 
                    <li class="dropdown messages">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <div class="notify">
                                <span class="heartbit" style="display:none"></span>
                                <span class="point"></span>
                            </div>
                        </a>
                        <div class="dropdown-menu pull-right panel panel-default" role="menu">
                            <ul class="list-group env">
							
                                
                                
                            </ul>
                            <div class="panel-footer">
                                <a href="<?php echo base_url("gelenkutusu"); ?>" role="button" tabindex="0">Mesajları Gör
                                    <i class="pull-right fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>
                    </li>
                
                    <li class="dropdown nav-profile">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo base_url("uploads/" . $uyebilgi->uye_foto); ?>" alt="" class="0 size-30x30"> </a>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li>
                                <div class="user-info">
                                    <div class="user-name"><?php echo $uyebilgi->uye_ad . " ".$uyebilgi->uye_soyad; ?></div>
                                    <div class="user-position online"><?php echo $uyebilgi->uye_turu == 4 ? "Yönetici":""; ?><?php echo $uyebilgi->uye_turu == 3 ? "Muhasebe":""; ?><?php echo $uyebilgi->uye_turu == 2 ? "Depo":""; ?><?php echo $uyebilgi->uye_turu == 1 ? "Pazarlama":""; ?></div>
                                </div>
                            </li>
                            <li>
                                <a href="<?php echo base_url("profilduzenle"); ?>" role="button" tabindex="0">
                                    
                                    <i class="fa fa-user"></i>Profilim</a>
                            </li>
                            
                            <li class="divider"></li>
                            
                            <li>
                                <a href="<?php echo base_url("yonetimpaneli/cikis"); ?>" role="button" tabindex="0">
                                    <i class="fa fa-sign-out"></i>Çıkış</a>
                            </li>
                        </ul>
                    </li>
                    
                </ul>
                <!-- Right-side navigation end -->
            </header>
        </div>
		
		<div class="cal">
		<audio controls id="player"><source src="<?php echo base_url("uploads/alert.mp3"); ?>" type='audio/mpeg'></audio>
		</div>
		
		<script>
		
		
		
		function bildirimler(){
			$.ajax({
				url:"<?php echo base_url("/yonetimpaneli/bildirimler"); ?>",
				type:"POST",
				success:function(r){
					$(".env").html(r);
					if(r == ""){
						$(".env").html("<p style='padding:10px; text-align:center;'>Yeni Mesajınız yok.</p>");
					
					}
					else{
					// cal();	
					 $(".heartbit").show();
					}
					
				}
			});
			
			
			
			
		}
		
		function cal(){
			document.getElementById('player').play();
		}
		
		bildirimler();
		
		
		
		</script>