</div>


    <!-- Vendor JavaScripts -->
    <script src="<?php echo base_url(); ?>assets/assets/bundles/libscripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bundles/vendorscripts.bundle.js"></script>

    <!--/ vendor javascripts -->
    <script src="<?php echo base_url(); ?>assets/assets/bundles/flotscripts.bundle.js"></script>    
    <script src="<?php echo base_url(); ?>assets/assets/bundles/d3cripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bundles/sparkline.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bundles/raphael.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bundles/morris.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/bundles/loadercripts.bundle.js"></script>

    <!-- page Js -->
    <script src="<?php echo base_url(); ?>assets/assets/bundles/mainscripts.bundle.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/js/page/index.js"></script>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/simple-lightbox.min.js"></script>
  
  <script>
  $( function() {
    
    $( "#tags" ).autocomplete({
      source: '/urunterm/'
    });
  } );
  
    $( function() {
    
    $( "#seri" ).autocomplete({
      source: '/urunseriterm/'
    });
  } );
  
  $( function() {
    
    $( "#tedarik" ).autocomplete({
      source: '/firmaturcekti/'
    });
  } );
  
     $( function() {
    
    $( "#seri" ).autocomplete({
      source: '/urunserileri/'
    });
  } );
  
   $( function() {
    
    $( "#ads" ).autocomplete({
      source: '/mustericekss/'
    });
  } );
  
  
  $( function() {
    
    $( "#urunids" ).autocomplete({
      source: 'yonetimpaneli/urunlerjson/',
	  select: function(index,ui){
		  console.log(ui.item);
		  $("#fabri").val(ui.item.id);
		  $("#kod").val(ui.item.kod);
	  }
    });
  } );
  
  $( function() {
    
    $( "#ryan" ).autocomplete({
      source: 'yonetimpaneli/urunlerjson/',
	  select: function(index,ui){
		  console.log(ui.item);
		  $("#babel").val(ui.item.id);
	  }
    });
  } );
  
   
  </script>
  
  
  <script>

  

  
  jQuery(document).on("DOMNodeInserted", function(){
	  
	  
	  
		  
	 
	  

	  
  $( "#parcadi*" ).autocomplete({
      source: "<?php echo base_url("yonetimpaneli/parcajson"); ?>",
	  select: function(index,ui){
		      var stok = ui.item.stok;
			  var indis = $(".parcam").index(this);
		      
			  $(".mibu").eq(indis).text(stok);
			  $(".paul").eq(indis).val(ui.item.id);
			  $("#parcastok").eq(indis).attr("max",stok);
			 
			  
	  }
    });
	
	
	
	$( ".tedarikci*" ).autocomplete({
      source: "<?php echo base_url("yonetimpaneli/urunlerjson"); ?>",
	  select: function(index,ui){
			  var indis = $(".tedarikci").index(this);
			  console.log(indis);
			  $("#pett*").eq(indis).val(ui.item.id);
			 
			  
	  }
    });
	
	
	

   });
   
   
   $(document).ready(function(){
	   
	   var gallery = $('.pw_img a').simpleLightbox();
	   
	   
	   
	   
	   
   });
  
  
</script>
  
  
   <style>
 
 .cpm a{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 margin-left:2px;
	 border-radius:3px;
	 color:#222;
 }
 
 .cpm strong{
	 border:1px solid #e1e1e1;
	 padding:5px;
	 color:#00bcd4;
 }
 
 #data-table-simple{
	 
 }
 
 .paginationer {
	 text-align:center;
     list-style: none;
 }
						
						ul.paginationer a {
                        
    margin-right: 2px;
    background: #f7f7f7;
    border: 1px solid #d1d1d1;
    border-radius: 2px;
    color: #000;
    display: inline-block;
    width: 25px;
	
    text-align: center;
                        }
						
						a.active {
    background: #337ab7 !important;
    color: #fff !important;
}
 
 
 </style>

	
</body>
</html>