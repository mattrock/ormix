<section id="content">
            <div class="page page-tables-footable">
                <!-- bradcome -->
                <div class="b-b mb-10">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Siparişler</h1>
                        </div>
                    </div>

                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">

                         <section class="boxs">
                            <div class="boxs-header">
                                <h2>Onaylanmış Siparişler</h2>
                            </div>
                            <div class="boxs-body">
                                <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom" style="text-align: center">
                                        <tr>
                                            <th>Siparis Tarihi</th>
                                            <th>Müşteri Adı</th>
                                            <th>Firma Adı</th>
                                            <th>Pazarlamacı Adı</th>               
                                            <th>İşlem</th>                
                               
                                        </tr>
                                                                        

                                    <tbody>
                              
                                            <?php foreach($veriler as $onay){ ?>
									<?php $pazarlamacis =$this->selectt->pazarlamaci($onay->uye_id); ?>
									<?php $musteri = $this->selectt->mustericekse($onay->musteri_id); ?>
											  <tr>
                                            <td><?php echo @$onay->tarih;  ?></td>
                                            <td><?php echo @$musteri->musteri_adi; ?></td>
                                            <td><?php echo @$musteri->firma_adi; ?></td>
                                            <td><?php echo @$pazarlamacis->uye_ad; ?></td>
                                            <td>
                                           <div class="col-lg-25">
                                            <button class="btn btn-raised btn-info btn-sm mr-10" data-toggle="modal" data-target="#myModal" title="Siparişleri Görüntüle" style="background-color: #09a0e9" onclick="siparis(<?php echo @$onay->musteri_id; ?>);"><i class="fa fa-file"></i></button>
                                                                 <button class="btn btn-raised btn-info btn-sm mr-10" title="Onayla"  onclick="onayolla(<?php echo @$onay->musteri_id; ?>);" style="background-color: green;"><i class="fa fa-plus"></i></button>        
                        
                                            </div>
											</td>
                                            </tr>
											
											<?php } ?> 
                                    </tbody>
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <ul class="pagination">
                                                </ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>	<div class="row">
         
				
                <div class="col s12 m12 cpm" style="background-color:#0099CC; height:30px; padding:5px;" >
				  
				   <?php echo $linkler; ?>
   
				 
  
  
             </div>     

</div>	
                            </div>
							
                        </section>
                    </div>
                </div>
            </div>
        </section>
		<script>
	
		function siparis(musteri_id){
			
             location.replace('yonetimpaneli/depo/'+musteri_id);
   
		}
				function onayolla(musteri_id){
			
			 var r = confirm("Onaylamak İçin Eminmisiniz ?");
	
    if (r == true) {
        location.replace('yonetimpaneli/depooll/'+musteri_id);
    } 
	else {
       return false;
    }	
			
		}
		</script>