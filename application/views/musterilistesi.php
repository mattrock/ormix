<div class="modal fade" id="myModal" role="dialog" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">

           
            <div class="modal-content">
                <div class="modal-header">
				
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Müşteri Güncelle</h4>
                </div>
                <div class="modal-body">
                   
				   
				         <form class="profile-settings" id="fileupload" action="<?php echo base_url('musteriguncelle'); ?>" method="POST" enctype="multipart/form-data">
				   
				     <div class="row">
                                                          
<div class="form-group col-sm-6">

<input type="hidden" class="uyeid1" value="" name="id">


                            <label for="username">Müşteri Adı</label>
                                <input type="text" name="ad" class="ad form-control" rows="5"  id="username" placeholder=" " value="" required="">
                            </div>
							
							<div class="form-group col-sm-6">
                            <label for="username">Müşteri Firma</label>
                                <input type="text" name="firma1" class="form-control" rows="5"  id="firmaci" placeholder="" value="" required="">
                            </div>
								
							<div class="form-group col-sm-6">
                            <label for="username">Müşteri Email</label>
                                <input type="text" name="mail" class="mail form-control" rows="5"  id="username" placeholder=" " value="" required="">
                            </div>
								<div class="form-group col-sm-6">
                            <label for="username">Müşteri Tel</label>
                                <input type="text" name="tel" class="tel form-control" rows="5"  id="username" placeholder="" value="" required="">
                            </div>
							<div class="form-group col-sm-6">
                            <label for="username">Müşteri Adres</label>
                                <input type="adres" name="adres" class="adres form-control" rows="5"  id="username" placeholder=" " value="" required="">
                            </div>
							   <div class="form-group col-sm-6">
                                                                <label for="username">Pazarlamaci Ata</label>
                                                                <select tabindex="3" id="pazarlar" class="chosen-select" name="pazarlamaci" required="">
                                                                   
                                                                        <option   value="" selected disabled>Seçiniz</option>
															<?php 			foreach($pazarlamacek as $pazar) {   ?>
                                                                        <option value="<?php echo $p = $pazar->uye_id; ?>"><?php echo $p = $pazar->uye_ad; ?> <?php echo $b = $pazar->uye_soyad; ?></option>
															<?php } ?>
                                                                    
                                                                </select>
                                                            </div>
							
							
							
                     </div>
				   
				   
				   
				   
				   
				   
				   
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-raised btn-primary">Müşteri Güncelle<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: 43.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div><div class="ripple ripple-on ripple-out" style="left: 63.875px; top: 22px; background-color: rgba(0, 0, 0, 0.87); transform: scale(9.375);"></div></div></button>
                </div>
				 </form>
            </div>
        </div>
    </div>



<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">MÜŞTERİLER</h1>
						</div>
					</div>
				</div>
<?php echo $this->session->userdata("silindi"); ?>
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
								       <div class="form-group col-sm-12">
                                                                <label for="username">Müşteri Adı<label>
                                                                <input type="text" name="ads" class="form-control" rows="5"  id="ads" placeholder="" required="">
                                                            </div>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ad-Soyad</th>
                                            <th>Firma Adı</th>
											<th>Firma-Tel</th>
                                            <th>E-Mail</th>
                                            <th>Borç</th>
                                            <th>Alacak</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
									
									<?php foreach($veriler as $yazdir){  ?>
									 <tr>
									      <input type="hidden" class="idcek" value="<?php echo $yazdir->musteri_id; ?>">
										  <input type="hidden" class="adrescek" value="<?php echo $yazdir->musteri_adres; ?>">
                                            <td class="adcek"><?php echo $yazdir->musteri_adi; ?></td>
                                            <td class="firma"><?php echo $yazdir->firma_adi; ?></td>
                                            <td class="telcek"><?php echo $yazdir->musteri_telefon; ?></td>
                                            <td class="mailcek"><?php echo $yazdir->musteri_mail; ?></td>
                                            <td class="pazarlamaci" style="display:none;"><?php echo $yazdir->gorevlipazarlama; ?></td>
                                            <td><?php echo $yazdir->musteri_cari; ?> TL</td>
                                            <td></td>
											<td>
                                            <div class="col-lg-15">
                                 <a href="<?php echo base_url('musteriodeme/').$yazdir->musteri_id; ?>"><button class="btn btn-raised btn-info btn-sm mr-10" title="Tahsilat Yap" style="background-color: green"><i class="fa fa-plus"></i></button>          
                                     <a href="<?php echo base_url('siparisler/').$yazdir->musteri_id; ?>"><button class="btn btn-raised btn-info btn-sm mr-10" title="Sipariş"><i class="fa fa-plus"></i></button>      
                                     </a>
                                     <button class="btn btn-raised btn-info btn-sm mr-10 duzenle" title="Düzenle" style="background-color: yellow" data-toggle="modal" data-target="#myModal"><i class="fa fa-file"></i></button>
                                    <button class="btn btn-raised btn-primary btn-sm" title="Sil"  style="background-color: red" onclick="sil(<?php echo @$yazdir->musteri_id; ?>);"><i class="fa fa-trash"></i></button>
                                        <a href="<?php echo base_url('iadeurunekle/').$yazdir->musteri_id; ?>"><button class="btn btn-raised btn-info btn-sm mr-10" title="İade Ürün Ekle"><i class="fa fa-exchange"></i></button>      
                                     </a>
											</div>
                                            </td>
                                            </tr>
                                       
									   
									<?php } ?>
									   
									   
									   
									   
									   
									   
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												<ul class="pagination">
												</ul>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
			<script>
		function sil(musteri_id){
			
			 var r = confirm("Silmek istediğinize emin misiniz ?");
	
    if (r == true) {
        location.replace('/yonetimpaneli/musterisil/'+musteri_id);
    } 
	else {
       return false;
    }	
			
		}
			$(document).ready(function(){
			
			$(".duzenle").click(function(){
				var say = $(".duzenle").index(this);
				
				var adcek = $(".adcek").eq(say).html();
				var adrescek = $(".adrescek").eq(say).val();
				var firma = $(".firma").eq(say).html();
				var telcek = $(".telcek").eq(say).html();
				var mailcek = $(".mailcek").eq(say).html();
				var idcek = $(".idcek").eq(say).val();
				var pazarlamaci = $(".pazarlamaci").eq(say).html();

				
				console.log(adcek);
				console.log(adrescek);
				console.log(firma);
				console.log(telcek);
				console.log(mailcek);
				console.log(idcek);
				console.log(pazarlamaci);

		
				$(".ad").val(adcek);
				$(".adres").val(adrescek);
				$("#firmaci").val(firma);
			    $(".tel").val(telcek);
			    $(".mail").val(mailcek);
				$(".uyeid1").val(idcek);
			    
				$("#pazarlar option").each(function(index){
					var valtr = $("#pazarlar option").eq(index).val();
					
					
					
					if(valtr == pazarlamaci){
						$("#pazarlar option").eq(index).attr("selected",true);
						
					}
					
				});
				
				
				
			});
			
		});
		
		
		</script>