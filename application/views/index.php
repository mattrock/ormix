<?Php $uyebilgi = $this->session->userdata("uyebilgi");  



?>

<?php if($uyebilgi->uye_turu == 4 ){ ?>
<section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Gösterge Paneli</h1>
                            <small class="text-muted">Hoşgeldiniz <?php echo $uyebilgi->uye_ad . " " . $uyebilgi->uye_soyad;  ?> </small>
                        </div>
                    </div>
                </div>
				
				
                <div class="row clearfix">
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $bekleyensiparislercount; ?> ₺</h3>
                                <p>Tüm bekleyen siparişler</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumgonderilenmalzemelercount; ?> ₺</h3>
                                <p>Tüm gönderilen malzemeler</p>
                            </div>
                        </div>
                    </div> </a>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumyapilantahsilatlarcount; ?> ₺</h3>
                                <p>Tüm yapılan tahsilatlar</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0">26.000 TL</h3>
                                <p>Tüm giderler</p>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="row clearfix">
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $depostokcount; ?> ₺</h3>
                                <p>Depo Stok</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $uretimstokcount; ?> ₺</h3>
                                <p>Üretim Stok</p>
                            </div>
                        </div>
                    </div> </a>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $toplammusteribakiyecount; ?> ₺</h3>
                                <p>Toplam Müşteri Bakiyesi</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $toplamfirmabakiyesicount; ?> ₺</h3>
                                <p>Toplam Firma Bakiyesi</p>
                            </div>
                        </div>
                    </div>
                </div>
				
				
				<div class="row">
					<div class="col-md-6">
						<section class="boxs ">
							<div class="boxs-header">
							<h2>Günün Kasası Gider</h2>
							</div>
							<div class="boxs-body">
							
                            <?php if($kasalistesi != null) { ?>
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>id</th>
											<th>Adı Soyadı</th>
                                            <th>Kasa Adı</th>
											<th>Miktar</th>
											
                                            
										</tr>
									</thead>
									<tbody>
										
                                         
										 
										 <?php foreach($kasalistesi as $yaz){ ?>
										 
										 
										 <?php $musteri = $this->selectt->teklimustericek($yaz->uye_id);  ?>
										 
										 
										   <tr>
										   
										   <td><?php echo $yaz->id; ?></td>
										   <td><?php echo $musteri->uye_ad ." " .$musteri->uye_soyad; ?></td>
										   <td><?php echo $yaz->kasaadi; ?></td>
										   <td><?php echo $yaz->odenen_miktar; ?> ₺</td>
										   </tr>
										   
										  
										   
										 <?php } ?>
										 
										 
										 
										 
										
										
										 
									</tbody>
									
									<tfoot class="hide-if-no-paging">
										<tr>
										
											<td colspan="5" class="text-right">
											
												<ul class="pagination">
												</ul>
											</td>
										</tr>
									</tfoot>
									
								</table>
							<?php } else { ?>
							<h5 style="text-align:center;">Günün Kasası Boş :(</h5>
							<?php } ?>
										<div class="row">
							


	<div class="col-md-4">&nbsp;</div>
				
                   

</div>	
		
							</div>
							

									</section>
					</div>
					
					<div class="col-md-6">
						<section class="boxs ">
							<div class="boxs-header">
							<h2>Günün Kasası Gelir</h2>
							</div>
							<div class="boxs-body">
							
                            <?php if($kasalistesi != null) { ?>
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>id</th>
											<th>Adı Soyadı</th>
                                            <th>Müşteri Adı Soyadı</th>
											<th>Miktar</th>
											
                                            
										</tr>
									</thead>
									<tbody>
										
                                         
										 
										 <?php foreach($kasalistesigelir as $yaz){ ?>
										 
										 
										 <?php $musteri = $this->selectt->teklimustericek($yaz->uye_id);  ?>
										 <?php $mus = $this->selectt->teklicekic($yaz->musteri_id);  ?>
										 
										   <tr>
										   
										   <td><?php echo $yaz->id; ?></td>
										   <td><?php echo $musteri->uye_ad ." " .$musteri->uye_soyad; ?></td>
										   <td><?php echo $mus->musteri_adi." ".$mus->musteri_soyadi; ?></td>
										   <td><?php echo $yaz->odenen_miktar; ?> ₺</td>
										   </tr>
										   
										  
										   
										 <?php } ?>
										 
										 
										 
										 
										
										
										 
									</tbody>
									
									<tfoot class="hide-if-no-paging">
										<tr>
										
											<td colspan="5" class="text-right">
											
												<ul class="pagination">
												</ul>
											</td>
										</tr>
									</tfoot>
									
								</table>
							<?php } else { ?>
							<h5 style="text-align:center;">Günün Kasası Boş :(</h5>
							<?php } ?>
										<div class="row">
							


	<div class="col-md-4">&nbsp;</div>
				
                   

</div>	
		
							</div>
							

									</section>
					</div>
					
				</div>
                

                <div class="row clearfix">
				
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <section class="boxs">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-blue">
                                    <strong>Son Kayıt Olan </strong>Müşteriler</h3>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-toggle">
                                                    <span class="minimize">
                                                        <i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Gizle</span>
                                                    <span class="expand">
                                                        <i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-refresh">
                                                    <i class="fa fa-refresh"></i> Yenile </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-fullscreen">
                                                    <i class="fa fa-expand"></i> Ekranı Kapla </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" tabindex="0" class="boxs-close">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="boxs-body">
                                <div class="body table-responsive members_profiles">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:60px;">Üye</th>
                                                <th>Ad Soyad</th>
                                                <th>Telefon</th>
                                                <th>İl / İlçe</th>
                                                <th>Firma Adı</th>                                    
                                                
                                                <th class="hidden-md hidden-sm">İlerleme</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										
										<?php foreach($sonuyecek as $yaz){ ?>
                                            <tr>
                                                <td>
                                                    <img class="rounded-circle" src="<?php echo base_url(); ?>assets/assets/images/xs/avatar1.jpg" alt="user" width="40"> </td>
                                                <td>
                                                    <a href="javascript:;"><?php echo $yaz->musteri_adi . " ". $yaz->musteri_soyadi; ?></a>
                                                </td>
                                                <td><?php echo $yaz->musteri_telefon; ?></td>
                                                <td><?php echo $yaz->musteri_il; echo " / ".$yaz->musteri_ilce; ?></td>
                                                <td><?php echo $yaz->firma_adi; ?></td>                                   
                                               
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
										<?php } ?>
                                            
                                           
                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                </div>
                
          
              
                <div class="row clearfix">
                    <h2 style="margin-left:20px; margin-bottom:30px;">En çok satan ürünler</h2>
					
					<?php foreach($encoksatan as $yaz){ 
					$pink =  $this->selectt->uruncekecen($yaz->urun_id);
					
					?>
					
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="boxs project_widget">
                             <div class="pw_img">
                                <img class="img-responsive" src="<?php echo base_url("uploads/"); echo $pink->resim; ?>  " alt="About the image">
                            </div>
                            <div class="pw_content">
                                <div class="pw_header">
                                    <h6><?php echo $pink->urun_adi; ?></h6> 
                                    <small class="text-muted"><?php echo $pink->urun_serisi; ?></small>
                                </div>
                                <div class="pw_meta">
                                    <span><?php echo $pink->satis_fiyati; ?> ₺</span>
                                    <small class="text-muted"> </small>
                                    <small class="text-danger"><?php echo $yaz->adet; ?> <?php echo $yaz->birim; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                   
                    
				
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-12">
                        <section class="boxs progress-report">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-green">
                                    <strong>Yapılan </strong>İşlem Listesi</h3>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-toggle">
                                                    <span class="minimize">
                                                        <i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Gizle</span>
                                                    <span class="expand">
                                                        <i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Genişlet</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-refresh">
                                                    <i class="fa fa-refresh"></i> Yenile </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-fullscreen">
                                                    <i class="fa fa-expand"></i> Ekranı Kapla </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" tabindex="0" class="boxs-close">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="boxs-body table-custom">
                                <div class="table-responsive">
                                    <table class="table table-custom" id="project-progress">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>İşlem Yapan Kişi</th>
                                                <th>Tarih</th>
                                                <th>Yaptığı İşlem</th>
                                                <th>İp Adresi</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php foreach($logcek as $yaz ){ ?>
                                            <tr>
                                                <td><?php echo $yaz->id; ?></td>
                                                <td><?php echo $yaz->ad_soyad; ?></td>
                                                <td><?php echo $yaz->tarih; ?></td>
                                                <td>
                                                    <small class="text-danger"><?php echo $yaz->yapilan_islem; ?></small>
                                                </td>
                                                <td>
												<?php echo $yaz->ip; ?>
												</td>
                                            </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
<?php } ?>

<?php if($uyebilgi->uye_turu == 1 ){ ?>
<section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Gösterge Paneli</h1>
                            <small class="text-muted">Hoşgeldiniz <?php echo $uyebilgi->uye_ad . " " . $uyebilgi->uye_soyad;  ?> </small>
                        </div>
                    </div>
                </div>
				
				
                <div class="row clearfix">
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $bekleyensiparislercount; ?> ₺</h3>
                                <p>Tüm bekleyen siparişler</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumgonderilenmalzemelercount; ?> ₺</h3>
                                <p>Tüm gönderilen malzemeler</p>
                            </div>
                        </div>
                    </div> </a>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumyapilantahsilatlarcount; ?> ₺</h3>
                                <p>Tüm yapılan tahsilatlar</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0">26.000 TL</h3>
                                <p>Tüm giderler</p>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="row clearfix">
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0">Depo Stok </h3>
                                <p>Depo detay için tıklayınız</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0">Üretim Stok</h3>
                                <p>Üretim detay için tıklayınız</p>
                            </div>
                        </div>
                    </div> </a>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0">1.000.000 TL</h3>
                                <p>Toplam Müşteri Bakiyesi</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0">9.000.000 TL</h3>
                                <p>Toplam Firma Bakiyesi</p>
                            </div>
                        </div>
                    </div>
                </div>
                

                <div class="row clearfix">
				
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <section class="boxs">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-blue">
                                    <strong>Son Kayıt Olan </strong>Müşteriler</h3>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-toggle">
                                                    <span class="minimize">
                                                        <i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Gizle</span>
                                                    <span class="expand">
                                                        <i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-refresh">
                                                    <i class="fa fa-refresh"></i> Yenile </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-fullscreen">
                                                    <i class="fa fa-expand"></i> Ekranı Kapla </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" tabindex="0" class="boxs-close">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="boxs-body">
                                <div class="body table-responsive members_profiles">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:60px;">Üye</th>
                                                <th>Ad Soyad</th>
                                                <th>Telefon</th>
                                                <th>İl / İlçe</th>
                                                <th>Firma Adı</th>                                    
                                                
                                                <th class="hidden-md hidden-sm">İlerleme</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										
										<?php foreach($sonuyecek as $yaz){ ?>
                                            <tr>
                                                <td>
                                                    <img class="rounded-circle" src="<?php echo base_url(); ?>assets/assets/images/xs/avatar1.jpg" alt="user" width="40"> </td>
                                                <td>
                                                    <a href="javascript:;"><?php echo $yaz->musteri_adi . " ". $yaz->musteri_soyadi; ?></a>
                                                </td>
                                                <td><?php echo $yaz->musteri_telefon; ?></td>
                                                <td><?php echo $yaz->musteri_il; echo " / ".$yaz->musteri_ilce; ?></td>
                                                <td><?php echo $yaz->firma_adi; ?></td>                                   
                                               
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
										<?php } ?>
                                            
                                           
                                           
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                </div>
                
          
              
                <div class="row clearfix">
                    <h2 style="margin-left:20px; margin-bottom:30px;">En çok satan ürünler</h2>
					
					<?php foreach($encoksatan as $yaz){ 
					$pink =  $this->selectt->uruncekecen($yaz->urun_id);
					
					?>
					
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="boxs project_widget">
                             <div class="pw_img">
                                <img class="img-responsive" src="<?php echo base_url("uploads/"); echo $pink->resim; ?>  " alt="About the image">
                            </div>
                            <div class="pw_content">
                                <div class="pw_header">
                                    <h6><?php echo $pink->urun_adi; ?></h6> 
                                    <small class="text-muted"><?php echo $pink->urun_serisi; ?></small>
                                </div>
                                <div class="pw_meta">
                                    <span><?php echo $pink->satis_fiyati; ?> ₺</span>
                                    <small class="text-muted"> </small>
                                    <small class="text-danger"><?php echo $yaz->adet; ?> <?php echo $yaz->birim; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                   
                    
				
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-12">
                        <section class="boxs progress-report">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-green">
                                    <strong>Yapılan </strong>İşlem Listesi</h3>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-toggle">
                                                    <span class="minimize">
                                                        <i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Gizle</span>
                                                    <span class="expand">
                                                        <i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Genişlet</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-refresh">
                                                    <i class="fa fa-refresh"></i> Yenile </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-fullscreen">
                                                    <i class="fa fa-expand"></i> Ekranı Kapla </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" tabindex="0" class="boxs-close">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="boxs-body table-custom">
                                <div class="table-responsive">
                                    <table class="table table-custom" id="project-progress">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>İşlem Yapan Kişi</th>
                                                <th>Tarih</th>
                                                <th>Yaptığı İşlem</th>
                                                <th>İp Adresi</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php foreach($logcek as $yaz ){ ?>
                                            <tr>
                                                <td><?php echo $yaz->id; ?></td>
                                                <td><?php echo $yaz->ad_soyad; ?></td>
                                                <td><?php echo $yaz->tarih; ?></td>
                                                <td>
                                                    <small class="text-danger"><?php echo $yaz->yapilan_islem; ?></small>
                                                </td>
                                                <td>
												<?php echo $yaz->ip; ?>
												</td>
                                            </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
<?php } ?>




<?php if($uyebilgi->uye_turu == 0 ){ ?>
<section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <h1 class="h3 m-0">Gösterge Paneli</h1>
                            <small class="text-muted">Hoşgeldiniz <?php echo $uyebilgi->uye_ad . " " . $uyebilgi->uye_soyad; 
                            ?>
                            
                           
                            <?php
                            if($uyebilgi->uyeturu==0){ echo $uyebilgi->musteri_adi . " ".$uyebilgi->musteri_soyadi; $gorevlipazarlama = $this->selectt->uyeadicek($uyebilgi->gorevlipazarlama); echo "</small><span style='float:right;'> Pazarlamacım : ".$gorevlipazarlama->uye_ad . " ".$gorevlipazarlama->uye_soyad."</span>"; } ?> 
                        </div>
                    </div>
                </div>
				
				
                <div class="row clearfix">
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $bekleyensiparislercount; ?> ₺</h3>
                                <p>Tüm bekleyen siparişler</p>
                            </div>
                        </div>
                    </div>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumgonderilenmalzemelercount; ?> ₺</h3>
                                <p>Tüm Satın Aldığım</p>
                            </div>
                        </div>
                    </div> </a>
                    <a href="#"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-seagreen">
                            <div class="boxs-body">
                                <h3 class="mt-0"><?php echo $tumyapilantahsilatlarcount; ?> ₺</h3>
                                <p>Cari</p>
                            </div>
                        </div>
                    </div>
                    <a href="<?php echo base_url("siparisler/").$uyebilgi->musteri_id; ?>"><div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-amber">
                            <div class="boxs-body">
                                <h3 class="mt-0">Sipariş Ver</h3>
                                <p>Hızlı Sipariş İçin Tıklayınız.</p>
                            </div>
                        </div>
                    </div>
                </div>
				
                
                    
                

                
                
          
              
                <div class="row clearfix">
                    <h2 style="margin-left:20px; margin-bottom:30px;">En çok satan ürünler</h2>
					
					<?php foreach($encoksatan as $yaz){ 
					$pink =  $this->selectt->uruncekecen($yaz->urun_id);
					
					?>
					
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="boxs project_widget">
                             <div class="pw_img">
                                <img class="img-responsive" src="<?php echo base_url("uploads/"); echo $pink->resim; ?>  " alt="About the image">
                            </div>
                            <div class="pw_content">
                                <div class="pw_header">
                                    <h6><?php echo $pink->urun_adi; ?></h6> 
                                    <small class="text-muted"><?php echo $pink->urun_serisi; ?></small>
                                </div>
                                <div class="pw_meta">
                                    <span><?php echo $pink->satis_fiyati; ?> ₺</span>
                                    <small class="text-muted"> </small>
                                    <small class="text-danger"><?php echo $yaz->adet; ?> <?php echo $yaz->birim; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                   
                    
				
                </div>
                
                
            </div>
        </section>
<?php } ?>