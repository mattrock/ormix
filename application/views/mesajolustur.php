
	<section id="content">
		<div class="page email-page tbox tbox-sm">
			<div class="row">
				
				
				
				<!-- right side -->
				<div class="col-md-12">
				
				<?php echo $this->session->flashdata('alert'); ?>
				
					<section class="boxs">
						<div class="tcol"> 
						
						
						<!-- right side body -->
						<div class="p-15">
							<form name="newMail" style="display:grid" action="" method="post" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-lg-2 control-label">Kime :</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" name="kime" id="kime">
										<input type="hidden" class="form-control" name="id" id="id">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">Konu :</label>
									<div class="col-lg-10">
										<input type="text" class="form-control konu" name="konu">
									</div>
								</div>
								<div class="form-group">
									<label class="col-lg-2 control-label">İçerik :</label>
									<div class="col-lg-10">
										<textarea class="form-control icerik" name="icerik"></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-lg-2 control-label">Ek :</label>
									<div class="col-lg-2">
										
															
															<span class="btn btn-raised btn-success fileinput-button">
												<i class="glyphicon glyphicon-plus"></i>
												<span>Dosya Seç</span>
												<input type="file" name="file[]" class="file" multiple>
											<div class="ripple-container"></div></span>
															
									</div>
									<ul class="resim">
									</ul>
								</div>
								<div class="form-group">
									<div class="col-"> <button class="btn btn-raised btn-success" type="submit"><i class="fa fa-send"></i> Gönder</button> </div>
								</div>
							</form>
						</div>
					</div>
					</section>
				</div>
				<!-- right side -->
			</div>
			
		</div>
	</section>
	
	<script>
	var mesajatan = [];
  $( function() {
	
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#kime" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        source: function( request, response ) {
          $.getJSON( "<?php echo base_url("yonetimpaneli/autocomplete"); ?>", {
            term: extractLast( request.term )
          }, response );
        },
        search: function() {
          // custom minLength
          var term = extractLast( this.value );
          if ( term.length < 2 ) {
            return false;
          }
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
		  
		  var key = ui.item.uye_id;
		  
		  
		  
		  
		  
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          mesajatan.push( ui.item.uye_id );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
		  
		  $("#id").val(mesajatan);
		  
          return false;
        }
      });
	  
	  
  } );
  
  
  
  </script>
	
	
	
	<script>
	
	
	
  $( function() {
	  
	  $("form").submit(function(){
		  var kime = $("#kime").val();
		  var konu = $(".konu").val();
		  var icerik = $(".icerik").val();
		  
		  if(kime == ""){
			  $("#kime").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #ff0000)");
		  }
		  else{
			  $("#kime").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #4caf4f)");
		  }
		  
		  if(konu == ""){
			  $(".konu").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #ff0000)");
		  }
		  else{
			  $(".konu").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #4caf4f)");
		  }
		  
		  if(icerik == ""){
			  $(".icerik").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #ff0000)");
		  }
		  else{
			  $(".icerik").css("background-image","linear-gradient(#49cdd0, #49cdd0),linear-gradient(#D2D2D2, #4caf4f)");
		  }
		  
		  if(konu != "" && icerik != "" && kime != ""){
			  return true;
		  }
		  
		  
		  return false;
	  });
    

	 var preview = $(".resim");

     $(".file").change(function(){
     
     var file = $('input[type=file]').get(0).files.length;
   
	 
	 
	 
	 preview.html(file+" Dosya Seçildi.");
	 
	 
	
    });
  
	
  } );
  
  
  
  </script>
  
  <style>
  
  ul.resim {
    padding: 20px;
}
  
  .resim img{
	  max-width:50px;
	  min-width:50px;
	  border:1px solid #e1e1e1;
  }
  
  .resim a {
	  margin-left:10px;
  }
  
  </style>