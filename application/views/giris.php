<!doctype html>
<html class="no-js" lang="tr">
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  <title>Ormix Yönetim Paneli</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/assets/js/vendor/bootstrap/bootstrap.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url(); ?>/assets/assets/css/main.css" rel="stylesheet">
</head>

<body id="falcon" class="authentication">
  <div class="wrapper">
    <div class="header header-filter" style="background-image: url('<?php echo base_url(); ?>/assets/assets/images/login-bg.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
            <div class="card card-signup">
              <form class="form" action="<?php echo base_url('yonetim/logincheck'); ?>" method="POST">
                <div class="header header-primary text-center">
                  <h4>Ormix Su Armatürleri</h4>
				  
                  <div class="social-line">
                    
                  </div>
                </div>
                <h3 class="mt-0">Sisteme Giriş</h3>
                
                <div class="content">
                  <div class="form-group">
                    <input name="kuladi" type="text" class="form-control underline-input" placeholder="Kullanıcı Adı">
                  </div>
                  <div class="form-group">
                    <input name="sifre" type="password" placeholder="Şifre" class="form-control underline-input">
                  </div>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="optionsCheckboxes"> Beni Hatırla</label>
                  </div>
                </div>
                <div class="footer text-center">
                  <button type="submit"  class="btn btn-info btn-raised">Giriş</button>
                </div>
                
				
              </form>
			  
            </div>
			<?php echo $this->session->flashdata('alert'); ?>
			<?php echo validation_errors(); ?>
          </div>
		  
        </div>
      </div>
      <footer class="footer mt-20">
        <div class="container">
          <div class="col-lg-12 text-center">
            
            <div class="copyright text-white mt-20">
              
              <p> © 2018, <a href="<?php echo base_url(); ?>" target="_blank">Kodifix Bilişim Hizmetleri</a>  tarafından hazırlanmıştır. <br/> <small>Coders Blackman & <a href="http://www.feaxer.com">Feaxer</a></small> </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>/assets/assets/bundles/libscripts.bundle.js"></script>
  <script src="<?php echo base_url(); ?>/assets/assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>