<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">Devir İşlemleri</h1>
						</div>
					</div>

				</div>

				<!-- row -->
				<div class="row">
                    <div class="col-md-12">

						<section class="boxs ">
							<div class="boxs-header">
                                <h2>Devir İşlemleri Yönetimi</h2>
							</div>
							<div class="boxs-body">
								
    	<a href="<?php echo base_url('firmadevri'); ?>">  <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                    <div class="boxs top_report_chart l-parpl">
                          <div class="boxs-body">
                        
                                <p>Firma Devri</p>
                            </div>
                        </div>
                    </div></a>
					<a href="<?php echo base_url('musteridevri'); ?>">	<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                              
                                <p>Müşteri Devri</p>
                            </div>
                        </div>
                    </div></a>
				<a href="<?php echo base_url('urunstok'); ?>">	<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                         
                                <p>Depo Stok</p>
                            </div>
                        </div>
                    </div></a>
			<a href="<?php echo base_url('malzemestok'); ?>">		<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                               
                                <p>Malzeme Stok</p>
                            </div>
                        </div>
                    </div></a>
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<?php
                                                    if($this->uri->segment(2) == 'acekliste'){ 
													
									?>
										<tr>
                                            <th>Durum</th>
                                            <th>Firma Adı</th>
											<th>Çek No</th>
											<th>Çek Banka Adı</th>
											<th>Keşide Tarihi</th>
                                           <th>Tutar</th>
                                           <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
										<tr>
											<td><?php  ?></td>
											<td><?php echo $yaz->uretim_kodu; ?></td>
											<td><?php $fi = $this->selectt->urunserisicek($yaz->uretim_seriadi); echo $fi->urun_seriadi; ?></td>
											<td><?php echo $this->selectt->modulparcasayisicount($yaz->urun_id);   ?></td>
											
                                           <td><?php echo $yaz->urun_aciklamasi; ?></td>
                                           <td> <?php echo $yaz->urun_adet; ?></td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                                 <button class="onayla btn btn-raised btn-info btn-sm mr-10" title="Onayla" id="<?php echo $yaz->urun_id; ?>" style="background-color: green" max="<?php echo $yaz->uretim_id; ?>" ><i class="fa fa-check"></i></button>          
                                
                                            </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
													<?php
                                                    if($this->uri->segment(2) == 'vcekliste'){ 
													
									?>
										<tr>
                                            <th>Firma Adı</th>
											<th>Çek No</th>
											<th>Çek Banka Adı</th>
											<th>Keşide Tarihi</th>
                                           <th>Tutar</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
										<tr>
											<td><?php echo $yaz->uretim_kodu; ?></td>
											<td><?php $fi = $this->selectt->urunserisicek($yaz->uretim_seriadi); echo $fi->urun_seriadi; ?></td>
											<td><?php echo $this->selectt->modulparcasayisicount($yaz->urun_id);   ?></td>
											
                                           <td><?php echo $yaz->urun_aciklamasi; ?></td>
                                           <td> <?php echo $yaz->urun_adet; ?></td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                                 <button class="onayla btn btn-raised btn-info btn-sm mr-10" title="Onayla" id="<?php echo $yaz->urun_id; ?>" style="background-color: green" max="<?php echo $yaz->uretim_id; ?>" ><i class="fa fa-check"></i></button>          
                                   <button class="btn btn-raised btn-primary btn-sm" title="Sil"   onclick="sil(<?php echo $yaz->uretim_id; ?>);"><i class="fa fa-trash"></i></button>
                                            </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
								</table>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
		
		<script>
		
		$(".onayla").click(function(){
			
			var urunid = $(this).attr("max");
			var uretimid = $(this).attr("id");
			
			console.log(urunid);
			console.log(uretimid);
			
			var a = confirm("Onaylamak istediğinize emin misiniz ?");
			
			
			if(a){
				$.ajax({
				url:"<?php echo base_url("yonetimpaneli/isemirionayla/"); ?>"+urunid,
				type:"POST",
				data:{uretimid:uretimid},
				success:function(r){
				   if(r == 1){
					   alert("onaylandı.");
					   location.replace("");
				   }
				}
			});
			}
			
		});
		
		
		function sil(id){
			
			var a = confirm("Silmek istediğinize emin misiniz ?");
			
			
			if(a){
			$.ajax({
				url:"<?php echo base_url("yonetimpaneli/isemirisil/"); ?>"+id,
				type:"POST",
				success:function(r){
				   if(r == 1){
					   alert("başarıyla silindi.");
					   location.replace("");
				   }
				}
			});	
			}
			
			
			
		}
		
		</script>