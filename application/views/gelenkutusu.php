<?php 

$bul = $_SERVER["REQUEST_URI"];

$segment = $this->uri->segment(4);

?>
<section id="content">
		<div class="page email-page tbox tbox-sm">
			<div class="row">
				<div class="col-md-3">
					<section class="boxs">
						<div class="tcol"> 
							
							<div class="p-15 collapse collapse-xs collapse-sm" id="mail-nav">
								<ul class="nav nav-pills nav-stacked nav-sm" id="mail-folders">
									<li class="sira active"><a href="<?php echo base_url("gelenkutusu"); ?>" title="Inbox">Gelen Kutusu </a></li>
									<li class="sira"><a href="<?php echo base_url("gonderilenler"); ?>" title="Sent">Gönderilenler </a></li>
									
								</ul>
					
							</div>
						</div>
					</section>
				</div>
				
				
				<div class="col-md-9 tbest">
					<section class="boxs mail_listing">
						<div class="inbox-center table-responsive">
						
						<?php if(!empty($veriler)){ ?>
						
							<table class="table table-hover">
								<thead>
									<tr>
										
										<th>
										
											<div class="btn-group">
												<button type="button" onclick="yenile();" class="btn btn-raised btn-success btn-sm" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-refresh"></i> </button>
											</div>
										</th>
                                        
										
									</tr>
								</thead>
								<tbody>
								
								
								
								
								<?php foreach($veriler as $yaz){ ?>
									<tr class="unread">
										
											
										<td class="hidden-xs"><a href=""><i class="fa fa-star-o"></i></a></td>
										<td class="hidden-xs"><?php $al =  $this->selectt->alanidcektekli($yaz->gonderen_id); echo ucfirst($al->uye_ad) ." ". ucfirst($al->uye_soyad); ?></td>
										<td class="max-texts"><a href="javascript:;"> İlk Mesaj </a></td>
										<td class="hidden-xs"> <?php echo $yaz->ek_id != null ? "<i class='fa fa-paperclip'></i>":""; ?>  </td>
										<td class="text-right"> <a href="<?php echo base_url("mesajgoster/".$yaz->id); ?>" class="btn btn-raised btn-default btn-xs">görüntüle</a> </td>
										<td class="text-right"> <button class="sil btn btn-raised btn-primary btn-xs" onclick="sil(<?php echo $yaz->id; ?>);">sil</button> </td>
									</tr>
								<?php } } else { ?>
								
								
								<h5 style="margin:0px; padding:20px;">Hiç Mesajınız Yok :(</h5>
								
								<?php } ?>
									
								</tbody>
							</table>
						</div>
						<div class="row">
							
							<div class="col-xs-12" style="text-align:center;">
								
									
								
							</div>
						</div>
						
						
						
					</section>
				</div>
				
				
				
			</div>
		</div>
	</section>
	
	
	
	
	<script>
	
	
	
	function sil(id){
			var r = confirm("Silmek istediğinize emin misiniz ?");
			
		if(r){
			
			$.ajax({
				url:"<?php echo base_url("/yonetimpaneli/gelensil/"); ?>"+id,
				type:"POST",
				success:function(r){
					if(r == 1){
						alert("silindi !");
						location.reload();
					}
				}
			});
			
		}
			
			
		}
		
		function yenile(){
		location.reload();
	}
	
	
	
	
	</script>
	
	
	<style>
						
						.ul.paginationer {
                         list-style: none;
                         }
						
						ul.paginationer a {
                        
    margin-right: 2px;
    background: #f7f7f7;
    border: 1px solid #d1d1d1;
    border-radius: 2px;
    color: #000;
    display: inline-block;
    width: 25px;
    text-align: center;
                        }
						
						a.active {
    background: #337ab7 !important;
    color: #fff !important;
}
						
						
						</style>