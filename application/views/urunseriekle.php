<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<?php echo $this->session->flashdata("alert"); ?>
					
					
					<div class="row">
					<div class="col-md-12">
					
					<button class="btn btn-raised btn-primary" data-toggle="modal" data-target="#myModal">Ürün Seri Ekle</button>
					
						<section class="boxs ">
							<div class="boxs-header">
							</div>
							<div class="boxs-body">
								<div class="form-group">
									<label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="button" class="btn btn-raised btn-success btn-sm" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-search"></i> </button>
                                            </div>
								</div> <br><br>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<thead>
										<tr>
											<th>Ürün Seri İd</th>
											<th>Ürün Seri Adı</th>
                                            <th>İşlemler</th>
										</tr>
									</thead>
									<tbody>
										
                                         
										 
										   <?php foreach($sericek as $yazdir) {  ?>
										 
										 
										 <tr>
										    
											<td class="a6"><?php echo $yazdir->id; ?></td>
                                            <td class="a3"><?php echo $yazdir->urun_seriadi; ?></td>
                                   <td>
                                            <div class="col-lg-15">
                                  
                                    <button class="btn btn-raised btn-info btn-sm mr-10 duzenle" onclick="duzenle(<?php echo @$yazdir->id; ?>);" title="Düzenle" style="background-color: yellow" data-toggle="modal" data-target="#myModal1"><i class="fa fa-file"></i></button>
                                    <button class="btn btn-raised btn-primary btn-sm" title="Sil" onclick="sil(<?php echo @$yazdir->id; ?>);"><i class="fa fa-trash"></i></button>
                                            </div>
                                            </td>
                                            </tr>
										 
										   <?php } ?> 
										 
										 
										 
									</tbody>
									
									<tfoot class="hide-if-no-paging">
										<tr>
										
											<td colspan="5" class="text-right">
											
												<ul class="pagination">
												
												</ul>
											</td>
										</tr>
									</tfoot>
									
								</table>
										<div class="row">
							


	<div class="col-md-4">&nbsp;</div>
				
                <div class="col s4 m4 cpm">
				  
				   <?php echo $linkler; ?>
   
				 
  
  
             </div>     

</div>	
		
							</div>
							

									</section>
					</div>
				</div>
				
		
					
					
				</div>
			</div>
		</section>
		
		
		<div class="modal fade in" id="myModal" role="dialog" style="display: none;" aria-hidden="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Ürün Seri Ekle</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
					
					<form action="" method="POST">
					
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Ürün Seri Ekle</strong> Formu</h3>
                                                                <p>Ürün Seri Ekle işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Ürün Seri Adı</label>
                                                                <input type="text" name="seri" class="form-control" rows="5"  id="username" placeholder=" ">
                                                            </div>
                                                            <div class="form-group col-sm-12" style="margin-top: 30px">
                                                                <button class="btn btn-raised btn-primary">Ürün Seri Ekle</button>
                                                            </div>
                                                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                </div>
				</form>
            </div>
        </div>
    </div>
	
	
	<div class="modal fade in" id="myModal1" role="dialog" style="display: none;" aria-hidden="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Ürün Seri Düzenle</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
					
					<form action="<?php echo base_url("urunseriduzenle"); ?>" method="POST">
                                             <input type="hidden" name="urunseridduzenle" class="urunseridduzenle">               
                                                        <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Ürün Seri Adı</label>
                                                                <input type="text" name="seri" class="yah form-control" rows="5"  id="username" placeholder=" ">
                                                            </div>
                                                            <div class="form-group col-sm-12" style="margin-top: 30px">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
                                                        </div>
                                                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                </div>
				</form>
            </div>
        </div>
    </div>

<script>


	
	function sil(uye_id){
			
			 var r = confirm("Silmek istediğinize emin misiniz ?");
	
    if (r == true) {
        location.replace('/yonetimpaneli/urunserisil/'+uye_id);
    } 
	else {
       return false;
    }	
			
		}
		
	function duzenle(al){
		
		$.ajax({
			url:"<?php echo base_url("tekliurunsericek/"); ?>"+al,
			type:"POST",
			success:function(r){
				gelen = r.split(",");
				$(".urunseridduzenle").val(gelen[1]);
				$(".yah").val(gelen[0]);
			}
		});
		
	}
	


</script>
		
		