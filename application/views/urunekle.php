<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
			
						<?php echo $this->session->flashdata("alert"); ?>
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" id="fileupload" action="" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="form-group col-md-8 legend">
                                                                <h3>
                                                                    <strong>Ürün Ekleme</strong> Formu</h3>
                                                                <p>Ürün Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
														<div class="form-group col-sm-6">
                                                                <label for="username">Ürün Kodu</label>
                                                                <input type="text" name="kod" class="form-control" rows="5"  id="username" placeholder="" value="<?php echo rand(10000,99999); ?>-">
                                                            </div>
                                                            
                                                                
                              
                                                     <div class="form-group col-sm-6">
                                                                <label for="username">Ürün Adı</label>
                                                                <input type="text" name="ad" class="form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Alış Fiyat</label>
                                                                <input type="text" name="afiyat" class="afiyat form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Satış Fiyat</label>
                                                                <input type="text" name="sfiyat" class="sfiyat form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Ürün Serisi</label>
                                                              <input type="text" name="seri1" class="form-control" rows="5"  id="seri" placeholder=" " required="">
                                                            </div>
                                                          
                                                               
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Stok</label>
                                                                <input type="text" name="stok" class="form-control" rows="5"  id="username" required="" required="">
                                                            </div>
															
															
															
															  <div class="form-group col-sm-6">
                                                                <label for="username">KDV</label>
                                                          <input type="number" class="form-control mb-10" name="kdv" min="0" max="18" placeholder="0-18" style="background-color:#f0f3f5" required="">
                                                            </div>
														
															<div class="form-group col-sm-6" required="">
															<label for="username">Birim</label>
                                                            <select name="birim" class="form-control mb-10 parsley-success">
												            <option value="" selected disabled>Seçiniz</option>
												            <option value="adet">Adet</option>
												            <option value="kg">KG</option>
												            <!-- <option value="litre">Litre</option> -->
															 <!-- <option value="m2">M2</option> -->
												            <option value="m3">M3</option>
												           <!-- <option value="metre">METRE</option> -->
															 <option value="paket">PAKET</option>
											                </select>
															</div>
															
                                                            
                                                            <div class="form-group col-sm-12">
                                                                <label for="username">Ürün Yazısı</label>
                                                                <textarea name="urun" class="form-control" rows="5"  id="username" required=""></textarea>
                                                            </div>
                                                           
														   <div class="form-group col-sm-12">
															    <label for="phone">Ürün Fotoğrafları</label>
															    <span class="btn btn-raised btn-success fileinput-button">
												                <i class="glyphicon glyphicon-plus"></i>
												                <span>Resim Seç</span>
												                <input type="file" name="file" multiple="">
											                    <div class="ripple-container"></div></span>
															    </div>
                                                          

                                                            <div class="form-group col-sm-6">
                                                                <button class="btn btn-raised btn-primary">Ürün Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		
		<script>
		
		$(document).ready(function(){
			
			$(".afiyat").keydown(function(){
				
				
				var afiyat = $(".afiyat").val();
				
				var yeni = afiyat.replace(",",".");
				
				$(".afiyat").val(yeni);
				
				
			});
			
			$(".sfiyat").keydown(function(){
				
				
				var sfiyat = $(".sfiyat").val();
				
				var sfiyat1 = sfiyat.replace(",",".");
				
				$(".sfiyat").val(sfiyat1);
				
				
			});
			
		});
		
		</script>