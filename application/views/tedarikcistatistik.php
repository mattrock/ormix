<section id="content">
            <div class="page page-tables-footable">
                <!-- bradcome -->
                <div class="b-b mb-10">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Ürünlerim</h1>
                        </div>
                    </div>
                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <section class="boxs ">
                            <div class="boxs-header">
                            </div>
                            <div class="boxs-body">
                                <div class="form-group">
                                    <label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="button" class="btn btn-raised btn-success btn-sm" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-search"></i> </button>
                                            </div>
                                </div> <br><br>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom" align=”center”>
                                    <thead>
                                        <tr>
										<th>Tedarikçi Firma</th> 
                                           <th>Ürün Resmi</th>
										   <th>Ürün Adı </th>   
                                           <th>Ürün Serisi</th>
                                           <th>Birim</th>
                                           <th>Adet</th>
                                           <th>Fiyat</th>
                                           <th>Kdv</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
                                       
                                       <?php foreach($veriler as $yazyaz) { ?>   
									   <?php $urunadicek = $this->selectt->urunadicek1($yazyaz->urun_adi); ?>
									   <?php $tedarikcifirma = $this->selectt->tedarikcicek1($yazyaz->firma_kategori); ?>
									         
                                       <tr>
									   <td><?php echo ucfirst($tedarikcifirma->firma_adi); ?></td>
									                                               <td> <a href="<?php echo base_url('uploads/').$urunadicek->resim; ?>" target="_blank"><img src="<?php echo base_url('uploads/').$urunadicek->resim; ?>" width="50" height="50" ></a></td>

                                       <td><?php echo $urunadicek->urun_adi; ?></td>
											
                                            
                                            <td><?php echo $urunadicek->urun_serisi; ?></td>
                                         <td><?php echo ucfirst($yazyaz->birim); ?></td>
                                         <td><?php echo ucfirst($yazyaz->adet); ?></td>
                                         <td><?php echo ucfirst($yazyaz->fiyat); ?></td>
                                         <td><?php echo ucfirst($yazyaz->kdv); ?></td>
                                            </tr>
                                            
									   <?php } ?>

                                        </tbody>
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <ul class="pagination">
												<?php echo $linkler; ?>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
		
<script>

$(document).ready(function(){
	
	$(".sipsi").click(function(){
		
		var indis = $(".sipsi").index(this);
		
		text = $(".emre").eq(indis).val();
		eskideger = $(".yaz").eq(indis).html();
		id = $(".sipsi").eq(indis).attr("id");
		
		eskideger = parseInt(eskideger);
		yenideger = parseInt(text);
		topla = eskideger+yenideger;
		
		
		
		$.ajax({
		url:"<?php echo base_url("yonetimpaneli/urunstokekle/"); ?>",
		type:"POST",
		data:{id:id,text:topla},
		success:function(r){
			$(".yaz").eq(indis).html(topla);
		    $(".emre").eq(indis).val("");
		}
	    });
		
		
		
		
	});
	
});


</script>