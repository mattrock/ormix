<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">Çek Senet Yönetimi</h1>
						</div>
					</div>

				</div>

				<?php echo $this->session->userdata("basarili"); ?>
				<div class="row">
                    <div class="col-md-12">
						<section class="boxs ">
							<div class="boxs-header">
                                <h2>Çek Senet Yönetimi</h2>
							</div>
							<div class="boxs-body">
								
    	<a href="<?php echo base_url('senetcek/acekliste'); ?>">  <div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                    <div class="boxs top_report_chart l-parpl">
                          <div class="boxs-body">
                        
                                <p>Alınan Çek Listesi</p>
                            </div>
                        </div>
                    </div></a>
					<a href="<?php echo base_url('senetcek/asenetliste'); ?>">	<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                              
                                <p>Alınan Senet Listesi</p>
                            </div>
                        </div>
                    </div></a>
				<a href="<?php echo base_url('senetcek/vcekliste'); ?>">	<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-parpl">
                            <div class="boxs-body">
                         
                                <p>Verilen Çek Listesi</p>
                            </div>
                        </div>
                    </div></a>
			<a href="<?php echo base_url('senetcek/vsenetliste'); ?>">		<div class="col-lg-3 col-sm-6 col-md-6 col-xs-12">
                        <div class="boxs top_report_chart l-blue">
                            <div class="boxs-body">
                               
                                <p>Verilen Senet Listesi</p>
                            </div>
                        </div>
                    </div></a>
                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									<?php
                                         if($this->uri->segment(2) == 'acekliste'){ 
													
									?>
										<tr>
                                            <th>Durum</th>
                                            <th>Firma Adı</th>
											<th>Çek No</th>
											<th>Çek Banka Adı</th>
											<th>Keşide Tarihi</th>
                                           <th>Tutar</th>
                                           <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
											<tr>
															<td><?php $durum =$yaz->durum; if($durum==0){ echo '<span style="color:red;" "> Ödenmedi !</span> ';}else{echo '<span style="color:green;" "> Ödendi.</span>';}?></td>
											<td><?php echo $yaz->kesidici; ?></td>
											<td><?php echo $yaz->banka_hesabi;  ?></td>
											<td><?php  echo $yaz->banka_adi; ?></td>
                                           <td><?php  echo $yaz->odeme_tarih; ?></td>
                                           <td> <?php echo $yaz->odenen_miktar; ?> TL</td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                      <?php if($yaz->durum== 0){ ?>    <a href="<?php echo base_url("/yonetimpaneli/aceklistes/").$yaz->id; ?>">       <button class=" btn btn-raised btn-info btn-sm mr-10" title="Ödendi" style="background-color: green"  ><i class="fa fa-check"></i></button></a>   <?php } else {} ?> 
                                           </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
													<?php
                                                    if($this->uri->segment(2) == 'vcekliste'){ 
													
									?>
										<tr>
                                            <th>Durum</th>
                                            <th>Firma Adı</th>
											<th>Çek No</th>
											<th>Çek Banka Adı</th>
											<th>Keşide Tarihi</th>
                                           <th>Tutar</th>
                                           <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
										<tr>
															<td><?php $durum =$yaz->durum; if($durum==0){ echo '<span style="color:red;" "> Ödenmedi !</span> ';}else{echo '<span style="color:green;" "> Ödendi.</span>';}?></td>
											<td><?php echo $yaz->kesidici; ?></td>
											<td><?php echo $yaz->banka_hesabi;  ?></td>
											<td><?php  echo $yaz->banka_adi; ?></td>
                                           <td><?php  echo $yaz->odeme_tarih; ?></td>
                                           <td> <?php echo $yaz->odenen_miktar; ?> TL</td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                      <?php if($yaz->durum== 0){ ?>    <a href="<?php echo base_url("/yonetimpaneli/vceklistes/").$yaz->id; ?>">       <button class=" btn btn-raised btn-info btn-sm mr-10" title="Ödendi" style="background-color: green"  ><i class="fa fa-check"></i></button></a>   <?php } else {} ?> 
                                           </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
													
													<?php
                                         if($this->uri->segment(2) == 'vsenetliste'){ 
													
									?>
										<tr>
                                            <th>Durum</th>
                                            <th>Firma Adı</th>
											<th>Tutar</th>
											<th>Keşide Tarihi</th>
                                           <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
											<tr>
														<td><?php $durum =$yaz->durum; if($durum==0){ echo '<span style="color:red;" "> Ödenmedi !</span> ';}else{echo '<span style="color:green;" "> Ödendi.</span>';}?></td>
											<td><?php echo $yaz->kesidici; ?></td>
											<td><?php echo $yaz->odenen_miktar;  ?> TL</td>
                                           <td><?php  echo $yaz->odeme_tarih; ?></td>
                                          
                                            <td>
                                           <div class="col-lg-20">
                      <?php if($yaz->durum== 0){ ?>    <a href="<?php echo base_url("/yonetimpaneli/vsenetlistes/").$yaz->id; ?>">       <button class=" btn btn-raised btn-info btn-sm mr-10" title="Ödendi" style="background-color: green"  ><i class="fa fa-check"></i></button></a>   <?php } else {} ?> 
                                           </div>
                                            </tr>
									<?php } ?>
									
                            
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
													<?php
                                         if($this->uri->segment(2) == 'asenetliste'){ 
													
									?>
										<tr>
                                         <th>Durum</th>
                                            <th>Firma Adı</th>
											<th>Tutar</th>
											<th>Keşide Tarihi</th>
                                           <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
											<tr>
											<td><?php $durum =$yaz->durum; if($durum==0){ echo '<span style="color:red;" "> Ödenmedi !</span> ';}else{echo '<span style="color:green;" "> Ödendi.</span>';}?></td>
											<td><?php echo $yaz->kesidici; ?></td>
											<td><?php echo $yaz->odenen_miktar;  ?> TL</td>
                                           <td><?php  echo $yaz->odeme_tarih; ?></td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                      <?php if($yaz->durum== 0){ ?>    <a href="<?php echo base_url("/yonetimpaneli/asenetliste/").$yaz->id; ?>">       <button class=" btn btn-raised btn-info btn-sm mr-10" title="Ödendi" style="background-color: green"  ><i class="fa fa-check"></i></button></a>   <?php } else {} ?> 
                                           </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
													<?php } ?>
								</table>
							</div>
						</section>
				</div>
				</div>
			</div>
		</section>
