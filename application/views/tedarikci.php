<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
			
						<?php echo $this->session->flashdata("alert"); ?>
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" id="fileupload" action="" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="form-group col-md-8 legend">
                                                                <h3>
                                                                    <strong>Tedarikçi Ekleme</strong> Formu</h3>
                                                                <p>Ürün Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
														
														    
															
															<div class="form-group col-sm-10">
                                                                <label for="username">Ürün Adı</label>
																<input type="text" name="urunim" class="tedarikci form-control" rows="5">
																<input type="hidden" name="urunadi[]" class="form-control" rows="5"  id="pett">
                                                            </div>
															
															
															
															<div class="form-group col-sm-2">
															<button type="button" class="ekle btn btn-raised btn-primary">+</button>
															<button type="button" class="sil btn btn-raised btn-primary">-</button>
															</div>
															
															
															
															
                                                            <div class="form-group col-sm-6" required="">
															<label for="username">Birim</label>
                                                            <select name="birim[]" class="form-control parsley-success">
												            <option value="" selected disabled>Seçiniz</option>
												            <option value="adet">Adet</option>
												            <option value="kg">KG</option>
												            
															  
												            <option value="m3">M3</option>
												            
															 <option value="paket">PAKET</option>
											                </select>
															</div>
                                                           
														   <div class="form-group col-sm-6">
                                                                <label for="username">Adet</label>
                                                                <input type="text" name="adet[]" class="form-control" rows="5"  id="username" placeholder="">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Fiyat</label>
                                                                <input type="number" name="fiyat[]" class="form-control" rows="5"  id="username" placeholder="">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">KDV</label>
                                                                <input type="number" name="kdv[]" class="form-control" rows="5" min="0" max="18"  id="username" placeholder="">
                                                            </div>
															
															<div class="parcalar form-group">
															</div>
														  
                                                          

                                                            <div class="form-group col-sm-6">
                                                                <button class="btn btn-raised btn-primary">Ürün Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		
		<script>
		
		$(document).ready(function(){
			
	    $(".ekle").click(function(){
		
		var clone = '<div class="bobs col-sm-12" style="margin-top:20px; border:1px dashed; border-radius:3px;">  <div class="form-group col-sm-12"> <label for="username">Ürün Adı</label> <input type="text" name="urunim" class="tedarikci form-control" rows="5" > <input type="hidden" name="urunadi[]" class="form-control" rows="5" id="pett"> </div><div class="form-group col-sm-6" required=""><label for="username">Birim</label> <select name="birim[]" class="form-control parsley-success"> <option value="" selected disabled>Seçiniz</option> <option value="adet">Adet</option> <option value="kg">KG</option>   <option value="m3">M3</option>  <option value="paket">PAKET</option> </select></div><div class="form-group col-sm-6"> <label for="username">Adet</label> <input type="text" name="adet[]" class="form-control" rows="5" id="username" placeholder=""> </div><div class="form-group col-sm-6"> <label for="username">Fiyat</label> <input type="number" name="fiyat[]" class="form-control" rows="5" id="username" placeholder=""> </div><div class="form-group col-sm-6"> <label for="username">KDV</label> <input type="number" name="kdv[]" class="form-control" rows="5" min="0" max="18" id="username" placeholder=""> </div>  </div>';
		
		
		$(clone).clone().appendTo(".parcalar").last();
		
	     });
	
	
	
	
	    $(".sil").click(function(){
		$(".bobs").last().remove();
	   });
			
			
			$(".firmakategori").change(function(){
				
				var id = $(".firmakategori option:selected").val();
				
				
				$.ajax({
				url:"<?php echo base_url("yonetimpaneli/tederakciajax/"); ?>"+id,
				type:"POST",
				success:function(r){
					
					if(r == ""){
						$(".firmalar").html("<option>Bu kategoride firma yoktur.</option>");
						return false;
					}
					
					$(".firmalar").html(r);
				}
				});
				
				
			});
			
			
		});
		
		</script>