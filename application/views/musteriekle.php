<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">

						<?php echo $this->session->flashdata("alert"); ?>
					<div class="row">
                        <div style="width:1000px; height: 1000px; margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" name="arama" method="POST" action="">
                                                        <div class="row">
                                                            <div class="form-group col-md-12 legend">
                                                                <h3>
                                                                    <strong>Müşteri Yönetimi</strong> Formu</h3>
                                                                <p>Müşteri Ekleme işlemini buradan gerçekleştirebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
														
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Adı</label>
                                                                <input type="text" name="musteri_adi" class="form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
															
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Soyadı</label>
                                                                <input type="text" name="musteri_soyadi" class="form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Kullanıcı Adı</label>
                                                                <input type="text" name="musterikuladi" class="form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Şifre</label>
                                                                <input type="password" name="musterisifre" class="form-control" rows="5"  id="username" placeholder=" " required="">
                                                            </div>
															
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Müşteri Kodu</label>
                                                                <input type="text" name="musteri_kod" class="form-control" rows="5"  id="username" value="<?php echo rand(10000,99999) ?>-" required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="phone">Telefon : </label>
                                                                <input type="text" name="musteri_tel" id="phone" class="form-control" placeholder="(XXX) XXXX XXX" data-parsley-trigger="change" pattern="^[\d\+\-\.\(\)\/\s]*$" required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="phone">Fax : </label>
                                                                <input type="text" name="musteri_fax" id="phone" class="form-control" placeholder="Fax" required="">
                                                            </div>
                                                             <div class="form-group col-sm-6">
                                                                <label for="phone">Ülke : </label>
                                                                <input type="text" name="musteri_ulke" id="phone" class="form-control" placeholder="Türkiye" required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="phone">il : </label>
                                                                <input type="text" name="musteri_il" id="phone" class="form-control" placeholder="İstanbul" required="">
                                                            </div>
                                                         <div class="form-group col-sm-6">
                                                                <label for="phone">ilce : </label>
                                                                <input type="text" name="musteri_ilce" id="phone" class="form-control" placeholder="Şişli" required="">
                                                            </div>
                                                            
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">E-Mail</label>
                                                                <input type="text" name="musteri_mail" class="form-control" rows="5"  id="username" required="" >
                                                            </div>
                                                            
                                                           
                                                            
                                                            <div class="form-group col-sm-6" >
                                                                <label for="message">Müşteri Çek Limiti</label>
                                                                <input type="text" name="musteri_cek" class="form-control" rows="5" name="message" id="message" placeholder="20.000 TL" required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="message">Müşteri Senet Limiti</label>
                                                                <input type="text" name="musteri_senet" class="form-control" rows="5" name="message" id="message" placeholder="20.000 TL" required="">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="message">Müşteri Nakit Limiti</label>
                                                                <input type="text" name="musteri_nakit" class="form-control" rows="5" name="message" id="message" placeholder="20.000 TL" required="">
                                                            </div>
															 <div class="form-group col-sm-12">
                                                                <label for="username">Hesap Türü</label>
                                                                <select name="hesap" tabindex="3" class="form-control mb-10">
                                                                    <section>
                                                                        <option>Dolar</option>
                                                                        <option>Türk Lirası</option>
                                                                        <option>Euro</option>
                                                                    </section>
                                                                </select>
                                                            </div>
															<div class="form-group col-sm-12">
                                                                <label for="message">Adres Detay: </label>
                                                                <textarea class="form-control" rows="5" name="adres_detay" id="message"></textarea>
                                                            </div>
                                                            <div class="form-group col-md-12 legend">
                                                               <h3>Fatura Bilgileri</h3>
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="message">Müşteri Ünvanı : </label>
                                                                <input type="text" name="musteri_unven" class="form-control" rows="5" name="message" id="message" placeholder=""required="" >
                                                            </div>
															 <div class="form-group col-sm-6">
                                                                <label for="message">Müşteri Firması : </label>
                                                                <input type="text" name="firma_adi" class="form-control" rows="5" name="message" id="firma_adi" placeholder=""required="" >
                                                            </div>
                                                           
                                                            
                                                            
                                                            <div class="form-group col-sm-6">
                                                                <label for="message">Vergi Numarası : </label>
                                                                <input type="text" name="musteri_vergi" class="form-control" rows="5" name="message" id="message" placeholder="" required="">
                                                            </div>
													    <div class="form-group col-sm-6">
                                                                <label for="message">İl : </label>
                                                                <input type="text" name="musteri_ilbir" class="form-control" rows="5" name="message" id="message" placeholder="Ankara" required="">
                                                            </div>
															
														   <div class="form-group col-sm-6">
                                                                <label for="message">İlce : </label>
                                                                <input type="text" name="musteri_ilcebir" class="form-control" rows="5" name="message" id="message" placeholder="Elmadağ" required="">
                                                            </div>
															
                                                            <div class="form-group col-sm-12">
                                                                <label for="message">Vergi Dairesi : </label>
                                                                <input type="text" name="musteri_daire" class="form-control" rows="5" name="message" id="message" placeholder="" required="">
                                                            </div>
															 <div class="form-group col-sm-12">
                                                                <label for="message">Adres : </label>
                                                                <textarea rows="5" name="musteri_adres" id="message" class="form-control" placeholder="Adresinizi yazınız..."></textarea>
                                                            </div>

                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Müşterİ Ekle</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>