<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">Teklifler</h1>
						</div>
					</div>

				</div>

				<!-- row -->
				<div class="row">
                    <div class="col-md-12">

						<section class="boxs ">
							<div class="boxs-header">
                                <h2>Teklifler</h2>
							</div>
							<div class="boxs-body">
								

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									
										<tr>
											<th>Firma Adı</th>
                                            <th>Firma Tel</th>
											<th>Müşteri Adı</th>
											<th>Tedarikçi Firma</th>
                                            <th>Cari</th>
                                            <th>İşlemler</th>
										</tr>
																		

                                    <tbody>
										<tr>
											<td>Kodifix</td>
											<td>0 212 532 1218</td>
                                            <td>Bekir Sadık ERTÜRK</td>
                                            <td>Ormix</td>
                                            <td>2500 TL</td>
                                            <td>
                                           <div class="col-lg-10">
                                        <button class="btn btn-raised btn-info btn-sm mr-10" title="Siparişi Görüntüle" style="background-color: blue   "><i class="fa fa-file"></i></button>  
                                        <button class="btn btn-raised btn-info btn-sm mr-10" title="Onayla" style="background-color: green"><i class="fa fa-plus"></i></button>          
                                        <button class="btn btn-raised btn-primary btn-sm" title="Sil"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </td>
                                            </tr>
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												<ul class="pagination">
												</ul>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
	