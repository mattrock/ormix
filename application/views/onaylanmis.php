<section id="content">
			<div class="page page-tables-footable">
				<!-- bradcome -->
				<div class="b-b mb-10">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<h1 class="h3 m-0">İş Emirleri</h1>
						</div>
					</div>

				</div>

				<!-- row -->
				<div class="row">
                    <div class="col-md-12">

						<section class="boxs ">
							<div class="boxs-header">
                                <h2>Onaylanmış İş Emirleri</h2>
							</div>
							<div class="boxs-body">
								

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom">
									
										<tr>
											<th>Modül Kodu</th>
                                            <th>Modül Seri Adı</th>
											<th>Modül Tipi</th>
											<th>Modül Açıklaması</th>
                                            <th>Modül Parça sayısı</th>
                                           <th>İşlem</th>
										</tr>
																		

                                    <tbody>
									
									<?php foreach($veriler as $yaz){ ?>
									
										<tr>
											<td><?php echo $yaz->uretim_kodu; ?></td>
											<td><?php $fi = $this->selectt->urunserisicek($yaz->uretim_seriadi); echo $fi->urun_seriadi; ?></td>
											<td><?php echo $this->selectt->modulparcasayisicount($yaz->urun_id);   ?></td>
											
                                           <td><?php echo $yaz->urun_aciklamasi; ?></td>
                                           <td> <?php echo $yaz->urun_adet; ?></td>
                                           
                                            <td>
                                           <div class="col-lg-20">
                                 <button class="btn btn-raised btn-info btn-sm mr-10" title="Onayla" style="background-color: green" onclick="onayla(<?php echo $yaz->uretim_id; ?>);"><i class="fa fa-check"></i></button>          
                                   <button class="btn btn-raised btn-primary btn-sm" title="Sil" onclick="sil(<?php echo $yaz->uretim_id; ?>);"><i class="fa fa-trash"></i></button>
                                            </div>
                                            </tr>
									<?php } ?>
                                           
                                            
                                           
									</tbody>
									<tfoot class="hide-if-no-paging">
										<tr>
											<td colspan="5" class="text-right">
												
												<?php echo $linkler; ?>
												
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
		
		<script>
		
		function onayla(id){
			
			var a = confirm("Onayı Kaldırmak istediğinize emin misiniz ?");
			
			if(a){
				
				$.ajax({
				url:"<?php echo base_url("yonetimpaneli/isemirionaylama/"); ?>"+id,
				type:"POST",
				success:function(r){
				   if(r == 1){
					   location.replace("");
				   }
				}
			});
				
			}
			
				
			
			
			
			
		}
		
		function sil(id){
			
			var a = confirm("Silmek istediğinize emin misiniz ?");
			
			
			if(a){
			$.ajax({
				url:"<?php echo base_url("yonetimpaneli/isemirisil/"); ?>"+id,
				type:"POST",
				success:function(r){
				   if(r == 1){
					   alert("başarıyla silindi.");
					   location.replace("");
				   }
				}
			});	
			}
			
			
			
		}
		
		</script>