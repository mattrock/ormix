<section id="content">
            <div class="page dashboard-page">
                <!-- bradcome -->
                <div class="b-b mb-20">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0"> Çek Risk Raporu</h1>
                            <small class="text-muted">Risk durumlarını buradan inceleyebilirsiniz.</small>
                        </div>
                    </div>
                </div>
                
                 

                <div class="row clearfix">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <section class="boxs" style="width: 1000px">
                            <div class="boxs-header">
                                <h3 class="custom-font hb-blue">
                                    <strong>Çek Risk </strong>Raporu</h3>
                                <ul class="controls">
                                    <li class="dropdown">
                                        <a role="button" tabindex="0" class="dropdown-toggle settings" data-toggle="dropdown">
                                            <i class="fa fa-cog"></i>
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right with-arrow animated littleFadeInUp">
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-toggle">
                                                    <span class="minimize">
                                                        <i class="fa fa-angle-down"></i>&nbsp;&nbsp;&nbsp;Gizle</span>
                                                    <span class="expand">
                                                        <i class="fa fa-angle-up"></i>&nbsp;&nbsp;&nbsp;Expand</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-refresh">
                                                    <i class="fa fa-refresh"></i> Yenile </a>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0" class="boxs-fullscreen">
                                                    <i class="fa fa-expand"></i> Ekranı Kapla </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="remove">
                                        <a role="button" tabindex="0" class="boxs-close">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="boxs-body">
                                <div class="body table-responsive members_profiles">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Firma</th>
                                                <th>Yetkili Adı</th>
                                                <th>Telefon</th>
                                                <th>Çek Limiti</th>
                                                <th>Alınan Çek Tutarı</th>
                                                <th>Çek Sayısı</th> 
                                                <th class="hidden-md hidden-sm">Risk raporu</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td>Kodifix</td>
                                                <td>
                                                <a href="musteriler.html">Murat Kaya</a>
                                                </td>
                                                <td>0536 256 25 32</td>
                                                <td>15.000 TL</td>
                                                <td>4.200 TL</td>
                                                <td>23</td>                                   
                                               
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="87" aria-valuemin="0" aria-valuemax="100" style="width: 87%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kodifix</td>
                                                <td>
                                                    <a href="musteriler.html">Elif Kulakçı</a>
                                                </td>
                                                <td>0536 256 25 32</td>
                                                <td>16.000 TL</td>
                                                <td>2.500 TL</td>
                                                <td>16</td>                                   
                                               
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kodifix</td>
                                                <td>
                                                    <a href="musteriler.html">Serhat Taşdemir</a>
                                                </td>
                                                <td>0536 256 25 32</td>
                                                <td>18.000 TL</td>
                                                <td>2.010 TL</td>
                                                <td>11</td>                                   
                                               
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="32" aria-valuemin="0" aria-valuemax="100" style="width:32%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Kodifix</td>
                                                <td>
                                                    <a href="musteriler.html">Selma Baykuş</a>
                                                </td>
                                                <td>0536 256 25 32</td>
                                                <td>29.000 TL</td>
                                                <td>6.510 TL</td>
                                                <td>28</td>                                   
                                                
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                   Kodifix </td>
                                                <td>
                                                    <a href="musteriler.html">Bilal Bayrak</a>
                                                </td>
                                                <td>0536 256 25 32</td>
                                                <td>30.000 TL</td>
                                                <td>3.000 TL</td>
                                                <td>20</td>                                   
                                                
                                                 <td class="hidden-md hidden-sm">
                                                    <div class="progress">
                                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                    </div>
                    
                
          
              
               
                
               
                        </section>
                    </div>
                </div>
            </div>
        </section>