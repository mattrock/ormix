<section id="content">
            <div class="page page-tables-footable">
                <!-- bradcome -->
                <div class="b-b mb-10">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Sipariş Geçmişi</h1>
                        </div>
                    </div>

                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">

                         <section class="boxs">
                            <div class="boxs-header">
                                <h2>Sipariş Geçmişi</h2>
                            </div>
                            <div class="boxs-body">
                                <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom" style="text-align: center">
                                        <tr>
                                            <th>Siparis No</th>
                                            <th>Siparis Tarihi</th>
                                            <th>Ürün adı</th>
                                            <th>Birim</th>
                                            <th>Adet</th>
                                            <th>İskonto</th>
                                            <th>Fiyat</th>
											<th>Durum</th>
                                                         
                               
                                        </tr>
                                                                        

                                    <tbody>
                               
							   <?php foreach($veriler as $yaz){ ?>
                                            
									
											<tr>
                                            <td style="text-align:left;"><?php echo $yaz->siparis_id; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->tarih; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->urun_id; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->birim; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->adet; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->iskonto; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->fiyat; ?></td>
                                            <td style="text-align:left;"><?php echo $yaz->muhasebe_onay == 0 ? "onaylanmamış":""; echo $yaz->muhasebe_onay == 1 ? "onaylanmış":""; echo $yaz->muhasebe_onay == 2 ? "teslim edilmiştir.":"";  ?></td>
                                            </tr>
							   <?php } ?>
											 
                                    </tbody>
                                    <tfoot class="hide-if-no-paging">
                                        <tr>
                                            <td colspan="5" class="text-right">
                                                <ul class="pagination">
                                                </ul>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>	<div class="row">
         
				
                <div class="col s12 m12 cpm" style="background-color:#0099CC; height:30px; padding:5px;" >
				  
				   <?php echo $linkler; ?>
   
				 
  
  
             </div>     

</div>	
                            </div>
							
                        </section>
                    </div>
                </div>
            </div>
        </section>
		<script>
		function siparis(musteri_id){
			
             location.replace('yonetimpaneli/depo/'+musteri_id);
   
		}
		</script>