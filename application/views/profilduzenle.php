<section id="content">
			<div class="page profile-page">
				<!-- page content -->
				<div class="pagecontent">
					<!-- row -->
					<div class="row">
					<!--
						<div class="col-md-12">
							<section class="boxs">
								<div class="profile-header">
									<div class="profile_info">
										<div class="profile-image">
											<img src="assets\images\profile-photo.jpg" alt="">
										</div>
										<h4 class="mb-0"><strong>Ad Soyad</strong></h4>
										<span class="text-muted">Yonetici</span>
									</div>
								</div>
								<div class="profile-sub-header">
									<div class="box-list">
										<ul class="text-center">
											<li>
												<a href="mail-inbox.html" class="">
													<i class="fa fa-inbox"></i>
													<p>Mesajlarım</p>
												</a>
											</li>
											<li>
												<a href="uyeler.html" class="">
													<i class="fa fa-paperclip"></i>
													<p>Uyelerim</p>
												</a>
											</li>
											<li>
												<a href="firmalar.html">
													<i class="fa fa-paperclip"></i>
													<p>Firmalar</p>
												</a>
											</li>
											<li>
												<a href="urunlerim.html">
													<i class="fa fa-tasks "></i>
													<p>Ürünlerim</p>
												</a>
											</li>
                                            <li>
                                                <a href="musteriler.html">
                                                    <i class="fa fa-tasks "></i>
                                                    <p>Müşterilerim</p>
                                                </a>
                                            </li>
										</ul>
									</div>
								</div>
							</section>
						</div>
						--->
						<?php $uyebilgi = $this->session->userdata("uyebilgi"); ?>
                        <div style="width: 1000px;margin-left:50px;">
						<div role="tabpanel" class="tab-pane" id="setting" >
						
						
						
						<?php echo $this->session->flashdata('alert'); ?>
						
						
                                                <div class="wrap-reset">
                                                    <form class="profile-settings" action="" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
														
                                                            <div class="form-group col-md-12 legend">
                                                                <h4>
                                                                    <strong>Profil Düzenleme</strong> Formu</h4>
                                                                <p>Profil Bilgilerinizi buradan düzenleyebilirsiniz.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                                <label for="username">Üye Adı</label>
                                                                <input type="text" name="uyead" class="form-control"  value="<?php echo $uyebilgi->uye_ad; ?>" >
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="password">Üye Soyadı</label>
                                                                <input type="text" name="uyesoyad" class="form-control"  value="<?php echo $uyebilgi->uye_soyad; ?>" >
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-sm-6">
                                                                <label for="new-password">Üye Mail</label>
                                                                <input type="email" name="uyemail" class="form-control"  value="<?php echo $uyebilgi->uye_email; ?>">
                                                            </div>
                                                            <div class="form-group col-sm-6">
                                                                <label for="new-password-repeat">Üye Şifre</label>
                                                                <input type="password" name="uyesifre" class="form-control"  value="<?php echo $uyebilgi->uye_sifre; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="new-password">Üye Kullanıcı adı</label>
                                                                <input type="text" name="uyekuladi" class="form-control"  value="<?php echo $uyebilgi->uye_kuladi; ?>">
                                                            </div>
															
															<div class="form-group col-sm-6">
                                                                <label for="new-password-repeat">Üye Rütbe</label>
																<?php if($uyebilgi->uye_turu == 3 || $uyebilgi->uye_turu == 2 || $uyebilgi->uye_turu == 1){ ?>
																<input type="text" name="farmi" class="form-control" disabled  value="<?php echo $uyebilgi->uye_turu == 4 ? "Yönetici":""; ?> <?php echo $uyebilgi->uye_turu == 1 ? "Pazarlama":""; ?> <?php echo $uyebilgi->uye_turu == 2 ? "Depo":""; ?> <?php echo $uyebilgi->uye_turu == 3 ? "Muhasebe":""; ?>">
																<?php } else { ?>
                                                                <select class="form-control" name="uyerutbe">
																<option value="0" selected disabled>Seçiniz</option>
																<option value="4" <?php echo $uyebilgi->uye_turu == 4 ? "selected":""; ?> >Yönetici</option>
																<option value="3" <?php echo $uyebilgi->uye_turu == 3 ? "selected":""; ?>>Muhasebe</option>
																<option value="2" <?php echo $uyebilgi->uye_turu == 2 ? "selected":""; ?>>Depo</option>
																<option value="1" <?php echo $uyebilgi->uye_turu == 1 ? "selected":""; ?>>Pazarlama</option>
																</select>
																<?php } ?>
                                                            </div>
															
															<div class="form-group col-sm-6">
															<label for="phone">Üye Fotoğrafı</label>
															<span class="btn btn-raised btn-success fileinput-button">
												            <i class="glyphicon glyphicon-plus"></i>
												            <span>Resim Seç</span>
												            <input type="file" name="file" class="file">
											                <div class="ripple-container"></div></span>
															</div>
															
															<div class="form-group col-sm-6">
															<li class="ekle"><img src="<?php echo base_url("uploads/").$uyebilgi->uye_foto; ?>" width="125" height="125"></li>
															</div>
															
                                                            <div class="form-group col-sm-12">
                                                                <button class="btn btn-raised btn-primary">Kaydet</button>
                                                            </div>
                                                        </div>
                                                       
                                                    </form>
                                                </div>
                                            </div> </div>
						
					</div>
				</div>
			</div>
		</section>
		
		<script>
		
		$(document).ready(function(){
			
			
			var preview = $(".ekle");
 
$(".file").change(function(event){
var input = $(event.currentTarget);
var file = input[0].files[0];
var reader = new FileReader();
reader.onload = function(e){
image_base64 = e.target.result;
preview.html("<img src='"+image_base64+"' style='max-width:200px'/>");
};
reader.readAsDataURL(file);
});
			
		});
		
		</script>
		
		<style>
		
		li{
			list-style:none;
		}
		
		</style>