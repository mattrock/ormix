<section id="content">
            <div class="page page-tables-footable">
                <!-- bradcome -->
                <div class="b-b mb-10">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <h1 class="h3 m-0">Malzeme Stok Yönetimi</h1>
                        </div>
                    </div>
                </div>

                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <section class="boxs ">
                            <div class="boxs-header">
                            </div>
                            <div class="boxs-body">
                                <div class="form-group">
                                    <label for="filter" style="padding-top: 5px">Arama:</label>
                                    <input id="filter" type="text" class="form-control rounded w-md mb-10 inline-block">
                                     <div class="btn-group" style="margin-left: 50px">
                                                <button type="button" class="btn btn-raised btn-success btn-sm" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-refresh"></i> </button>
                                            </div>
                                </div> <br><br>

                            <table id="searchTextResults" data-filter="#filter" data-page-size="5" class="footable table table-custom" align=”center”>
                                    <thead>
                                        <tr>
                                            
                                        <th>Malzeme Kodu</th>
                                        <th>Malzeme Adı</th>
                                        <th>Stok</th>
                                        <th>Stok Arttır</th>
                                        <th>İşlem</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									
									
									 <?php foreach($verilers as $yazyaz) { 	?>
								
                                 
								        <tr>
                                           <td><?php echo $yazyaz->urun_kodu; ?></div></td>
                                            <td><?php echo 	$yazyaz->urun_adi; ?></td>
                                            <td><span class="yaz"><?php echo $yazyaz->stok; ?></span> <?php $birim = $this->selectt->birimgetir($yazyaz->urun_id); echo $birim->birim; ?></td>
                                            <td><input id="filter" class="emre" type="number" size="5"></td>
                                            <td>
                                            <button style="background-color: green;margin-top: 1px" id="<?php echo $yazyaz->id; ?>" class="sipsi btn btn-warning btn-sm btn-raised mr-5" data-method="crop" type="button" title="Kaydet">
                                                <span class="docs-tooltip" data-toggle="tooltip" title="Kaydet">
                                                    <span class="fa fa-check"></span>
                                                </span>
                                            </button>
                                            </td>
                                            </tr>
											
											
									 <?php } ?>
                                            
                                    </tfoot>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
		
		
<script>

$(document).ready(function(){
	
	$(".sipsi").click(function(){
		
		var indis = $(".sipsi").index(this);
		
		text = $(".emre").eq(indis).val();
		eskideger = $(".yaz").eq(indis).html();
		id = $(".sipsi").eq(indis).attr("id");
		
		eskideger = parseInt(eskideger);
		yenideger = parseInt(text);
		topla = eskideger+yenideger;
		
		
		
		$.ajax({
		url:"<?php echo base_url("yonetimpaneli/malzemestokekle/"); ?>",
		type:"POST",
		data:{id:id,text:topla},
		success:function(r){
			$(".yaz").eq(indis).html(topla);
		    $(".emre").eq(indis).val("");
		}
	    });
		
		
		
		
	});
	
});


</script>	